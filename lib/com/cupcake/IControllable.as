﻿package com.cupcake 
{
	public interface IControllable 
	{
		function get isInitialized():Boolean;
		function get isPaused():Boolean;
		function get isRunning():Boolean;
		
		function Destroy():void;
		function Pause():void;
		function Reset():void;
		function Resume():void;
		function Start():void;
		function Stop():void;
	}
}
