package com.reyco1.box2dwrapper.samples.tutorials
{
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	
	import com.reyco1.physinjector.PhysInjector;
	import com.reyco1.physinjector.data.PhysicsObject;
	import com.reyco1.physinjector.data.PhysicsProperties;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class AbstractTutorialBase extends Sprite
	{
		protected var physicsInjector:PhysInjector;
		private   var opacity:Number = 0.75;
		private   var floor:b2Body;
		
		public function AbstractTutorialBase()
		{			
			addEventListener(Event.ADDED_TO_STAGE, handleAdded);
		}
		
		protected function handleAdded(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, handleAdded);
			
			physicsInjector = new PhysInjector(stage, new b2Vec2(0, 60), true, addChild( new Sprite() ) as Sprite);		
			floor = createRectangle(10, stage.stageHeight - 30, stage.stageWidth - 20, 20, 0x448822, false).body;
			
			addEventListener(Event.ENTER_FRAME, update);
			
			setup();
		}
		
		protected function setup():void
		{
			// for override
		}
		
		public function clear():void
		{
			removeEventListener(Event.ENTER_FRAME, update);
			physicsInjector.dispose();
			physicsInjector = null;
		}
		
		public function update(e:Event):void
		{
			physicsInjector.update();
		}
		
		public function createRectangle(posX:Number, posY:Number, sizeW:Number, sizeH:Number, color:uint = 0xff8844, isDynamic:Boolean = true, 
				density:Number = 1, friction:Number = 0.2, restitution:Number = 0.0, categoryBits:uint = 0x0002, maskBits:uint = 0x0002):PhysicsObject
		{
			var rect:Sprite = new Sprite();
			rect.graphics.lineStyle(0.5, 0x000000, opacity);
			rect.graphics.beginFill(color, opacity);
			rect.graphics.drawRect(0, 0, sizeW, sizeH);
			rect.graphics.endFill();			
			rect.x = posX;
			rect.y = posY;
			addChild(rect);
			
			var props:PhysicsProperties = new PhysicsProperties();
			props.isDynamic = isDynamic;
			props.density = density;
			props.friction = friction;
			props.restitution = restitution;
			props.categoryBits = categoryBits;
			props.maskBits = maskBits;
			
			return physicsInjector.injectPhysics(rect, PhysInjector.SQUARE, props);
		}
		
		public function createCircle(posX:Number, posY:Number, radius:Number, color:uint = 0xff8844, isDynamic:Boolean = true, 
				density:Number = 0.2, friction:Number = 0.2, restitution:Number = 0.0, categoryBits:uint = 0x0002, maskBits:uint = 0x0002):PhysicsObject
		{
			var circ:Sprite = new Sprite();
			circ.graphics.lineStyle(0.5, 0x000000, opacity);
			circ.graphics.beginFill(color, opacity);
			circ.graphics.drawCircle(radius, radius, radius);
			circ.graphics.moveTo(radius, radius);
			circ.graphics.lineTo(radius, 0)
			circ.graphics.endFill();			
			circ.x = posX;
			circ.y = posY;
			addChild(circ);
			
			var props:PhysicsProperties = new PhysicsProperties();
			props.isDynamic = isDynamic;
			props.density = density;
			props.friction = friction;
			props.restitution = restitution;
			props.categoryBits = categoryBits;
			props.maskBits = maskBits;
			
			return physicsInjector.injectPhysics(circ, PhysInjector.CIRCLE, props);
		}
	}
}