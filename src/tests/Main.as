package com.reyco1.box2dwrapper.samples.tutorials.starling
{
	import Box2D.Common.Math.b2Vec2;
	
	import com.reyco1.physinjector.PhysInjector;
	import com.reyco1.physinjector.data.PhysicsObject;
	import com.reyco1.physinjector.data.PhysicsProperties;
	import com.reyco1.physinjector.plugins.ExplosionPlugin;
	import com.reyco1.physinjector.plugins.MagnetizePlugin;
	
	import flash.events.MouseEvent;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class Main extends Sprite
	{
		private var floor:Quad;
		private var physics:PhysInjector;
		private var interval:uint;
		private var textField:TextField;
		private var explosion:ExplosionPlugin;
		private var magetize:MagnetizePlugin;
		
		public function Main()
		{
			addEventListener(Event.ADDED_TO_STAGE, handleAdded);
		}
		
		private function handleAdded(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, handleAdded);
			
			PhysInjector.STARLING = true;
			physics = new PhysInjector(Starling.current.nativeStage, new b2Vec2(0, 40), true, Starling.current.nativeOverlay);
			
			addEventListener(Event.ENTER_FRAME, updatePhysics);
			
			addMagnet();
			addFloor();
			addQuad();
			addBodyCountTextField();
			
			interval = setInterval(addQuad, 500);
			
			explosion = new ExplosionPlugin(physics);
			Starling.current.nativeStage.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
		}
		
		protected function handleMouseDown(event:MouseEvent):void
		{			
			explosion.create(Starling.current.nativeStage.mouseX, Starling.current.nativeStage.mouseY, 200, 20);
		}
		
		private function addMagnet():void
		{
			var quad:Quad = new Quad(50, 50, 0xFFFFFF);
			quad.x = stage.stageWidth * 0.5;
			quad.y = stage.stageHeight * 0.5;
			addChild( quad );
			
			physics.injectPhysics( quad, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false}) );
			
			magetize = new MagnetizePlugin(quad, physics, 2, 200);
			magetize.active = true;
		}
		
		private function addBodyCountTextField():void
		{
			textField = new TextField(100, 40, "Current body count: 0");
			textField.x = stage.stageWidth * 0.25;
			textField.y = 10;
			addChild( textField );
		}
		
		public function addFloor():void
		{
			floor = new Quad(stage.stageWidth - 20, 20, Math.random()*0xFFFFFF);
			floor.x = 10;
			floor.y = stage.stageHeight - floor.height * 0.5 - 10;
			addChild( floor );
			
			physics.injectPhysics( floor, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false}) );
		}
		
		private function addQuad():void
		{
			var quad:Quad = new Quad(50, 50, Math.random()*0xFFFFFF);
			quad.x = stage.stageWidth * Math.random();
			quad.y = quad.height * 0.5 + 10;
			quad.rotation = 360 * Math.random();
			addChild( quad );
			
			physics.injectPhysics( quad, PhysInjector.SQUARE, new PhysicsProperties({restitution:0.4}) );
			
			magetize.subscribe(quad);
		}
		
		private function updatePhysics():void
		{
			physics.update();
			
			textField.text = "Current body count:" + physics.bodies.length;
			
			for (var a:int = 0; a < physics.bodies.length; a++) 
			{
				if(PhysicsObject(physics.bodies[a].GetUserData()).displayObject.y > stage.stageHeight + 20)
				{
					physics.removePhysics( PhysicsObject(physics.bodies[a].GetUserData()).displayObject );
				}
			}			
		}
		
		public function clearPhysics():void
		{
			clearInterval(interval);
			
			removeEventListener(Event.ENTER_FRAME, updatePhysics);
			physics.dispose();
			physics = null;
		}
	}
}