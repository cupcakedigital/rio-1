package com.reyco1.box2dwrapper.samples.tutorials
{
	import Box2D.Common.Math.b2Vec2;
	
	import com.reyco1.physinjector.PhysInjector;
	import com.reyco1.physinjector.data.PhysicsObject;
	import com.reyco1.physinjector.data.PhysicsProperties;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	[SWF(width="800", height="600", frameRate="60")]
	public class SimpleTest extends Sprite
	{
		protected var floor:Sprite;
		protected var box:Sprite;
		protected var physics:PhysInjector;
		
		private var boxP:PhysicsObject;
		
		public function SimpleTest()
		{
			super();			
			addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
		}
		
		protected function handleAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
			
			createSprites();
			injectPhysics();
			
			addEventListener(Event.ENTER_FRAME, onUpdate);
		}
		
		private function createSprites():void
		{
			floor = new Sprite();
			floor.graphics.beginFill(0xFF0000);
			floor.graphics.drawRect(0, 0, stage.stageWidth - 20, 20);
			floor.graphics.endFill();
			floor.x = stage.stageWidth * 0.5 - floor.width * 0.5;
			floor.y = stage.stageHeight - floor.height - 10;
			addChild( floor );
			
			box = new Sprite();
			box.graphics.beginFill(0x00FF00);
			box.graphics.drawRect(0, 0, 50, 50);
			box.graphics.endFill();
			box.x = stage.stageWidth * 0.5 - box.width * 0.5;
			box.y = 10;
			box.rotation = 36;
			addChild( box );
		}	
		
		private function injectPhysics():void
		{
			physics = new PhysInjector(stage, new b2Vec2(0, 60), true, addChild(new Sprite) as Sprite);			
			
			var floorProps:PhysicsProperties = new PhysicsProperties();
			floorProps.isDynamic 	= false;
			floorProps.friction 	= 1;
			floorProps.restitution 	= 0.5;
			floorProps.name			= "floor";
			floorProps.contactGroup = "group1";
			floorProps.linearVelocity = new b2Vec2(0, 60);
			
			var floorObject:PhysicsObject = physics.injectPhysics(floor, PhysInjector.SQUARE, floorProps);			
			
			boxP = physics.injectPhysics(box, PhysInjector.SQUARE, 
				new PhysicsProperties(
					{
						isDynamic	: true, 
						friction	: 0.2, 
						restitution	: 0.5,
						name		: "box",
						contactGroup: "group2",
						isBullet	: true
					}));
		}
		
		protected function onUpdate(event:Event):void
		{
			physics.update();
		}
		
		public function clear():void
		{
			removeEventListener(Event.ENTER_FRAME, onUpdate);
			physics.dispose();
			physics = null;
		}		
	}
}