package tests
{
	import Box2D.Common.Math.b2Vec2;
	
	import com.reyco1.physinjector.PhysInjector;
	import com.reyco1.physinjector.data.PhysicsObject;
	import com.reyco1.physinjector.data.PhysicsProperties;
	import com.reyco1.physinjector.plugins.BuoyancyPlugin;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="0xFFFFFF")]	
	public class PhysBuoyancyTest extends Sprite
	{
		[Embed(source="assets/background.png")]
		private const Background:Class;
		
		private var buoyancy:BuoyancyPlugin;
		private var tick:uint;
		private var pool:Sprite;
		private var physicsInjector:PhysInjector;
		
		public function PhysBuoyancyTest()
		{
			addEventListener(Event.ADDED_TO_STAGE, setup);
		}
		
		private function setup(e:Event):void
		{
			// physics controller
			physicsInjector = new PhysInjector(stage, new b2Vec2(0, 45));
			
			// background
			var background:Bitmap = new Background();
			addChild( background );
			
			// instructions text
			var instructionFormat:TextFormat = new TextFormat("Arial", 40, 0x000000);
			var instrunctions:TextField = new TextField();
			instrunctions.defaultTextFormat = instructionFormat;
			instrunctions.autoSize = "left";
			instrunctions.text = "Drag the Pool!";
			addChild( instrunctions );
			
			// pool sprite
			pool = new Sprite();
			pool.graphics.beginFill(0x00FFFF, 0.5);
			pool.graphics.drawRect(0, 0, 400, 250);
			pool.graphics.endFill();			
			pool.x = (stage.stageWidth - pool.width) * 0.5;
			pool.y = stage.stageHeight - pool.height - 100;
			pool.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			addChild(pool);
			
			// buoyancy plugn
			buoyancy = new BuoyancyPlugin(pool, physicsInjector);
			
			// create floor
			createRectangle(10, stage.stageHeight - 30, stage.stageWidth - 20, 20, 0x448822, 0, false);
			
			// start randon box interval
			tick = setInterval(addBox, 400);
			
			// add enter frame
			addEventListener(Event.ENTER_FRAME, update);
		}	
		
		private function handleMouseUp(event:MouseEvent):void
		{
			pool.stopDrag();
		}
		
		private function handleMouseDown(event:MouseEvent):void
		{
			pool.startDrag();
		}
		
		private function addBox():void
		{
			createRectangle(stage.stageWidth * Math.random(), 30, Math.random() * 50 + 20, Math.random() * 50 + 20, 0xFFFFFF * Math.random(), 360 * Math.random(), true);
		}
		
		public function createRectangle(posX:Number, posY:Number, sizeW:Number, sizeH:Number, color:uint = 0xff8844, rot:Number = 0, isDynamic:Boolean = true):PhysicsObject
		{
			var rect:Sprite = new Sprite();
			rect.graphics.lineStyle(0.5, 0x000000, 0.75);
			rect.graphics.beginFill(color, 0.75);
			rect.graphics.drawRect(0, 0, sizeW, sizeH);
			rect.graphics.endFill();
			rect.rotation = rot;
			rect.x = posX;
			rect.y = posY;
			addChild(rect);
			
			var props:PhysicsProperties = new PhysicsProperties();
			props.isDynamic = isDynamic;
			
			return physicsInjector.injectPhysics(rect, PhysInjector.SQUARE, props);
		}
		
		public function clear():void
		{
			pool.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
			stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
			
			clearInterval( tick );
			buoyancy.dispose();
			buoyancy = null;
			
			removeEventListener(Event.ENTER_FRAME, update);
			physicsInjector.dispose();
			physicsInjector = null;
		}
		
		public function update(e:Event):void
		{
			physicsInjector.update();
			setChildIndex(pool, numChildren-1);
		}
	}
}