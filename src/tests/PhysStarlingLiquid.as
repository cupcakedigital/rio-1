package tests
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	import starling.core.Starling;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="0x000000")]
	public class PhysStarlingLiquid extends Sprite
	{
		private var star:Starling;
		
		public function PhysStarlingLiquid()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			star = new Starling(Main, stage);
			star.showStats = true;
			star.start();
		}
	}
}