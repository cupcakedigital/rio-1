package tests 
{
    import feathers.controls.List;
    import feathers.layout.HorizontalLayout;
    
    import hairui.GalleryItemRenderer;
    import hairui.slideMenu;
    
    import starling.animation.Transitions;
    import starling.core.Starling;
    import starling.display.BlendMode;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.textures.Texture;
    import starling.utils.deg2rad;

    public class Game extends Sprite
    {

        private var girl:Sprite;
		
		private var mBackground:Image;
		
		private var mChair:Image;
		
		private var mBody:Image;
		private var mHead:Image;
		private var mHairOL:Image;
		private var mHairMID:Image;
		private var mHairUL:Image;
		private var mBow:Image;
		private var mFlower:Image;
		private var mHairband:Image;
		
		
		private var mBodyTexture:Texture;
		private var mHeadTexture:Texture;
		private var mHairOLTexture:Texture;
		private var mHairMIDTexture:Texture;
		private var mHairULTexture:Texture;
		private var mBowTexture:Texture;
		private var mFlowerTexture:Texture;
		private var mHairbandTexture:Texture;
		
		
		private var gameUI:GameUI;
		
		private var _hairBaseNames:Vector.<String> = new <String>["SSC_HAIR_GAME_HAIR_OL_","SSC_HAIR_GAME_HAIR_MID_","SSC_HAIR_GAME_HAIR_UL_"];
		private var _seqIds:Vector.<String> = new <String>["0001","0002","0003","0004","0005","0006","0007","0008","0009","0010","0011","0012","0013","0014","0015","0016","0017","0018","0019","0020"
			,"0021","0022","0023","0024","0025","0026","0027","0028","0029","0030"];
		
		private var _headBaseName:String = "SSC_HAIR_GAME_HEADS_";
		private var _bodyBaseName:String = "SSC_HAIR_GAME_BODIES_";
		
		private var _hairChooserText:String = "SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_";
		private var _girlChooserText:String = "Girls_BUTTONS_";
		private var _headbandChooserText:String = "SSC_HAIR_GAME_HEADBAND_CHOICES_";
		private var _tiaraChooserText:String = "SSC_HAIR_GAME_TIARAS_CHOICES_";
		private var _bowChooserText:String = "SSC_HAIR_GAME_BOW_CHOICES_";
		private var _flowerChooserText:String = "SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_";
		
		private var currentGirl:int = 4;
		private var currentGirlVector:Vector.<int> = new <int>[];
		
		private var girl1:Vector.<int> = new <int>[19,1,1];
		private var girl2:Vector.<int> = new <int>[8,4,4];
		private var girl3:Vector.<int> = new <int>[17,2,2];
		private var girl4:Vector.<int> = new <int>[6,3,3];
		private var girl5:Vector.<int> = new <int>[4,5,5];
		private var girl6:Vector.<int> = new <int>[5,6,6];
		private var girl7:Vector.<int> = new <int>[9,0,0];
		
		private var _girlSet:Vector.<Vector.<int>> = new <Vector.<int>>[girl1,girl2,girl3,girl4,girl5,girl6,girl7];
		
		
        public function Game()
        {
            init();
        }
        
        private function init():void
        {
           
			
			mBackground = new Image(Root.assets.getTexture("SSC_HAIR_GAME_BG"));
			mBackground.blendMode = BlendMode.NONE;
			mBackground.addEventListener(TouchEvent.TOUCH, onBackgroundTouched);
			trace("My bg width is " + mBackground.width);
			mBackground.x = -85;
			addChild(mBackground);
			
			
			// Create girl images, we will swap textures afterwards
			
			girl = new Sprite();
			girl.x = 60;
			addChild(girl);
			
			mHairUL = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HAIR_UL_0005"));
			mHairUL.x = 90;
			mHairUL.y = 2;
			girl.addChild(mHairUL);
			
			mBody = new Image(Root.assets.getTexture("SSC_HAIR_GAME_BODIES_0006"));
			mBody.x = 162;
			mBody.y = 168;
			girl.addChild(mBody);
			
			mChair = new Image(Root.assets.getTexture("SSC_HAIR_GAME_CHAIR_OL"));
			mChair.x = 84;
			mChair.y = 260;
			girl.addChild(mChair);
			
			mHairMID = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HAIR_MID_0005"));
			mHairMID.x = 90;
			mHairMID.y = 2;
			girl.addChild(mHairMID);
			
			mHead = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HEADS_0006"));
			mHead.x = 156;
			mHead.y = 65;
			girl.addChild(mHead);
			
			mHairOL = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HAIR_OL_0005"));
			
			mHairOL.x = 100;
			mHairOL.y = 18;
			girl.addChild(mHairOL);
			
			
			
			// HUD
			
			
			gameUI = new GameUI();
			addChild(gameUI);
			
			addEventListener(GameUI.PICK_GIRL, changeGirl);
			addEventListener(GameUI.PICK_STYLE, changeHair);
        }
		
   
		
		private function changeGirl(event:Event = null, GID:int=0):void{
			
			trace("Girl ID " + GID );
			
			currentGirl = GID;
			currentGirlVector = _girlSet[GID];
			
			mBodyTexture 	= 	Root.assets.getTexture(_bodyBaseName + _seqIds[currentGirlVector[1]]);
			mBody.texture = mBodyTexture;
			mBody.readjustSize();
			
			mHeadTexture = 	Root.assets.getTexture(_headBaseName + _seqIds[currentGirlVector[2]]);
			mHead.texture = mHeadTexture;
			mHead.readjustSize();
			
			changeHair(null, currentGirlVector[2]);
			
		}
		
		
        private function changeHair(event:Event = null, HID:int=0):void{
           
			mHairOLTexture 	= 	Root.assets.getTexture(_hairBaseNames[0] + _seqIds[HID]);
			mHairOL.texture = mHairOLTexture;
			mHairOL.readjustSize();
			
			mHairMIDTexture = 	Root.assets.getTexture(_hairBaseNames[1] + _seqIds[HID]);
			mHairMID.texture = mHairMIDTexture;
			mHairMID.readjustSize();
			
			mHairULTexture 	= 	Root.assets.getTexture(_hairBaseNames[2] + _seqIds[HID]);
			mHairUL.texture = mHairULTexture;
			mHairUL.readjustSize();
        }
        
        private function onBackgroundTouched(event:TouchEvent):void
        {
           
			if (event.getTouch(mBackground, TouchPhase.BEGAN))
            {
				
				/*
				Root.assets.playSound("sfx");
				var randomHair:int = Math.floor(Math.random()*22);
				
				var randomGirl:int = Math.floor(Math.random()*6);
				
				changeGirl(null, randomGirl);
				
				//changeHair(randomHair);
				
				
				gameUI.changeList("Hair");
				
               // dispatchEventWith(GAME_OVER, true, 100);
				*/
            }
			
        }
		
		
		
    }
}