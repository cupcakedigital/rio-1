package com.reyco1.box2dwrapper.samples.tutorials
{
	import Box2D.Dynamics.b2Body;
	
	import com.reyco1.physinjector.data.PhysicsObject;
	import com.reyco1.physinjector.factory.JointFactory;
	
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	public class CartTutorial extends AbstractTutorialBase
	{
		private var _wheel1:b2Body;
		private var _wheel2:b2Body;
		
		public function CartTutorial()
		{
			super();
		}
		
		override protected function setup():void
		{
			physicsInjector.allowDrag = false;
			
			var cartBody:PhysicsObject 	= createRectangle(400, 30, 150, 25);
			var wheel1:PhysicsObject 	= createCircle(cartBody.displayObject.x + 5, 30, 20, 0xff8844, true, 1, 40);
			var wheel2:PhysicsObject 	= createCircle(cartBody.displayObject.x + cartBody.displayObject.width - 45, 30, 20, 0xff8844, true, 1, 40);
			
			_wheel1 = wheel1.body;			
			_wheel2 = wheel2.body;
			
			createRectangle(125, 30, 70, 70, 0x003366, true, 3, 0.5);
			createRectangle(675, 30, 70, 70, 0x003366, true, 3, 0.5);
			
			JointFactory.createRevoluteJoint( cartBody.body, _wheel1, wheel1.getDisplayObjectCenterPoint() );
			JointFactory.createRevoluteJoint( cartBody.body, _wheel2, wheel2.getDisplayObjectCenterPoint() );
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyDown);
		}
		
		protected function handleKeyDown(event:KeyboardEvent):void
		{
			if(event.keyCode == Keyboard.LEFT)
			{
				_wheel1.ApplyTorque(-200);
			}
			
			if(event.keyCode == Keyboard.RIGHT)
			{
				_wheel2.ApplyTorque(200);
			}
		}
	}
}