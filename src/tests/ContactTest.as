package com.reyco1.box2dwrapper.samples.tutorials
{
	import Box2D.Dynamics.Contacts.b2Contact;
	
	import com.reyco1.physinjector.contact.ContactManager;
	import com.reyco1.physinjector.data.PhysicsObject;
	import com.reyco1.physinjector.factory.JointFactory;
	
	import flash.events.Event;

	[SWF(width="800", height="600", frameRate="60")]
	public class ContactTest extends SimpleTest
	{
		public function ContactTest()
		{
			super();
		}
		
		override protected function handleAddedToStage(event:Event):void
		{
			super.handleAddedToStage(event);
			//ContactManager.onContactBegin("floor", "box", handleContact); // individual contact listener
			ContactManager.onContactBegin("group1", "group2", handleContact, true); // group contact listener
		}
		
		private function handleContact(bodyA:PhysicsObject, bodyB:PhysicsObject, contact:b2Contact):void
		{
			// because on line 22 above we set floor first, here bodyA will be the PhysicsObject for the floor
			JointFactory.createWeldJoint(bodyA.body, bodyB.body);
		}
	}
}