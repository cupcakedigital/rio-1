package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	import starling.core.Starling;
	import com.reyco1.box2dwrapper.samples.tutorials.starling.Main;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="0x555555")]	
	public class StarlingTest extends Sprite
	{
		private var star:Starling;
				
		public function StarlingTest()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		protected function onAdded(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			star = new Starling(Main, stage);
			star.showStats = true;
			star.start();
		}
	}
}