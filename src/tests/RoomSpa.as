﻿package scenes  {
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Bounce;
	import com.greensock.easing.Linear;
	import com.reyco1.physinjector.PhysInjector;
	import com.reyco1.physinjector.data.PhysicsProperties;
	
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.sampler.Sample;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.text.TextFormat;
	
	import Box2D.Common.Math.b2Vec2;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	
	import feathers.controls.Button;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.ParticleDesignerPS;
	import starling.extensions.SoundManager;
	import starling.filters.BlurFilter;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.Color;
	import starling.utils.deg2rad;

public class RoomSpa extends Sprite {
	
	// Assets
	private var assets:AssetManager;
	private var soundManager:SoundManager;
	private var appDir:File = File.applicationDirectory;
	
	// Standard display
	private var stageWidth:int;
	private var stageHeight:int;
	
	// Scene Objects
	private var sceneRoot:Sprite;
	private var sceneWide:Sprite;
	private var sceneClose:Sprite;
	private var bg:Image;
	private var chair:Image;
	private var character:Image;
	
	private var characterString:String = "";

	// filters
	
	private var blur:BlurFilter = new BlurFilter(10,10);
	
	// Bubble Events
	
	public static const LOAD_LOBBY:String = "loadLobby";
	

	public function RoomSpa(charString:String = "Shimmer") {
		
		characterString = charString;
		
		assets = new AssetManager();
		assets.verbose = Capabilities.isDebugger;
		
		assets.enqueue(
			appDir.resolvePath("textures/roomSpa")
			// Add in room sound if available
		);
		
		assets.loadQueue(function onProgress(ratio:Number):void
		{
			if (ratio == 1){
				trace("loaded textures");
				Starling.juggler.delayCall(function():void
				{
					// now would be a good time for a clean-up 
					System.pauseForGCIfCollectionImminent(0);
					System.gc();
					
				}, 0.15);
				addObjects();
			};
				
		});
		
	}

	
	private function addObjects():void
	{

		// Create main scene root
		sceneRoot = new Sprite();
		sceneRoot.x = -110; // offset for widescreen;
		addChild(sceneRoot);
		
		//sceneClose = new Sprite();
		//sceneRoot.addChild(sceneClose);
		
		sceneWide = new Sprite();
		sceneRoot.addChild(sceneWide);
		
		bg = new Image(assets.getTexture("SPA_BG"));
		
		bg.pivotX = 1024;
		bg.pivotY = 650;
		bg.scaleX = bg.scaleY = 0.67;
		bg.x = 667;
		bg.y = 384;
		sceneWide.addChild(bg);
		
		
		//var bgClose:Image = new Image(assets.getTexture("SPA_FACEMASK_BG"));
		//sceneClose.addChild(bgClose);
		
		var characterPath:String;
		
		switch(characterString)
		{
			case "Shimmer":
			{
				characterPath = "SHIMMER_SIT";
				break;
			}
			
			case "Walden":
			{
				characterPath = "WALDEN_SIT";
				break;
			}
				
			case "Widget":
			{
				characterPath = "WIDGET_SIT";
				break;
			}
				
			case "Wubbzy":
			{
				characterPath = "WUBBZY_SIT";
				break;
			}
			case "Piggy":
			{
				characterPath = "MOOGOO_SIT";
				break;
			}
			default:
			{
				characterPath = "WUBBZY_SIT";
				break;
			}
		}
		
		var charVector:Vector.<Image> = new <Image>[];
		
		for (var i:int = 0; i < 10; i++) 
		{
			var charImage:Image = new Image(assets.getTexture(characterPath));
			charImage.x = 100 * i;
			charImage.y = 100;
			charVector.push(charImage);
			sceneWide.addChild(charVector[i]);
		}
		
		
		
		
		
		
		
		
		var powderButton:Button = new Button();
		powderButton.defaultSkin = new Image(assets.getTexture("POWDER_BRUSH"));
		powderButton.x = 950;
		powderButton.y = 500;
		powderButton.name = "powderButton"
		powderButton.addEventListener(Event.TRIGGERED, onButtonTriggered);
		sceneRoot.addChild(powderButton);

		
		
		var roomButton:Button = new Button();
		roomButton.defaultSkin = new Image(assets.getTexture("Wax"));
		roomButton.x = 1000;
		roomButton.y = 200;
		roomButton.name = "backButton"
		roomButton.addEventListener(Event.TRIGGERED, onButtonTriggered);
		sceneRoot.addChild(roomButton);
		
		
		
		var button:Button = new Button();
		button.defaultSkin = new Image(Root.assets.getTexture("LOBBY_BUTTON"));
		button.x = 150;
		button.y = 50;
		button.name = "exit";
		button.addEventListener(Event.TRIGGERED, onButtonTriggered);
		sceneRoot.addChild(button);

		
	}
	
	private function onButtonTriggered(evt:Event):void
	{
		var name:String = (evt.currentTarget as Button).name;
		trace("Hit " + name);
		switch(name)
		{
			case "exit":
			{
				// dispose everything to remove from GPU
				assets.dispose();
				// signaling back up to load lobby
				dispatchEventWith(LOAD_LOBBY, true);
				break;
			}
			
			case "powderButton":
			{
				TweenMax.to(bg,0.5,{scaleX:2,scaleY:2, y:300});
				//TweenMax.to(bg.filter,0.5,{blurX:10,blurY:10});

				//TweenMax.to(sceneWide,0.2,{delay:0.3,alpha:0, onComplete:function vis():void{sceneWide.touchable=false}});
				
				
				var charVector:Vector.<Image> = new <Image>[];
				
				for (var i:int = 0; i < 10; i++) 
				{
					var charImage:Image = new Image(assets.getTexture("WUBBZY_SIT"));
					charImage.x = 100 * i;
					charImage.y = Math.random()*500;
					charVector.push(charImage);
					sceneWide.addChild(charVector[i]);
				}
				
				
				break;
			}
			
			case "backButton":
			{
				TweenMax.to(bg,0.5,{scaleX:0.67,scaleY:0.67, x:667, y:384});
				//TweenMax.to(bg.filter,0.5,{blurX:0,blurY:0});
				//TweenMax.to(sceneWide,0.1,{alpha:1});
				break;
			}
				
			default:
			{
				break;
			}
		}
		
		
	}
	
	
	}
}