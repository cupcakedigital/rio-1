package tests 
{

	
	import feathers.controls.Button;
	import feathers.controls.List;
	import feathers.controls.PageIndicator;
	import feathers.controls.Scroller;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.data.ListCollection;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.TiledRowsLayout;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.ResizeEvent;
	import starling.text.BitmapFont;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class GameUI extends Sprite
	{
		
		
		private var _menuBottom:Sprite;
		private var _menuTop:Sprite;
		
		private var _list:List;
		private var _listGap:Number = 4;
		private var _listPadding:Number = 0;
		private var _listLayout:HorizontalLayout; 
		
		private var _butHome:Button;
		private var _butGirls:Button;
		private var _butHair:Button;
		private var _butAccessories:Button;
		
		private static var collectionGirls:ListCollection;
		private static var collectionHair:ListCollection;
		private static var collectionAccessories:ListCollection;
		
		private static var collectionStyle:ListCollection;
		private static var collectionGlitter:ListCollection;
		private static var collectionDye:ListCollection;
		
		private static var collectionHeadbands:ListCollection;
		private static var collectionTiaras:ListCollection;
		private static var collectionBows:ListCollection;
		private static var collectionFlowers:ListCollection;
				
		public static const GAME_OVER:String = "gameOver";
		public static const PICK_GIRL:String = "pickGirl";
		public static const PICK_STYLE:String = "pickStyle";
		public static const PICK_GLITTER:String = "pickGlitter";
		public static const PICK_DYE:String = "pickDye";
		public static const PICK_TIARAS:String = "pickTiaras";
		public static const PICK_HEADBANDS:String = "pickHeadbands";
		public static const PICK_BOWS:String = "pickBows";
		public static const PICK_FLOWERS:String = "pickFlowers";
		
		public function GameUI()
		{
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}


		private function addedToStageHandler(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			
			_menuBottom = new Sprite();
			
			var bottomBg:Image = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HUD_01"));
			_menuBottom.x = Costanza.STAGE_WIDTH/2  - bottomBg.width/2;
			_menuBottom.y = Costanza.STAGE_HEIGHT - bottomBg.height;
			
			_menuBottom.addChild(bottomBg);
			
			
			// Bottom Menu main buttons
			
			_butHome = new Button();
			_butHome.defaultSkin = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HUD_BUT_HOME"));
			_butHome.x = 8;
			_butHome.y = 10;
			_butHome.addEventListener(Event.TRIGGERED,function hitHome():void{trace("Go Home");dispatchEventWith(GAME_OVER, true);});
			_menuBottom.addChild(_butHome);
			
			_butGirls = new Button();
			_butGirls.defaultSkin = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HUD_BUT_GIRLS"));
			_butGirls.addEventListener(Event.TRIGGERED,function hitGirls():void{changeList("Girls");});
			_butGirls.x = 52;
			_butGirls.y = 10;
			_menuBottom.addChild(_butGirls);
			
			_butHair = new Button();
			_butHair.defaultSkin = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HUD_BUT_HAIR"));
			_butHair.addEventListener(Event.TRIGGERED,function hitHair():void{changeList("Hair");});
			_butHair.x = 54;
			_butHair.y = 52;
			_menuBottom.addChild(_butHair);
			
			_butAccessories = new Button();
			_butAccessories.defaultSkin = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HUD_BUT_ACC"));
			_butAccessories.addEventListener(Event.TRIGGERED,function hitAccessories():void{changeList("Accessories");});
			_butAccessories.x = 8;
			_butAccessories.y = 52;
			_menuBottom.addChild(_butAccessories);
			
			
			addChild(_menuBottom);
			
			
			_menuTop = new Sprite();
			
			var topBg:Image = new Image(Root.assets.getTexture("SSC_HAIR_GAME_HUD_02"));
			_menuTop.x = 30;
			_menuTop.y = -190;
			_menuTop.addChild(topBg);
			
			addChild(_menuTop);
			
			
			//// Sliding menu
			
			
			stage.addEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
			
			
			
			collectionGirls = new ListCollection(
				[
					{ label: "Girl1", type:"Girl", id:0, texture: Root.assets.getTexture("Girls_BUTTONS_0001")},
					{ label: "Girl2", type:"Girl", id:1,  texture: Root.assets.getTexture("Girls_BUTTONS_0002")},
					{ label: "Girl3", type:"Girl", id:2,  texture: Root.assets.getTexture("Girls_BUTTONS_0003")},
					{ label: "Girl4", type:"Girl", id:3,  texture: Root.assets.getTexture("Girls_BUTTONS_0004")},
					{ label: "Girl5", type:"Girl", id:4,  texture: Root.assets.getTexture("Girls_BUTTONS_0005")},
					{ label: "Girl6", type:"Girl", id:5,  texture: Root.assets.getTexture("Girls_BUTTONS_0006")},
					{ label: "Girl7", type:"Girl", id:6,  texture: Root.assets.getTexture("Girls_BUTTONS_0007")},
				]);
			
			collectionHair = new ListCollection(
				[
					{ label: "Style", type:"Hair", id:0, texture: Root.assets.getTexture("SSC_HAIR_GAME_STYLE_BUTTON")},
					{ label: "Glitter", type:"Hair", id:1, texture: Root.assets.getTexture("SSC_HAIR_GAME_GLITTER_BUTTON")},
					{ label: "Dye", type:"Hair", id:2, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_BUTTON")},
				]);
			
			
			collectionStyle = new ListCollection(
				[
					{ label: "Style1", type:"Style", id:0, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0001")},
					{ label: "Style2", type:"Style", id:1, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0002")},
					{ label: "Style3", type:"Style", id:2, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0003")},
					{ label: "Style4", type:"Style", id:3, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0004")},
					{ label: "Style5", type:"Style", id:4, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0005")},
					{ label: "Style6", type:"Style", id:5, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0006")},
					{ label: "Style7", type:"Style", id:6, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0007")},
					{ label: "Style8", type:"Style", id:7, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0008")},
					{ label: "Style9", type:"Style", id:8, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0009")},
					{ label: "Style10", type:"Style", id:9, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0010")},
					{ label: "Style11", type:"Style", id:10, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0011")},
					{ label: "Style12", type:"Style", id:11, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0012")},
					{ label: "Style13", type:"Style", id:12, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0013")},
					{ label: "Style14", type:"Style", id:13, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0014")},
					{ label: "Style15", type:"Style", id:14, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0015")},
					{ label: "Style16", type:"Style", id:15, texture: Root.assets.getTexture("SSC_HAIR_GAME_HAIR_CHOICE_BUTTONS_0016")},

				]);
			
			collectionGlitter = new ListCollection(
				[
					{ label: "Glitter1", type:"Glitter", id:0, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Glitter2", type:"Glitter", id:1, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Glitter3", type:"Glitter", id:2, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Glitter4", type:"Glitter", id:3, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Glitter5", type:"Glitter", id:4, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Glitter6", type:"Glitter", id:5, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Glitter7", type:"Glitter", id:6, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Glitter8", type:"Glitter", id:7, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Glitter9", type:"Glitter", id:8, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Glitter10", type:"Glitter", id:9, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
				]);
			
			collectionDye = new ListCollection(
				[
					{ label: "Dye1", type:"Dye", id:0, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Dye2", type:"Dye", id:1, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Dye3", type:"Dye", id:2, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Dye4", type:"Dye", id:3, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Dye5", type:"Dye", id:4, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Dye6", type:"Dye", id:5, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Dye7", type:"Dye", id:6, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Dye8", type:"Dye", id:7, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Dye9", type:"Dye", id:8, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
					{ label: "Dye10", type:"Dye", id:9, texture: Root.assets.getTexture("SSC_HAIR_GAME_DYE_COLORCHOICE_BUTTON")},
				]);
			
			collectionAccessories = new ListCollection(
				[
					{ label: "Headbands", type:"Accessories", id:0, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBANDS_BUTTON")},
					{ label: "Tiaras", type:"Accessories", id:1, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_BUTTON")},
					{ label: "Bows", type:"Accessories", id:2, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOWS_BUTTON")},
					{ label: "Flowers", type:"Accessories", id:3, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWERS_BUTTON")},
				]);
			
			
			
			collectionHeadbands = new ListCollection(
				[
					{ label: "Headband1", type:"Headband", id:0, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0001")},
					{ label: "Headband2", type:"Headband", id:1, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0002")},
					{ label: "Headband3", type:"Headband", id:2, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0003")},
					{ label: "Headband4", type:"Headband", id:3, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0004")},
					{ label: "Headband5", type:"Headband", id:4, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0005")},
					{ label: "Headband6", type:"Headband", id:5, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0006")},
					{ label: "Headband7", type:"Headband", id:6, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0007")},
					{ label: "Headband8", type:"Headband", id:7, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0008")},
					{ label: "Headband9", type:"Headband", id:8, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0009")},
					{ label: "Headband10", type:"Headband", id:9, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0010")},
					{ label: "Headband11", type:"Headband", id:10, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0011")},
					{ label: "Headband12", type:"Headband", id:11, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0012")},
					{ label: "Headband13", type:"Headband", id:12, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0013")},
					{ label: "Headband14", type:"Headband", id:13, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0014")},
					{ label: "Headband15", type:"Headband", id:14, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0015")},
					{ label: "Headband16", type:"Headband", id:15, texture: Root.assets.getTexture("SSC_HAIR_GAME_HEADBAND_CHOICES_0016")},
					
				]);
			
			collectionTiaras = new ListCollection(
				[
					{ label: "Tiara1", type:"Tiara", id:0, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0001")},
					{ label: "Tiara2", type:"Tiara", id:1, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0002")},
					{ label: "Tiara3", type:"Tiara", id:2, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0003")},
					{ label: "Tiara4", type:"Tiara", id:3, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0004")},
					{ label: "Tiara5", type:"Tiara", id:4, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0005")},
					{ label: "Tiara6", type:"Tiara", id:5, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0006")},
					{ label: "Tiara7", type:"Tiara", id:6, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0007")},
					{ label: "Tiara8", type:"Tiara", id:7, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0008")},
					{ label: "Tiara9", type:"Tiara", id:8, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0009")},
					{ label: "Tiara10", type:"Tiara", id:9, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0010")},
					{ label: "Tiara11", type:"Tiara", id:10, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0011")},
					{ label: "Tiara12", type:"Tiara", id:11, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0012")},
					{ label: "Tiara13", type:"Tiara", id:12, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0013")},
					{ label: "Tiara14", type:"Tiara", id:13, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0014")},
					{ label: "Tiara15", type:"Tiara", id:14, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0015")},
					{ label: "Tiara16", type:"Tiara", id:15, texture: Root.assets.getTexture("SSC_HAIR_GAME_TIARAS_CHOICES_0016")},
					
				]);
			
			collectionBows = new ListCollection(
				[
					{ label: "Bow1", type:"Bow", id:0, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0001")},
					{ label: "Bow2", type:"Bow", id:1, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0002")},
					{ label: "Bow3", type:"Bow", id:2, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0003")},
					{ label: "Bow4", type:"Bow", id:3, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0004")},
					{ label: "Bow5", type:"Bow", id:4, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0005")},
					{ label: "Bow6", type:"Bow", id:5, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0006")},
					{ label: "Bow7", type:"Bow", id:6, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0007")},
					{ label: "Bow8", type:"Bow", id:7, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0008")},
					{ label: "Bow9", type:"Bow", id:8, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0009")},
					{ label: "Bow10", type:"Bow", id:9, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0010")},
					{ label: "Bow11", type:"Bow", id:10, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0011")},
					{ label: "Bow12", type:"Bow", id:11, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0012")},
					{ label: "Bow13", type:"Bow", id:12, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0013")},
					{ label: "Bow14", type:"Bow", id:13, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0014")},
					{ label: "Bow15", type:"Bow", id:14, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0015")},
					{ label: "Bow16", type:"Bow", id:15, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0016")},
					{ label: "Bow17", type:"Bow", id:16, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0017")},
					{ label: "Bow18", type:"Bow", id:17, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0018")},
					{ label: "Bow19", type:"Bow", id:18, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0019")},
					{ label: "Bow20", type:"Bow", id:19, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0020")},
					{ label: "Bow21", type:"Bow", id:20, texture: Root.assets.getTexture("SSC_HAIR_GAME_BOW_CHOICES_0021")},
					
				]);
			
			collectionFlowers = new ListCollection(
				[
					{ label: "Flowers1", type:"Flowers", id:0, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0001")},
					{ label: "Flowers2", type:"Flowers", id:1, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0002")},
					{ label: "Flowers3", type:"Flowers", id:2, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0003")},
					{ label: "Flowers4", type:"Flowers", id:3, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0004")},
					{ label: "Flowers5", type:"Flowers", id:4, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0005")},
					{ label: "Flowers6", type:"Flowers", id:5, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0006")},
					{ label: "Flowers7", type:"Flowers", id:6, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0007")},
					{ label: "Flowers8", type:"Flowers", id:7, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0008")},
					{ label: "Flowers9", type:"Flowers", id:8, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0009")},
					{ label: "Flowers10", type:"Flowers", id:9, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0010")},
					{ label: "Flowers11", type:"Flowers", id:10, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0011")},
					{ label: "Flowers12", type:"Flowers", id:11, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0012")},
					{ label: "Flowers13", type:"Flowers", id:12, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0013")},
					{ label: "Flowers14", type:"Flowers", id:13, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0014")},
					{ label: "Flowers15", type:"Flowers", id:14, texture: Root.assets.getTexture("SSC_HAIR_GAME_FLOWER_CHOICE_BUTTONS_0015")},
				]);
			
			_listLayout = new HorizontalLayout();
			_listLayout.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_JUSTIFY;
			_listLayout.hasVariableItemDimensions = true;
			_listLayout.manageVisibility = true;
			_listLayout.gap = _listGap;
			_listLayout.paddingLeft = _listPadding;
			
			_list = new List();
			_list.horizontalScrollPolicy = List.SCROLL_POLICY_ON;
			_list.snapScrollPositionsToPixels = true;
			_list.dataProvider = collectionGirls;
			_list.layout = _listLayout;
			_list.itemRendererFactory = tileListItemRendererFactory;
			_list.addEventListener(starling.events.Event.CHANGE, listTriggered);
			_menuBottom.addChild(_list);
			
			layout();
			
		}
		
		private function listTriggered(evt:Event):void{
			
			var type:String = _list.selectedItem.type;
			var id:Number = _list.selectedItem.id;
			var label:String = _list.selectedItem.label;
			
			switch(type)
			{
				case "Girl":
				{
					dispatchEventWith(PICK_GIRL, true,id);
					break;
				}
				
				case "Hair":
				{
					switch(label)
					{
						case "Style":{changeList("Style");break;};
						case "Glitter":{changeList("Glitter");break;};
						case "Dye":{changeList("Dye");break;};

					}
					
				}
				
				case "Style":
				{
					dispatchEventWith(PICK_STYLE, true,_list.selectedItem.id);
					break;
				}
				case "Glitter":
				{
					dispatchEventWith(PICK_GLITTER, true,_list.selectedItem.id);
					break;
				}
				case "Dye":
				{
					dispatchEventWith(PICK_DYE, true,_list.selectedItem.id);
					break;
				}
					
				case "Accessories":
				{
					switch(label)
					{
						case "Headbands":{changeList("Headband");break;};
						case "Tiaras":{changeList("Tiara");break;};
						case "Bows":{changeList("Bow");break;};
						case "Flowers":{changeList("Flowers");break;};	
					}
				}
				
				case "Tiaras":
				{
					dispatchEventWith(PICK_TIARAS, true,_list.selectedItem.id);
					break;
				}
				case "Headbands":
				{
					dispatchEventWith(PICK_HEADBANDS, true,_list.selectedItem.id);
					break;
				}
				case "Bows":
				{
					dispatchEventWith(PICK_BOWS, true,_list.selectedItem.id);
					break;
				}
				case "Flowers":
				{
					dispatchEventWith(PICK_FLOWERS, true,_list.selectedItem.id);
					break;
				}
					
				default:
				{
					break;
				}
			}
			
			
			trace("hit " + label + " of type " + type);
			Root.assets.playSound("sfx");
		}
		
		
		public function changeList(section:String = ""):void{
			
			_list.dataProvider = null;
			
			switch(section)
			{
				case "Hair":
				{
					_list.dataProvider = collectionHair;
					_listGap = 70;
					_listPadding = 40;
					break;
				}
					
				case "Style":
				{
					_list.dataProvider = collectionStyle;
					_listGap = 4;
					_listPadding = 0;
					break;
				}
					
				case "Glitter":
				{
					_list.dataProvider = collectionGlitter;
					_listGap = 4;
					_listPadding = 0;
					break;
				}
					
				case "Dye":
				{
					_list.dataProvider = collectionDye;
					_listGap = 4;
					_listPadding = 0;
					break;
				}
				
				case "Accessories":
				{
					_list.dataProvider = collectionAccessories;
					_listGap = 30;
					_listPadding = 24;
					break;
				}
					
				case "Tiara":
				{
					_list.dataProvider = collectionTiaras;
					_listGap = 4;
					_listPadding = 0;
					break;
				}
					
				case "Headband":
				{
					_list.dataProvider = collectionHeadbands;
					_listGap = 4;
					_listPadding = 0;
					break;
				}
					
				case "Bow":
				{
					_list.dataProvider = collectionBows;
					_listGap = 4;
					_listPadding = 0;
					break;
				}
					
				case "Flowers":
				{
					_list.dataProvider = collectionFlowers;
					_listGap = 4;
					_listPadding = 0;
					break;
				}
					
				case "Girls":
				{
					_list.dataProvider = collectionGirls;
					_listGap = 4;
					_listPadding = 0;
					break;
				}
					
				default:
				{
					break;
				}
					
					
			}
			_listLayout.paddingLeft = _listPadding;
			_listLayout.gap = _listGap;
			
			layout();
		}
		
		
		
		protected function layout():void
		{
			_list.width = 414; // width of bar less the main button area on the left
			_list.x = 96;
			_list.height = 72;
			_list.y = 26;
			_list.validate();
		}
		
		
		

		protected function tileListItemRendererFactory():IListItemRenderer
		{
			const renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
			renderer.labelField = "label";
			renderer.iconSourceField = "texture";
			renderer.iconPosition = Button.ICON_POSITION_TOP;
			//renderer.defaultLabelProperties.textFormat = new BitmapFontTextFormat(_font, NaN, 0x000000);
			return renderer;
		}
		
		
		
		

		protected function stage_resizeHandler(event:ResizeEvent):void
		{
			layout();
		}
		
		
	}
}
