package utils.ParticleSystem
{
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.deg2rad;
	
	import utils.MathHelpers;

	public class ParticleEmitter extends Sprite
	{
		//private variables
		private var particleImage:Image; //this is the image were going to be displaying, we only need 1 copy
		private var particleCount:int; //the amount of particles were going to throw
		private var particleArray:Array = new Array(); //array to hold all the particles
		
		private var useGravity:Boolean;
		private var lastTime:Number;
		private var currentTime:Number;
		private var destination:Point = new Point(0,0);
		
		public var GRAVITY:Number = 500;
		public var ROTATE:Number = 5;

		public function ParticleEmitter(image:Image, gravity:Boolean = true)
		{
			//store our image, we can change this to a vector of images and
			//randomly pick among the images to use as part of the vectors, but
			//lets keep it simple for now
			particleImage = image;
			useGravity = gravity;
			lastTime = getTimer();
			addEventListener(Event.ENTER_FRAME, Update);
		}

		public function Update(e:Event):void
		{
			//update the Timer
			currentTime = getTimer();
			var timePassed:Number = (currentTime - lastTime) * 0.001;
			//Update all the particles if any are alive
			if(particleArray.length > 0)
			{
				//go through the list backwards, to prevent fallback on removals
				for(var i:int = particleArray.length -1 ; i >= 0; i--)
				{
					//update the particles position based on trajectory and gravity(if used).
					//also reduce the lifespan
					//this is acceleration, unimplemented at the moment
					//particleArray[i].velocity.x += particleArray[i].velocity.x * timePassed;
					//particleArray[i].velocity.y += particleArray[i].velocity.y * timePassed;

					if(useGravity)
					{
						//if were using gravity, pull the object more downwards
						particleArray[i].velocity.y += (GRAVITY * timePassed);
					}

					if(particleArray[i].useDestination)
					{
						//start to pull the particles towards the destination point
						//by altering their velocities
						//find the new difference in positions
						var deltaX:Number = destination.x - particleArray[i].x;
						var deltaY:Number = destination.y - particleArray[i].y;
						//get the new angle between the 2
						var angle:Number = Math.atan2(deltaY, deltaX);
						//set the new velocities based on the new angle
						var vX:Number = Math.cos(angle) * 500;
						var vY:Number = Math.sin(angle) * 500;
						if(particleArray[i].lifeSpan < 0.5)
						{
							particleArray[i].velocity.x = vX;
							particleArray[i].velocity.y = vY;
						}
						else
						{
							particleArray[i].velocity.x += vX * timePassed;
							particleArray[i].velocity.y += vY * timePassed;
						}

						particleArray[i].x += particleArray[i].velocity.x * timePassed;
						particleArray[i].y += particleArray[i].velocity.y * timePassed;

						if(particleArray[i].x > destination.x - 25 &&
							particleArray[i].x < destination.x + 25 &&
							particleArray[i].y > destination.y - 25 &&
							particleArray[i].y < destination.y + 25)
						{
							KillMe(i);
							continue;
						}
					}
					else
					{
						particleArray[i].x += particleArray[i].velocity.x * timePassed;
						particleArray[i].y += particleArray[i].velocity.y * timePassed;
					}

					//if this is a rotating particle, rotate it
					if(particleArray[i].rotate)
					{
						if(particleArray[i].velocity.x >= 0)
							particleArray[i].rotation += deg2rad(ROTATE);
						else
							particleArray[i].rotation += deg2rad(-ROTATE);
					}
					particleArray[i].lifeSpan -= timePassed;
					if(particleArray[i].lifeSpan < 0.5)
					{
						if(particleArray[i].fadeOut)//start a fade out near the end of it's life
						{
							particleArray[i].alpha -= 2 * timePassed;
						}
						if(particleArray[i].suddenSlow)//start the sudden slow near the end of it's life
						{
							particleArray[i].velocity.x -= particleArray[i].velocity.x * timePassed;
							particleArray[i].velocity.y -= particleArray[i].velocity.y * timePassed;
						}
					}
					if(particleArray[i].lifeSpan <= 0.0 && !particleArray[i].useDestination)
					{
						KillMe(i);
					}
				}
			}

			//set our current time to our last time for next frame
			lastTime = currentTime;
		}

		private function KillMe(index:int):void
		{
			trace("removing particle: " + index);
			removeChild(particleArray[index], true);
			particleArray.splice(index,1);
		}

		//PUBLIC FUNCTIONS
		public function CreateBurst(count:int, forceVarianceX:Number = 100, forceVarianceY:Number = 100, lifeSpan:Number = 1.25, fadeOut:Boolean = false, rotate:Boolean = false):void
		{
			for(var i:int = 0; i < count; i++)
			{
				var velocity:Point = new Point(MathHelpers.randomNumberRange(-forceVarianceX, forceVarianceX), MathHelpers.randomNumberRange(-forceVarianceY, forceVarianceY));
				var particle:TexturedParticle = new TexturedParticle(new Image(particleImage.texture), velocity, lifeSpan);
				if(fadeOut)
				{
					particle.fadeOut = fadeOut;
				}
				particle.rotate = rotate;
				particle.touchable = false;
				addChild(particle);
				particleArray.push(particle);
			}
		}

		public function CreateVerticalBurst(count:int, forceVarianceX:Number = 100, forceVarianceY:Number = 100, lifeSpan:Number = 1.25, fadeOut:Boolean = false, rotate:Boolean = false):void
		{
			for(var i:int = 0; i < count; i++)
			{
				var velocity:Point = new Point(MathHelpers.randomNumberRange(-forceVarianceX, forceVarianceX), MathHelpers.randomNumberRange(forceVarianceY - 100, forceVarianceY + 100));
				var particle:TexturedParticle = new TexturedParticle(new Image(particleImage.texture), velocity, lifeSpan);
				if(fadeOut)
				{
					particle.fadeOut = fadeOut;
				}
				particle.rotate = rotate;
				particle.touchable = false;
				addChild(particle);
				particleArray.push(particle);
			}
		}

		public function CreateHorizontalBurst(count:int, forceVarianceX:Number = 100, forceVarianceY:Number = 100, lifeSpan:Number = 1.25, fadeOut:Boolean = false, rotate:Boolean = false, randSize:Boolean = false, slow:Boolean = false):void
		{
			for(var i:int = 0; i < count; i++)
			{
				var velocity:Point = new Point(MathHelpers.randomNumberRange(forceVarianceX - 100, forceVarianceX + 100) , MathHelpers.randomNumberRange(-forceVarianceY, forceVarianceY));
				var particle:TexturedParticle = new TexturedParticle(new Image(particleImage.texture), velocity, lifeSpan);
				if(fadeOut)
				{
					particle.fadeOut = fadeOut;
				}
				if(randSize)
				{
					var range:Number = MathHelpers.randomNumberRange(0.4, 0.8);
					particle.scaleX = range;
					particle.scaleY = range;
				}
				if(slow)
				{
					particle.suddenSlow = true;
				}
				particle.rotate = rotate;
				particle.touchable = false;
				addChild(particle);
				particleArray.push(particle);
			}
		}
	
		public function CreatePointedBurst(count:int, newDestination:Point, position:Point):void
		{
			for(var i:int = 0; i < count; i++)
			{
				var velocity:Point = new Point(MathHelpers.randomNumberRange(-200, 200), MathHelpers.randomNumberRange(-200, 200));
				var particle:TexturedParticle = new TexturedParticle(new Image(particleImage.texture), velocity, 1);

				particle.fadeOut = false;
				particle.rotate = false;
				particle.touchable = false;
				particle.useDestination = true;
				particle.x = position.x;
				particle.y = position.y;
				particle.scaleX = particle.scaleY = MathHelpers.randomNumberRange(0.3,1);
				addChild(particle);
				particleArray.push(particle);
			}
			destination = newDestination;
		}
	}
}