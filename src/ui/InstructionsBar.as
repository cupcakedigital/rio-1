package ui
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.TextureSmoothing;
	import starling.utils.Color;

	public class InstructionsBar extends Sprite
	{
		private var bg:Image;
		private var textField:TextField;
		private var textFieldDrop:TextField;
		
		public function InstructionsBar(text:String="", fSize:int=46, topOfScreen:Boolean=false)
		{
			this.touchable = false;
			bg = new Image(Root.assets.getTexture("Instructions BG"));
			bg.smoothing = TextureSmoothing.BILINEAR;
			this.addChild(bg);
			
			textField = new TextField(1000,150, text, NutJob.headerFont.fontName);
			textField.color = Color.WHITE;
			textField.fontSize = fSize;
			textField.y = -25;
			textField.hAlign = "center";
			textField.text = text;
			
			textFieldDrop = new TextField(1000,150, text, NutJob.headerFont.fontName);
			textFieldDrop.color = 0x003405;
			textFieldDrop.fontSize = fSize;
			textFieldDrop.x = textField.x+1;
			textFieldDrop.y = textField.y+1;
			textFieldDrop.hAlign = "center";
			textFieldDrop.text = text;
			this.addChild(textFieldDrop);
			this.addChild(textField);
			
			this.x = Costanza.STAGE_WIDTH/2 - this.width/2;
			if(!topOfScreen)
			{
				this.y = Costanza.STAGE_HEIGHT - bg.height;
			}
		}
		
		public function switchText(txt:String):void
		{
			textField.text = txt;
			textFieldDrop.text = txt;
		}
		
		public function destroy():void
		{
			this.removeChild(bg,true);
			this.removeChild(textField,true);
			this.parent.removeChild(this,true);
		}
	}
}