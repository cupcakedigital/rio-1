package ui
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.utils.AssetManager;

	public class Settings extends Sprite
	{
		public static const HIDE_SETTINGS:String = "hideSettings";
		
		private var musicSlider:Image;
		private var soundSlider:Image;
		private var voiceSlider:Image;
		
		private var dragging:Boolean;
		
		private var startX:Number = 200;//min point for slider
		private var endX:Number = 580;//max point for slider
		private var barWidth:Number = endX-startX;

		private var ok_BTN:Image;
		
		public function Settings(assets:AssetManager)
		{
			var bg:Image = new Image(assets.getTexture("Settings_Frame"));
			bg.x = 0;
			bg.y = 0;
			this.addChild(bg);
			
			trace("music" + Costanza.musicVolume + "voice" + Costanza.voiceVolume + "sound" + Costanza.soundVolume);
			
			//music max is 40% of the others
			musicSlider = new Image(assets.getTexture("Slider"));
			musicSlider.x = startX + (barWidth*(Costanza.musicVolume/Costanza.musicVolumeMax));
			musicSlider.y = 145;
			musicSlider.alignPivot();
			musicSlider.name = "music";

			soundSlider = new Image(assets.getTexture("Slider"));
			soundSlider.x = startX + (barWidth*Costanza.soundVolume);
			soundSlider.y = musicSlider.y + musicSlider.height + 60;
			soundSlider.alignPivot();
			soundSlider.name = "sound";

			voiceSlider = new Image(assets.getTexture("Slider"));
			voiceSlider.x = startX + (barWidth*Costanza.voiceVolume);
			voiceSlider.y = soundSlider.y + soundSlider.height + 60;
			voiceSlider.alignPivot();
			voiceSlider.name = "voice";
			
			this.addChild(musicSlider);
			this.addChild(soundSlider);
			this.addChild(voiceSlider);

			//ok button
			ok_BTN = new Image(assets.getTexture("ok_normal"));
			ok_BTN.x = this.width/2 - ok_BTN.width/2;
			ok_BTN.y = this.height - 50;
			this.addChild(ok_BTN);
			ok_BTN.addEventListener(TouchEvent.TOUCH, closeSettings);

			musicSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			soundSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			voiceSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			
		}//end constructor
		
		private function closeSettings(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				trace("hide");
				trace(Rio.savedVariablesObject.data.soundVolume);
				//saves values to sharedobject
				Rio.savedVariablesObject.data.musicVolume = Costanza.musicVolume;
				Rio.savedVariablesObject.data.voiceVolume = Costanza.voiceVolume;
				Rio.savedVariablesObject.data.soundVolume = Costanza.soundVolume;
				
				Rio.savedVariablesObject.flush();
				
				trace("music" + Costanza.musicVolume + "voice" + Costanza.voiceVolume + "sound" + Costanza.soundVolume);
				
				dispatchEventWith(HIDE_SETTINGS,true);
				trace(Rio.savedVariablesObject.data.soundVolume);
			}
		}
		
		private function dragSlider(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			var mc:Image = e.currentTarget as Image;
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				dragging = true;
			}
			else if(touch.phase == TouchPhase.MOVED)
			{
				if(dragging)
				{
					mc.x = touch.globalX - this.x;
					if(mc.x > endX){mc.x = endX;}
					if(mc.x < startX){mc.x = startX}
				}
			}
			else if(touch.phase == TouchPhase.ENDED)
			{
				dragging = false;
				setVolume(mc);
			}
		}//end dragSlider
		
		private function setVolume(mc:Image):void
		{
			switch(mc.name)
			{
				case "music":
				{
					Costanza.musicVolume = ((mc.x - startX)/barWidth)*Costanza.musicVolumeMax;
					trace(mc.x - startX);
					if(mc.x == startX){Costanza.musicVolume=0;}
					break;
				}
				case "sound":
				{
					Costanza.soundVolume = (mc.x - startX)/barWidth;
					if(mc.x == startX){Costanza.soundVolume=0;}
					break;
				}
				case "voice":
				{
					Costanza.voiceVolume = (mc.x - startX)/barWidth;
					if(mc.x == startX){Costanza.voiceVolume=0;}
					break;
				}
			}
		}
	}
}