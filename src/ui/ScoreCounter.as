package ui
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class ScoreCounter extends Sprite
	{
		private var _score:int = 0;
		private var background:Image;
		private var scoreText:TextField;

		public function ScoreCounter(backgroundTexture:Texture)
		{
			super();
			background = new Image(backgroundTexture);
			addChild(background);

			scoreText = new TextField(background.width, background.height,score.toString(),"TikiIsland" ,70,Color.YELLOW, true);
			scoreText.y = -5;
			addChild(scoreText);
			
			score = 0;
		}

		public function get score():int
		{
			return _score;
		}

		public function set score(value:int):void
		{
			_score = value;
			scoreText.text = _score.toString();
		}
	}
}