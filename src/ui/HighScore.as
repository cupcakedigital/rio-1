package ui
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Bounce;
	import com.greensock.easing.Linear;
	
	import flash.net.SharedObject;
	
	import feathers.controls.TextInput;
	import feathers.events.FeathersEventType;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.text.TextField;
	import starling.utils.Color;

	public class HighScore extends Sprite
	{
		public static const RESTART_GAME:String = "restartGame";
		public static const GO_HOME:String = "goHome";

		private var BG:Image;
		private var BG2:Image;
		private var BG3:Image;

		private var gameHS:SharedObject;

		private var scoresArray:Array = [];

		public var risingScore:int = 0;

		private var risingTextScore:TextField;

		private var finalScore:int;

		private var homeBtn:Button;
		private var replayBtn:Button;
		private var forwardBtn:Button;

		private var newScore:Boolean = false;

		private var newScoreIndex:int;

		private var Phase:int = 0;

		//stars
		private var star1:MovieClip;
		private var star2:MovieClip;
		private var star3:MovieClip;

		//private var enterNameText:Image;

		private var inputText:TextInput;

		private var newName:String = "";

		//highscore display
		private var name1:TextField;
		private var score1:TextField;

		private var name2:TextField;
		private var score2:TextField;

		private var name3:TextField;
		private var score3:TextField;

		private var name4:TextField;
		private var score4:TextField;

		private var name5:TextField;
		private var score5:TextField;

		private var CurrentScores:String;

		private var btn1:Image;
		private var btn2:Image;
		private var btn3:Image;
		private var btn4:Image;

		private var skipScoring:Boolean;

		private var soundManager:SoundManager;

		public function HighScore(Game:String, currentScore:int, justDisplay:Boolean=false)
		{
			CurrentScores = Game;
			finalScore = currentScore;
			skipScoring = justDisplay;
			soundManager = SoundManager.getInstance();
			addObjects();
		}

		private function addObjects():void
		{
			if(skipScoring)
			{
				checkSavedData(CurrentScores);

				BG3 = new Image(Root.assets.getTexture("HighScore_BG"));
				this.addChildAt(BG3,0);

				var btn:Button = new Button(Root.assets.getTexture("Home_BTN"));
				btn.x = BG3.width/2 - btn.width/2;
				btn.y = Costanza.STAGE_HEIGHT - btn.height;
				btn.addEventListener(Event.TRIGGERED, homeTap);
				this.addChild(btn);

				declareTextObjects();

				saveHighscores();
				setHighscores();
			}
			else
			{
				BG = new Image(Root.assets.getTexture("Score_BG"));
				this.addChild(BG);

				homeBtn = new Button(Root.assets.getTexture("Home_BTN"));
				homeBtn.x = 450 + Costanza.STAGE_OFFSET;
				homeBtn.y = 850;
				homeBtn.visible = false;
				this.addChild(homeBtn);
				homeBtn.addEventListener(Event.TRIGGERED, homeTap);

				replayBtn = new Button(Root.assets.getTexture("Retry_BTN"));
				replayBtn.x = homeBtn.x + homeBtn.width + 15 ;
				replayBtn.y = 850;
				this.addChild(replayBtn);
				replayBtn.addEventListener(Event.TRIGGERED, replayTap);

				forwardBtn = new Button(Root.assets.getTexture("Next_BTN"));
				forwardBtn.x = Costanza.STAGE_WIDTH/2 - forwardBtn.width/2
				forwardBtn.y = 850;
				this.addChild(forwardBtn);
				forwardBtn.addEventListener(Event.TRIGGERED, forwardTap);

				star1 = new MovieClip(Root.assets.getTextures("star_"));
				star1.loop = false;
				star1.stop();
				star1.visible = false;
				star1.x = 450 + Costanza.STAGE_OFFSET;;
				star1.y = 160;

				star2 = new MovieClip(Root.assets.getTextures("star_"));
				star2.loop = false;
				star2.stop();
				star2.visible = false;
				star2.x = 580 + Costanza.STAGE_OFFSET;;
				star2.y = 160;

				star3 = new MovieClip(Root.assets.getTextures("star_"));
				star3.loop = false;
				star3.stop();
				star3.visible = false;
				star3.x = 710 + Costanza.STAGE_OFFSET;;
				star3.y = 160;

				checkSavedData(CurrentScores);

				DropStars();
			}
		}

		private function checkSavedData(name:String):void
		{
			gameHS = SharedObject.getLocal(name);

			scoresArray = [];

			var tmpScore:int = 0;
			var tmpName:String = "";
			//top score
			if(gameHS.data.highScore1)
			{
				tmpScore = gameHS.data.highScore1;
				tmpName = gameHS.data.name1;
			}
			else
			{
				tmpScore = 0;
				tmpName = "";
				gameHS.data.highScore1 = tmpScore;
				gameHS.data.name1 = tmpName;
			}
			scoresArray.push([tmpScore,tmpName]);

			//second place
			if(gameHS.data.highScore2)
			{
				tmpScore = gameHS.data.highScore2;
				tmpName = gameHS.data.name2;
			}
			else
			{
				tmpScore = 0;
				tmpName = "";
				gameHS.data.highScore2 = tmpScore;
				gameHS.data.name2 = tmpName;
			}
			scoresArray.push([tmpScore,tmpName]);

			//third place
			if(gameHS.data.highScore3)
			{
				tmpScore = gameHS.data.highScore3;
				tmpName = gameHS.data.name3;
			}
			else
			{
				tmpScore = 0;
				tmpName = "";
				gameHS.data.highScore3 = tmpScore;
				gameHS.data.name3 = tmpName;
			}
			scoresArray.push([tmpScore,tmpName]);

			//fourth place
			if(gameHS.data.highScore4)
			{
				tmpScore = gameHS.data.highScore4;
				tmpName = gameHS.data.name4;
			}
			else
			{
				tmpScore = 0;
				tmpName = "";
				gameHS.data.highScore4 = tmpScore;
				gameHS.data.name4 = tmpName;
			}
			scoresArray.push([tmpScore,tmpName]);

			//fifth place
			if(gameHS.data.highScore5)
			{
				tmpScore = gameHS.data.highScore5;
				tmpName = gameHS.data.name5;
			}
			else
			{
				tmpScore = 0;
				tmpName = "";
				gameHS.data.highScore5 = tmpScore;
				gameHS.data.name5 = tmpName;
			}
			scoresArray.push([tmpScore,tmpName]);

			trace(scoresArray);
		}

		private function DropStars():void
		{
			Starling.juggler.add(star1);
			addChild(star1);

			Starling.juggler.add(star2);
			addChild(star2);

			Starling.juggler.add(star3);
			addChild(star3);
			star3.addEventListener(Event.COMPLETE, showScore);

			PlayStar(star1);
			soundManager.playSound("star1", Costanza.soundVolume);
			TweenMax.delayedCall(1, PlayStar,[star2]);
			TweenMax.delayedCall(1, soundManager.playSound, ["star2", Costanza.soundVolume]);
			TweenMax.delayedCall(2, PlayStar,[star3]);
			TweenMax.delayedCall(2, soundManager.playSound, ["star3", Costanza.soundVolume]);
		}

		private function PlayStar(movieClip:MovieClip):void
		{
			movieClip.visible = true;
			movieClip.play();
		}

		private function showScore():void
		{
			risingTextScore = new TextField(400, 200, risingScore.toString(), "TikiIsland", 160, Color.WHITE);
			risingTextScore.hAlign = "center";
			risingTextScore.vAlign = "center";
			risingTextScore.touchable = false;
			risingTextScore.x = Costanza.STAGE_WIDTH/2 - risingTextScore.width/2;
			risingTextScore.y = 410;
			this.addChild(risingTextScore);

			AccumulateScore();
		}

		private function AccumulateScore():void
		{
			risingScore = 0;
			TweenMax.to(this,0.3,{risingScore:finalScore, onUpdate:ShowTmpScore, ease:Linear.easeNone, onComplete:checkHighscore});
		}

		private function checkHighscore():void
		{
			newScore = false;
			for(var i:int = 0; i < scoresArray.length && !newScore; i++)
			{
				if(finalScore > scoresArray[i][0])
				{
					newScore = true;
					newScoreIndex = i;
				}
			}

			TweenMax.allTo([forwardBtn],.5,{y:Costanza.STAGE_HEIGHT - forwardBtn.height, ease:Bounce.easeOut});
		}

		private function ShowTmpScore():void
		{
			risingTextScore.text = risingScore.toString();
		}

		private function forwardTap(e:Event):void
		{
			if(Phase == 0)
			{
				var btn:Button;
				soundManager.playSound("forwardButton", Costanza.soundVolume);

				if(newScore)
				{
					trace("NewScore");
					Phase = 1;
					this.removeChild(BG,true);
					this.removeChild(forwardBtn,true);
					this.removeChild(homeBtn,true);
					this.removeChild(star1);
					Starling.juggler.remove(star1);
					this.removeChild(star2);
					Starling.juggler.remove(star2);
					this.removeChild(star3);
					Starling.juggler.remove(star3);

					BG2 = new Image(Root.assets.getTexture("EnterName_BG"));
					this.addChildAt(BG2,0);

					risingTextScore.y = 180;
					replayBtn.y = 850;

					forwardBtn = new Button(Root.assets.getTexture("Next_BTN"));
					forwardBtn.x = Costanza.STAGE_WIDTH/2 - forwardBtn.width/2;
					forwardBtn.y = Costanza.STAGE_HEIGHT - forwardBtn.height;
					forwardBtn.visible = false;
					forwardBtn.addEventListener(Event.TRIGGERED, forwardTap);
					this.addChild(forwardBtn);

					enterName();
				}
				else//didnt get a new highscore
				{
					Phase = 2;
					if(BG)
					{
						this.removeChild(BG,true);
						//this.removeChild(forwardBtn,true);
						this.removeChild(homeBtn,true);
						this.removeChild(risingTextScore,true);
						this.removeChild(star1);
						Starling.juggler.remove(star1);
						this.removeChild(star2);
						Starling.juggler.remove(star2);
						this.removeChild(star3);
						Starling.juggler.remove(star3);
					}
					BG3 = new Image(Root.assets.getTexture("HighScore_BG"));
					this.addChildAt(BG3,0);

					replayBtn.x = BG3.width/2 - replayBtn.width/2 + 75;
					replayBtn.y = Costanza.STAGE_HEIGHT - replayBtn.height;
					if(forwardBtn){this.removeChild(forwardBtn,true);}

					btn = new Button(Root.assets.getTexture("Home_BTN"));
					btn.x = BG3.width/2 - btn.width/2 - 75;
					btn.y = Costanza.STAGE_HEIGHT - btn.height;
					btn.addEventListener(Event.TRIGGERED, homeTap);
					this.addChild(btn);

					declareTextObjects();

					saveHighscores();
					setHighscores();
				}
			}
			else if(Phase == 1)//new highscore then final page
			{
				soundManager.playSound("forwardButton", Costanza.soundVolume);
				Costanza.lastNameEntered = inputText.text;
				Rio.savedVariablesObject.data.lastNameEntered = Costanza.lastNameEntered;
				Rio.savedVariablesObject.flush();
				Phase = 2;
				this.removeChild(BG2,true);
				this.removeChild(risingTextScore,true);
				BG3 = new Image(Root.assets.getTexture("HighScore_BG"));
				this.addChildAt(BG3,0);

				replayBtn.x = BG3.width/2 - replayBtn.width/2 + 75;
				replayBtn.y = Costanza.STAGE_HEIGHT - replayBtn.height;
				if(forwardBtn){this.removeChild(forwardBtn,true);}

				btn = new Button(Root.assets.getTexture("Home_BTN"));
				btn.x = BG3.width/2 - btn.width/2 - 75;
				btn.y = Costanza.STAGE_HEIGHT - btn.height;
				btn.addEventListener(Event.TRIGGERED, homeTap);
				this.addChild(btn);

				newName = inputText.text;
				this.removeChild(inputText, true);
				this.removeChild(forwardBtn, true);

				if(newScore)
				{
					var arr:Array = [finalScore,newName];
					scoresArray.splice(newScoreIndex,0,arr);
					scoresArray.pop();
				}

				declareTextObjects();

				saveHighscores();
				setHighscores();

				/*var glow:Image = new Image(Root.assets.getTexture("selected"));
				glow.x = 271;
				glow.y = 175 + (newScoreIndex*85);
				this.addChild(glow);*/
			}
		}

		private function saveHighscores():void
		{
			gameHS.data.highScore1 = scoresArray[0][0];
			gameHS.data.name1 = scoresArray[0][1];

			gameHS.data.highScore2 = scoresArray[1][0];
			gameHS.data.name2 = scoresArray[1][1];

			gameHS.data.highScore3 = scoresArray[2][0];
			gameHS.data.name3 = scoresArray[2][1];

			gameHS.data.highScore4 = scoresArray[3][0];
			gameHS.data.name4 = scoresArray[3][1];

			gameHS.data.highScore5 = scoresArray[4][0];
			gameHS.data.name5 = scoresArray[4][1];

			gameHS.flush();
		}

		private function setHighscores():void
		{
			score1.text = scoresArray[0][0];
			name1.text = (scoresArray[0][1] as String).toLocaleUpperCase();

			score2.text = scoresArray[1][0];
			name2.text = (scoresArray[1][1] as String).toLocaleUpperCase();

			score3.text = scoresArray[2][0];
			name3.text = (scoresArray[2][1] as String).toLocaleUpperCase();

			score4.text = scoresArray[3][0];
			name4.text = (scoresArray[3][1] as String).toLocaleUpperCase();

			score5.text = scoresArray[4][0];
			name5.text = (scoresArray[4][1] as String).toLocaleUpperCase();
		}

		private function replayTap(e:Event):void
		{
			soundManager.playSound("forwardButton", Costanza.soundVolume);
			dispatchEventWith(RESTART_GAME, true);
			//destroy();
		}

		private function homeTap(e:Event):void
		{
			soundManager.playSound("backwardButton", Costanza.soundVolume);
			if(skipScoring)
			{
				destroy();
			}
			else
			{
				dispatchEventWith(GO_HOME, true);
			}
		}

		private function enterName():void
		{
			inputText = new TextInput();
			inputText.maxChars = 12;
			inputText.height = 160;
			inputText.width = 600;
			inputText.textEditorProperties.fontFamily = "TikiIsland";
			inputText.textEditorProperties.fontSize = 100;
			inputText.textEditorProperties.color =  Color.BLACK;
			inputText.textEditorProperties.textAlign = "center";
			if ( CONFIG::MARKET == "itunes" ) { inputText.restrict = "a-zA-Z"; }
			inputText.text = Costanza.lastNameEntered;
			inputText.addEventListener(FeathersEventType.ENTER,hideKeyboard);
			inputText.addEventListener(FeathersEventType.FOCUS_IN, hideText);
			this.addChild(inputText);
			inputText.x = Costanza.STAGE_WIDTH/2 - inputText.width/2;
			inputText.y = 465;
			inputText.addEventListener(FeathersEventType.FOCUS_OUT, saveText);

			//enterNameText = new Image(Root.assets.getTexture("Name_Prompt"));
			//enterNameText.x = 360;
			//enterNameText.y = 420;
			//enterNameText.visible = enterNameText.touchable = (inputText.text == "");
			//this.addChild(enterNameText);
			//enterNameText.addEventListener(TouchEvent.TOUCH, tapText);	

			forwardBtn.touchable = forwardBtn.visible = (inputText.text != "");
		}

		private function hideText():void
		{
			//enterNameText.visible = enterNameText.touchable = false;
		}

		private function tapText(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				//enterNameText.visible = enterNameText.touchable = false;
				//enterNameText.y = 800;
				inputText.y = 400;
				inputText.setFocus();
			}
		}

		private function hideKeyboard():void
		{
			Starling.current.nativeStage.focus = null;
		}

		private function saveText():void
		{
			if(inputText.text == "")
			{
				//enterNameText.y = 420;
				//enterNameText.visible = enterNameText.touchable = true;
				forwardBtn.visible = forwardBtn.touchable = false;
			}
			else
			{
				forwardBtn.visible = forwardBtn.touchable = true;
				inputText.text = upperCase(inputText.text);
			}
		}

		private function upperCase(str:String) : String {
			var firstChar:String = str.substr(0, 1); 
			var restOfString:String = str.substr(1, str.length); 
			
			return firstChar.toUpperCase()+restOfString.toLowerCase(); 
		}

		private function declareTextObjects():void
		{
			name1 = new TextField(1000, 100, "Your Name", "TikiIsland", 60, Color.WHITE);
			name1.vAlign = "center";
			name1.hAlign = "left";
			name1.x = 435 + Costanza.STAGE_OFFSET;;
			name1.y = 245;
			this.addChild(name1);

			score1 = new TextField(800, 100, "Your Name", "TikiIsland", 60, 0xE7B351);
			score1.vAlign = "center";
			score1.hAlign = "left";
			score1.x = 880 + Costanza.STAGE_OFFSET;;
			score1.y = 245;
			this.addChild(score1);

			name2 = new TextField(1000, 100, "Your Name", "TikiIsland", 60, Color.WHITE);
			name2.vAlign = "center";
			name2.hAlign = "left";
			name2.x = 435 + Costanza.STAGE_OFFSET;;
			name2.y = 320;
			this.addChild(name2);

			score2 = new TextField(800, 100, "Your Name", "TikiIsland", 60, 0xE7B351);
			score2.vAlign = "center";
			score2.hAlign = "left";
			score2.x = 880 + Costanza.STAGE_OFFSET;;
			score2.y = 320;
			this.addChild(score2);

			name3 = new TextField(1000, 100, "Your Name", "TikiIsland", 60, Color.WHITE);
			name3.vAlign = "center";
			name3.hAlign = "left";
			name3.x = 435 + Costanza.STAGE_OFFSET;;
			name3.y = 395;
			this.addChild(name3);

			score3 = new TextField(800, 100, "Your Name", "TikiIsland", 60, 0xE7B351);
			score3.vAlign = "center";
			score3.hAlign = "left";
			score3.x = 880 + Costanza.STAGE_OFFSET;;
			score3.y = 395;
			this.addChild(score3);

			name4 = new TextField(1000, 100, "Your Name", "TikiIsland", 60, Color.WHITE);
			name4.vAlign = "center";
			name4.hAlign = "left";
			name4.x = 435 + Costanza.STAGE_OFFSET;;
			name4.y = 470;
			this.addChild(name4);

			score4 = new TextField(800, 100, "Your Name", "TikiIsland", 60, 0xE7B351);
			score4.vAlign = "center";
			score4.hAlign = "left";
			score4.x = 880 + Costanza.STAGE_OFFSET;;
			score4.y = 470;
			this.addChild(score4);

			name5 = new TextField(800, 100, "Your Name", "TikiIsland", 60, Color.WHITE);
			name5.vAlign = "center";
			name5.hAlign = "left";
			name5.x = 435 + Costanza.STAGE_OFFSET;;
			name5.y = 542;
			this.addChild(name5);

			score5 = new TextField(1000, 100, "Your Name", "TikiIsland", 60, 0xE7B351);
			score5.vAlign = "center";
			score5.hAlign = "left";
			score5.x = 880 + Costanza.STAGE_OFFSET;;
			score5.y = 542;
			this.addChild(score5);

			setScoreButtons();
		}

		private function setScoreButtons():void
		{
			btn1 = new Image(Root.assets.getTexture("BTNover_1"));
			btn1.name = "BlusRescueEasy";
			btn1.x = 303 + Costanza.STAGE_OFFSET;;
			btn1.y = 150;
			if(btn1.name != CurrentScores){btn1.alpha=0;}
			this.addChild(btn1);

			btn2 = new Image(Root.assets.getTexture("BTNover_2"));
			btn2.name = "BlusRescueHard";
			btn2.x = btn1.x + btn1.width + 3;
			btn2.y = 150;
			if(btn2.name != CurrentScores){btn2.alpha=0;}
			this.addChild(btn2);

			btn3 = new Image(Root.assets.getTexture("BTNover_3"));
			btn3.name = "JewelsGems";
			btn3.x = btn2.x + btn2.width + 2;
			btn3.y = 150;
			if(btn3.name != CurrentScores){btn3.alpha=0;}
			this.addChild(btn3);

			/*btn4 = new Image(Root.assets.getTexture("BTNover_4"));
			btn4.name = "GoneNuts";
			btn4.x = 898;
			btn4.y = 121;
			if(btn4.name != CurrentScores){btn4.alpha=0;}
			this.addChild(btn4);*/

			btn1.addEventListener(TouchEvent.TOUCH,loadingScore);
			btn2.addEventListener(TouchEvent.TOUCH,loadingScore);
			btn3.addEventListener(TouchEvent.TOUCH,loadingScore);
			//btn4.addEventListener(TouchEvent.TOUCH,loadingScore);
		}

		public function destroy():void
		{
			//clear assets
			this.removeChildren(0, this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}

		private function loadingScore(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				btn1.alpha = btn2.alpha = btn3.alpha = 0;
				(e.currentTarget as Image).alpha = 1;
				//soundManager.playSound("click", Costanza.soundVolume);
				switch((e.currentTarget as Image).name)
				{
					case "BlusRescueEasy":
					{
						if(CurrentScores != "BlusRescueEasy")
						{
							CurrentScores = "BlusRescueEasy";
							checkSavedData(CurrentScores);
							setHighscores();
						}
						break;
					}
					case "BlusRescueHard":
					{
						if(CurrentScores != "BlusRescueHard")
						{
							CurrentScores = "BlusRescueHard";
							checkSavedData(CurrentScores);
							setHighscores();
						}
						break;
					}
					case "JewelsGems":
					{
						if(CurrentScores != "JewelsGems")
						{
							CurrentScores = "JewelsGems";
							checkSavedData(CurrentScores);
							setHighscores();
						}
						break;
					}
				}
			}
		}
	}
}