package ui
{
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.extensions.SoundManager;

	public class GreatJob extends Sprite
	{
		private var bg:Image;
		private var button:Button;
		
		public function GreatJob()
		{
			var sm:SoundManager = SoundManager.getInstance();
			
			bg = new Image(Root.assets.getTexture("EndOfLevel_Background"));
			
			addChild(bg);
			
			button = new Button(Root.assets.getTexture("EndOfLevel_Arrow"));
			button.x = bg.x + bg.width - 40;
			button.y = bg.y + bg.height/2;
			
			addChild(button);
			
			sm.playSound("End",Costanza.soundVolume);
		}
	}
}