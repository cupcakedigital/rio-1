package ui
{
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.Color;

	public class PlayAgainPopup extends Sprite
	{
		public static const Reset_Game:String = "resetgame";
		public static const QuitGame:String = "quitgame";
		
		public function PlayAgainPopup(text:String="Play Again?")
		{
			var bg:Image = new Image(Root.assets.getTexture("Dialog_Button_Narrow_h"));
			this.addChild(bg);
			
			var title:TextField = new TextField(500,200,"Play Again?", "VeggieFont",BitmapFont.NATIVE_SIZE,Color.WHITE);
			title.x = -10;
			title.y = -50;
			title.touchable = false;
			
			var yes_BTN:Button = new Button(Root.assets.getTexture("Dialog_Button_Normal_s"));
			yes_BTN.scaleX = yes_BTN.scaleY = .8;
			yes_BTN.x = 50;
			yes_BTN.y = bg.height - yes_BTN.height - 15;
			yes_BTN.addEventListener(Event.TRIGGERED, quitGame);
			this.addChild(yes_BTN);
			
			var yesTxt:TextField = new TextField(250,200,"Yes","VeggieFont",BitmapFont.NATIVE_SIZE,Color.WHITE);
			yesTxt.touchable = false;
			yesTxt.y = 25;
			this.addChild(yesTxt);
			
			var no_BTN:Button = new Button(Root.assets.getTexture("Dialog_Button_Normal_s"));
			no_BTN.scaleX = no_BTN.scaleY = .8;
			no_BTN.x = bg.width - no_BTN.width - 60;
			no_BTN.y = bg.height - yes_BTN.height - 15;
			no_BTN.addEventListener(Event.TRIGGERED, resetGame);
			this.addChild(no_BTN);
			
			var noTxt:TextField = new TextField(250,200,"No","VeggieFont",BitmapFont.NATIVE_SIZE,Color.WHITE);
			noTxt.touchable = false;
			noTxt.x = 220;
			noTxt.y = 25;
			this.addChild(noTxt);
			
			this.addChild(title);
		}
		
		private function quitGame():void
		{
			trace("reset");
			dispatchEventWith(Reset_Game,true);
		}
		
		private function resetGame():void
		{
			trace("quit");
			dispatchEventWith(QuitGame,true);
		}
		
		public function destroy():void
		{
			this.removeChildren(0,this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}
	}
}