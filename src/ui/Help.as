package ui
{
	import flash.desktop.NativeApplication;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.text.TextField;
	import starling.textures.Texture;

	public class Help extends Sprite
	{
		private var soundmanager:SoundManager;
		
		private var HelpImg:Image;
		private var CreditsImg:Image;
		private var buildText:TextField;
		
		public function Help(creditsImg:Texture, helpImg:Texture)
		{
			HelpImg = new Image(helpImg);
			this.addChild(HelpImg);
			
			var creditsBTN:Quad = new Quad(250,100);
			creditsBTN.alpha = 0;
			creditsBTN.x = 520 + Costanza.STAGE_OFFSET;
			creditsBTN.y = 40;
			this.addChild(creditsBTN);
			creditsBTN.addEventListener(TouchEvent.TOUCH, showCredits);

			CreditsImg = new Image(creditsImg);
			this.addChild(CreditsImg);
			CreditsImg.visible = false;
			CreditsImg.touchable = false;
			
			var closeBTN:Quad = new Quad(100,100);
			closeBTN.alpha = 0;
			closeBTN.x = 1080 + Costanza.STAGE_OFFSET;
			closeBTN.y = 8;
			this.addChild(closeBTN);
			closeBTN.addEventListener(TouchEvent.TOUCH,closeTap);
			
			buildText = new TextField(200, 50, "ver. " + getAppVersion());
			buildText.x = 200;
			buildText.y = 675;
			buildText.visible = false;
			this.addChild(buildText);
		}

		private function showCredits(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				CreditsImg.visible = true;
				CreditsImg.touchable = true;
				buildText.visible = true;
			}
		}
		
		private function closeTap(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(CreditsImg.visible)
				{
					CreditsImg.visible = false;
					CreditsImg.touchable = false;
					buildText.visible = false;
				}
				else
				{
					//goBack
					destroy();
				}
			}
		}

		private function getAppVersion():String
		{
			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			var appVersion:String = appXml.ns::versionNumber[0];
			return appVersion;
		}

		public function destroy():void
		{
			//soundManager.removeSound("WidgetTheme");
			this.removeChildren(0,this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}
	}
}