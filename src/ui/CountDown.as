package ui
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Elastic;
	
	import starling.display.Sprite;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.deg2rad;

	public class CountDown extends Sprite
	{
		private var array:Array = [];
		private var txt:TextField;
		
		public function CountDown(finalWord:String="Go!",num:int=3, fontSize:int=BitmapFont.NATIVE_SIZE)
		{
			for(var i:int=num;i>0;i--)
			{
				array.push(i.toString());
			}
			finalWord = finalWord.toUpperCase();
			array.push(finalWord);
			
			txt = new TextField(1000,400,"","NJFont",fontSize,Color.WHITE);
			txt.text = array.splice(0,1);
			txt.pivotX = txt.width/2;
			txt.pivotY = txt.height/2;
			txt.alpha = 0;
			txt.scaleX = .5;
			txt.scaleY = .5;
			addChild(txt);
		}
		
		public function start(show:Boolean=true, delayStart:Number=0):void
		{
			if(show)
			{
				TweenMax.to(txt,.4, {delay:delayStart, scaleX:1, scaleY:1,alpha:1,ease:Elastic.easeOut, onComplete:start, onCompleteParams:[false]});
			}
			else
			{
				TweenMax.to(txt,.4, {delay:delayStart, scaleX:.5, scaleY:.5,alpha:0, onComplete:setText});
			}
		}
		
		private function setText():void
		{
			txt.text = array.splice(0,1);
			
			if(array.length == 0)
			{
				TweenMax.delayedCall(.5,spinWord);
			}
			else
			{
				TweenMax.delayedCall(.3,start);
			}
		}
		
		private function spinWord(show:Boolean=true):void
		{
			if(show)
			{
				TweenMax.to(txt,.3, {scaleX:1, scaleY:1,alpha:1,ease:Elastic.easeOut});
				TweenMax.delayedCall(.6,spinWord,[false]);
			}
			else
			{
				TweenMax.to(txt,.4, {scaleX:.4, scaleY:.4});
				TweenMax.to(txt,.8,{alpha:0,rotation:deg2rad(360), onComplete:done});
			}
		}
		
		private function done():void
		{
			dispatchEventWith("count_done",true);
			destroy();
		}
		
		public function destroy():void
		{
			this.removeChildren(0,this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}
	}
	
}