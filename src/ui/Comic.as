package ui
{
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.filesystem.File;
	import flash.system.Capabilities;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;

	public class Comic extends Sprite
	{
		public static const HIDE_COMIC:String = "hidecomic";
		public static const RESTART:String = "restart";
		
		private var comicAssets:AssetManager;
		private var appDir2:File = File.applicationDirectory;
		private var soundmanager:SoundManager;
		
		private var comicImg:Image;
		private var skip:Button;
		
		private var skipGlow:Image;
		
		//names for img and sound
		private var soundName:String;
		private var imgName:String;
		private var showskip:Boolean;
		
		private var ending:Boolean = false;
		
		public function Comic(imgString:String, soundString:String, showSkip:Boolean=false, end:Boolean=false)
		{
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;

			ending = end;
			soundName = soundString;
			imgName = imgString;
			showskip = showSkip;

			comicAssets = new AssetManager();
			Root.currentAssetsProxy = comicAssets;
			comicAssets.verbose = Capabilities.isDebugger;

			comicAssets.enqueue(
				appDir2.resolvePath("textures/LoadScreens/" + imgName + ".png"),
				appDir2.resolvePath("audio/ComicIntros/" + soundName + ".mp3")
			);

			//dispatchEventWith(HIDE_COMIC,true);

			comicAssets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					addObjects();
				};
			});
		}

		private function addObjects():void
		{
			soundmanager = SoundManager.getInstance();

			soundmanager.addSound(soundName,comicAssets.getSound(soundName));
			soundmanager.playSound(soundName,Costanza.voiceVolume);

			comicImg = new Image(comicAssets.getTexture(imgName));
			this.addChild(comicImg);

			skip = new Button(Root.assets.getTexture("Play"),"",Root.assets.getTexture("Play_Down"));
			skip.x = 1000;
			skip.y = 660;
			skip.touchable = skip.visible = showskip;
			this.addChild(skip);
			skip.addEventListener(Event.TRIGGERED,skipComic);
			
			skipGlow = new Image(Root.assets.getTexture("Play_Down"));
			skipGlow.x = skip.x;
			skipGlow.y = skip.y;
			skipGlow.touchable = false;
			skipGlow.alpha = 0;
			this.addChild(skipGlow);

			if(!ending){dispatchEventWith(Root.SCENE_LOADED,true);}
		}

		public function showSkip():void
		{
			skip.visible = true;
			skip.touchable = true;
			TweenMax.delayedCall(1, Glow);
		}

		private function Glow():void
		{
			TweenMax.to(skipGlow,0.5,{alpha:1, yoyo:true, repeat:100});
		}

		private function skipComic(e:Event):void
		{
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
			
			if(soundmanager.soundIsPlaying(soundName))
				soundmanager.tweenVolume(soundName,0,0.1);
			
			if(ending){dispatchEventWith(RESTART,true);}
			else{dispatchEventWith(HIDE_COMIC,true);destroy();}
		}

		public function destroy():void
		{
			TweenMax.killTweensOf(skipGlow);
			comicAssets.dispose();
			//soundManager.removeSound("WidgetTheme");
			this.removeChildren(0,this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}
	}
}