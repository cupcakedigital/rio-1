package ui
{
	import feathers.controls.Button;
	import feathers.controls.TextInput;
	import feathers.events.FeathersEventType;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.Color;

	public class Certificate extends Sprite
	{
		private var bg:Image;
		private var continueBTN:Button;
		private var EmailBtn:Button;
		private var PhotoBtn:Button;
		
		private var text:TextInput;
		
		public static const CLOSE_CERT:String = "closeCertificate";
		
		public function Certificate()
		{	
			bg = new Image(Root.assets.getTexture("CERTIFICATE"));
			addChild(bg);
			
			continueBTN = new Button();
			continueBTN.defaultSkin = new Image(Root.assets.getTexture("Net"));
			continueBTN.downSkin = new Image(Root.assets.getTexture("Next_Over"));
			continueBTN.addEventListener(Event.TRIGGERED, goForward);
			continueBTN.x = bg.width - 100;
			continueBTN.y = 25;
			addChild(continueBTN);
			
			EmailBtn = new Button();
			EmailBtn.defaultSkin = new Image(Root.assets.getTexture("cert_Email"));
			EmailBtn.addEventListener(Event.TRIGGERED, startEmail);
			EmailBtn.x = bg.width/2 + EmailBtn.width;
			EmailBtn.y = bg.height - 140;
			addChild(EmailBtn);
			
			PhotoBtn = new Button();
			PhotoBtn.defaultSkin = new Image(Root.assets.getTexture("cert_Photo"));
			PhotoBtn.addEventListener(Event.TRIGGERED, takePic);
			PhotoBtn.x = 400;
			PhotoBtn.y = bg.height - 140;
			addChild(PhotoBtn);
			
			text = new TextInput();
			text.maxChars = 15;
			text.height = 80;
			text.width = 600;
			text.textEditorProperties.fontFamily = "CoopFlaired";
			text.textEditorProperties.fontSize = 60;
			text.textEditorProperties.color =  Color.GREEN;
			text.textEditorProperties.textAlign = "center";
			text.restrict = "a-zA-Z";
			text.text = "Your Name";
			text.addEventListener(FeathersEventType.FOCUS_IN,clearText);
			text.addEventListener(FeathersEventType.ENTER,hideKeyboard);
			addChild(text);
			text.x = 200;
			text.y = 380;
		}
		
		private function hideKeyboard():void
		{
			Starling.current.nativeStage.focus = null;
		}
		
		private function clearText():void
		{
			text.text = "";
		}
		
		private function takePic():void
		{
			
		}
		
		private function startEmail():void
		{
			
		}
		
		private function goForward():void
		{
			trace("close");
			dispatchEventWith(CLOSE_CERT,true);
		}
		
		public function destroy():void
		{
			removeChildren(0,numChildren,true);
			this.parent.removeChild(this,true);
		}
	}
}