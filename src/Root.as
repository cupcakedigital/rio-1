package
{
	import com.cupcake.DeviceInfo;
	import com.greensock.TimelineMax;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.system.System;
	
	import games.SideScroller.SideScroller;
	import games.bejeweled.SceneBejeweled;
	
	import scenes.Menu;
	import scenes.Splash_Video;
	import scenes.ColoringBook.Coloring_Book_Scene;
	import scenes.Pages.PageManager;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.filters.WarpFilter;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	import utils.SkinnableProgressBar;

    /** The Root class is the topmost display object in your game. 
	 *  It is a general scene switcher and will contain any globally accessable items liek menus and transition screens.
     *  It listens for bubbled events from the scenes to trigger events.
	 *  Keep this class rather lightweight: it controls the high level behaviour of your game. */
    public class Root extends Sprite
    {
        private static var sAssets:AssetManager;
		public static var currentAssetsProxy:AssetManager;
		
        private var mSceneContainer:Sprite;
		private var mActiveScene:Sprite;
		private var mOldScene:Sprite;
        private var soundManager:SoundManager;

		private var splashTimeline:TimelineMax;

		private var splashWubbzy:Image;

		private var progressBar:SkinnableProgressBar;
		
		private var assetsLoaded:Boolean		= false;
		private var assetsLoading:Boolean		= false;

		private var transitionImage:Image;
		private var transitionTexture:Texture;
		private var tranData:BitmapData;
		private var tranWarpFilter:WarpFilter;

		public static const LOAD_LOBBY:String	= "loadLobby";
		public static const LOAD_MENU:String	= "loadMenu";
		public static const DISPOSE:String		= "dispose";
		public static const SCENE_LOADED:String	= "sceneLoaded";
		public static const LOAD_STORY:String	= "loadStory";
		public static const LOAD_COLOR:String	= "loadColor";

		private var splashCupcakeImage:Button;
		
		//embedded load bar textures
		[Embed(source="../assets/textures/LoadBar/load_1.png")]
		private static var load_1:Class;
		
		[Embed(source="../assets/textures/LoadBar/load_2.png")]
		private static var load_2:Class;

		public function Root()
		{
			addEventListener(Menu.LOAD_ROOM,	onLoadRoom);
			addEventListener(LOAD_LOBBY,		onLoadLobby);
			addEventListener(SCENE_LOADED,		onSceneLoaded);
			addEventListener(LOAD_COLOR,		onLoadColoring);
			addEventListener(LOAD_MENU,			onLoadStaticMenu);
			
			DeviceInfo.Init();
			// not more to do here -- Startup will call "start" immediately.
		}

		public function start(background:Texture, assets:AssetManager):void
		{
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE, onAppActivate);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE, onAppDeactivate);
			// Initialize UI Theme (Feathers,  need full tempalte for use)
			// new MetalWorksMobileTheme();

			// the asset manager is saved as a static variable; this allows us to easily access
			// all the assets from everywhere by simply calling "Root.assets"
			sAssets = assets;

			soundManager = SoundManager.getInstance();
			
			splashWubbzy = new Image(background);
			
			addChild(splashWubbzy);

			progressBar = new SkinnableProgressBar(Texture.fromBitmap(new load_1),Texture.fromBitmap(new load_2),5,5);
			progressBar.x = (Costanza.STAGE_WIDTH/2)  - (progressBar.width/2);
			progressBar.y = Costanza.STAGE_HEIGHT * 0.75;
			addChild(progressBar);

			assetsLoading = true;
            assets.loadQueue( onProgress );
        }
		
		private function onProgress(ratio:Number):void
		{
			progressBar.ratio = ratio;
			
			// a progress bar should always show the 100% for a while,
			// so we show the main menu only after a short delay. 
			
			if (ratio == 1){
				// Remove the event listeners so we don't reload the entire queue every time we leave/return
				NativeApplication.nativeApplication.removeEventListener(flash.events.Event.ACTIVATE, onAppActivate);
				NativeApplication.nativeApplication.removeEventListener(flash.events.Event.DEACTIVATE, onAppDeactivate);
				
				Starling.juggler.delayCall(function():void
				{
					load_1 = load_2 = null;
					progressBar.removeFromParent(true);
					assetsLoaded = true;
					trace("Loading Menu");
					
					/*
					splashCupcakeImage = new Button(Root.assets.getTexture("cupcakedigital_splash"));
					splashCupcakeImage.alpha = 0;
					splashCupcakeImage.scaleWhenDown = 1;
					addChild(splashCupcakeImage);
					*/
					
					//create the timeline and add an onComplete call to myFunction when the timeline completes
					splashTimeline = new TimelineMax({onComplete:loadMenu});
					
					//add a tween
					splashTimeline.append(new TweenMax(splashWubbzy, 0.5, {alpha:0,delay:0.5, onComplete:removeChild, onCompleteParams:[splashWubbzy,true]}));
					//splashTimeline.append(new TweenMax(splashCupcakeImage, 0.5, {alpha:1}));
					//splashTimeline.append(new TweenMax(splashCupcakeImage, 0.5, {alpha:0,delay:1, onComplete:removeChild, onCompleteParams:[splashCupcakeImage,true]}));
					
					addEventListener(starling.events.Event.TRIGGERED, skipLoaders);
					
					function skipLoaders(evt:starling.events.Event):void{
						
						var target:Button = evt.currentTarget as Button;
						splashTimeline.timeScale += 0.5;
					}
				}, 0.15);
			};
		}

		private function onAppActivate(e:flash.events.Event):void
		{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(!assetsLoaded && !assetsLoading)
			{
				assetsLoading = true;
				var appDir:File = File.applicationDirectory;
				assets.enqueue(
					appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Global"),
					appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/HighScore"),
					appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/promo"),
					appDir.resolvePath("fonts")
				);
				
				assets.loadQueue( onProgress );
			}
		}

		private function onAppDeactivate(e:flash.events.Event):void
		{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assetsLoading = false;
				assets.purgeQueue();
			}
		}

		private function loadMenu():void
		{
			mSceneContainer = new Sprite();
			addChild(mSceneContainer);

			transitionImage = new Image(Root.assets.getTexture("loading"));
			transitionImage.alpha = 0;
			transitionImage.touchable = false;
			addChild(transitionImage);

			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP ) { showStaticScene(Menu,true); }
				else 
				{ showScene(Splash_Video,false); }

			// now would be a good time for a clean-up 
			System.pauseForGCIfCollectionImminent(0);
			System.gc();
		}

		public function transitionScene(pageNum:int):void
		{
			trace("Page Number: " + pageNum);
			if(pageNum == -1)//default screen
			{
				transitionImage.x = 0;
				transitionImage.y = 0;
				transitionImage.alpha = 0;
				transitionImage.touchable = true;
				
				TweenMax.to(transitionImage,0.15,{alpha:1});
			}
		}

		private function onSceneLoaded(event:starling.events.Event):void
		{
			trace("SCENE LOADED");
			
			TweenMax.to(transitionImage,0.1,{alpha:0, delay:0.4, onComplete:hideTransition});
			//TweenMax.to(transitionImage,0.2,{alpha:0, onComplete:hideTransition});
			function hideTransition():void
			{
				trace( "MAKING TRANSITION IMAGE UNTOUCHABLE" );
				transitionImage.touchable = false;
			}
		}

        private function onLoadRoom(event:starling.events.Event, room:int = 0):void
        {
            trace("Switching Scenes");
			
			switch(room)
			{
				case 0://character room
				{
					showScene(SideScroller,true);
					break;
				}
				case 1://food making room
				{
					//showScene(MakeFood, currentCharacter);
					showScene(PageManager,true);
					break;
				}
				case 2:
				{
					//showScene(FeedingRoom, currentCharacter);
					showScene(SceneBejeweled,true);
					break;
				}
				case 3:
				{
					//showScene(MatchingRoom, currentCharacter);
					break;
				}
				case 4:
				{
					//showScene(CleanUp, currentCharacter);
					break;
				}
			}
		}

		private function onLoadLobby(event:starling.events.Event):void
		{
			trace("Loading Main Lobby");
			showScene(Menu, true);
		}
		
		private function onLoadStaticMenu(event:starling.events.Event):void
		{
			showStaticScene(Menu, true);
		}
		
		private function onLoadStory(event:starling.events.Event):void
		{
			showScene(PageManager, false);
		}

		private function onLoadColoring(event:starling.events.Event):void
		{
			trace("Loading Coloring in ");
			
			showScene(Coloring_Book_Scene, false);
		}

		public function showScene(screen:Class, transition:Boolean = true, comicPage:int=-1):void
		{
			trace("ShowScene");
			if(transition)
			{
				transitionScene(comicPage);
			};
			
			if(comicPage == -1)//if there isnt a comic page to show before a game
			{
				addPage(screen);
			}
		}

		public function showStaticScene(screen:Class, transition:Boolean = true, comicPage:int=-1):void
		{
			trace("ShowScene");
			if(transition)
			{
				transitionScene(comicPage);
			}

			if(comicPage == -1)//if there isnt a comic page to show before a game
			{
				addStaticPage(screen);
			}
		}

		private function addPage(screen:Class,num:int=-1):void
		{
			if (mActiveScene)
			{
				mOldScene = mActiveScene;
				mOldScene.touchable = false;
				TweenMax.delayedCall(1.5, KillPage);
			}
			Starling.juggler.delayCall(function():void
			{
				if(num > -1){mActiveScene = new screen(num);}
				else
				{
					mActiveScene = new screen(num);
					mActiveScene.touchable = false;
				}
				mActiveScene.x = 1366;
				TweenMax.to(mActiveScene, 1, {x:0 });
				mSceneContainer.addChild(mActiveScene);
			},0.2);
		}

		private function KillPage():void
		{
			trace("Destroying old Scene");
			mActiveScene.touchable = true;
			mOldScene.broadcastEventWith(DISPOSE);
			mOldScene.removeFromParent(true);
		}

		private function addStaticPage(screen:Class,num:int=-1):void
		{
			if (mActiveScene)
			{
				mActiveScene.broadcastEventWith(DISPOSE);
				mActiveScene.removeFromParent(true);
			}
			Starling.juggler.delayCall(function():void
			{
				if(num > -1){mActiveScene = new screen(num);}
				else
				{
					mActiveScene = new screen(num);
				}
				mSceneContainer.addChild(mActiveScene);
			},0.2);
		}

		public static function get assets():AssetManager { return sAssets; }
	}
}