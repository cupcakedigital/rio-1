﻿package games.bejeweled
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Cubic;
	import com.greensock.plugins.BezierPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.utils.getTimer;
	
	import dragonBones.Armature;
	import dragonBones.Bone;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.SoundManager;
	import starling.extensions.pixelmask.PixelMaskDisplayObject;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.Color;
	
	import ui.HighScore;
	import ui.ScoreCounter;

	public class SceneBejeweled extends Sprite 
	{
		// Assets
		private var assets:AssetManager;
		private var soundManager:SoundManager;
		private var appDir:File = File.applicationDirectory;

		// Standard display
		private var stageWidth:int;
		private var stageHeight:int;

		// Scene Objects
		private var sceneRoot:Sprite;
		
		private var assetsLoaded:Boolean = false;

		// main room assets
		private var bg:Image;
		private var board:Image;
		private var OL_1:Image;
		private var OL_2:Image;
		private var greatJob:Image;

		// Main game
		private var bejeweled:Bejeweled;

		private var bejeweledOffset:Point = new Point(515,60);

		// UI Assets
		private var backButton:Button;

		//Score
		public var score:int;
		public var tmpScore:int;
		private var changingScore:int;
		private var risingScore:int;
		private var multiplier:int;
		private var tmpMultiplier:int;

		// Tree objects
		private var bulbMoverBlue:Texture;
		private var bulbMoverPurple:Texture;
		private var bulbMoverRed:Texture;
		private var bulbMoverYellow:Texture;
		private var bulbMoverNut6:Texture;
		private var moverGemRound:Texture;

		private var movingBulb:Boolean = false;
		private var movingToPoint:Point = new Point(330,302);

		//RAT
		private var hand:Image;

		// Particle effects
		private var moveParticles:PDParticleSystem;
		private var starParticles:PDParticleSystem;
		private var resetParticles:PDParticleSystem;
		private var addParticles:PDParticleSystem;

		// Text Messages
		private var messageText:TextField;

		private var pageNum:int;

		public var firstTurn:Boolean = true;

		private var gameOver:Boolean = false;
		
		private var scoreCounter:ScoreCounter;

		//Dragonbones Content
		private var factory:StarlingFactory;
		private var jewelArmature:Armature;

		private var jewelClip:Sprite;

		private var starMeterEmpty:Image;
		private var starMeterFull:Image;
		private var starMeterText:Image;
		private var starMeterClip:Sprite;
		private var starMeterClipRect:Rectangle = new Rectangle(-160,182,330*Costanza.SCALE_FACTOR,400*Costanza.SCALE_FACTOR);

		private var starAmount:int = 10;
		private var starCount:int = 0;

		private var jewelAnimNum:int = 1;

		private var maskedJewel:PixelMaskDisplayObject;

		private var fillMask:MovieClip;
		private var tweening:Boolean = false;

		public function SceneBejeweled(num:int)
		{
			// Create main scene root
			sceneRoot = new Sprite();
			addChild(sceneRoot);

			//Scene Inited, listen for dipose now
			addEventListener(Root.DISPOSE,  disposeScene);
			addEventListener(HighScore.GO_HOME,  GameOver);
			addEventListener(HighScore.RESTART_GAME,  TouchReset);

			// Active tween plugin
			TweenPlugin.activate([BezierPlugin]);

			// Create new asset manager and load content
			assets = new AssetManager(Costanza.SCALE_FACTOR,Costanza.mipmapsEnabled);
			Root.currentAssetsProxy = assets;

			assets.verbose = Capabilities.isDebugger;
			assets.enqueue(
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/gameBejeweled"),
				appDir.resolvePath("audio/games/GONE NUTS"),
				appDir.resolvePath("particles/BejeweledMatch.pex"),
				appDir.resolvePath("particles/BejeweledMatch.png"),
				appDir.resolvePath("particles/BejeweledStarSparkle.pex"),
				appDir.resolvePath("particles/BejeweledStarSparkle.png"),
				appDir.resolvePath("particles/BejeweledMove.pex"),
				appDir.resolvePath("particles/BejeweledMove.png"),
				appDir.resolvePath("particles/BejeweledSelect.pex"),
				appDir.resolvePath("particles/BejeweledSelect.png"),
				appDir.resolvePath("particles/BejeweledReset.pex"),
				appDir.resolvePath("particles/BejeweledReset.png"),
				appDir.resolvePath("particles/BejeweledStarAdd.pex"),
				appDir.resolvePath("particles/BejeweledStarAdd.png")
			);

			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1)
				{
					trace("Loaded Scene Assets");
					addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
					addObjects();
					assetsLoaded = true;
				};
			});
		}

		private function addObjects():void
		{
			// Instance soundmanager and add objects
			soundManager = SoundManager.getInstance();
			soundManager.addSound("Match1-1", assets.getSound("3match1"));
			soundManager.addSound("Match1-2", assets.getSound("3match2"));
			soundManager.addSound("Match1-3", assets.getSound("3match3"));
			soundManager.addSound("Match1-4", assets.getSound("3match4"));
			soundManager.addSound("Match2", assets.getSound("4match"));
			soundManager.addSound("Match3", assets.getSound("5match"));
			soundManager.addSound("Tutorial", assets.getSound("bejeweledTutorial"));
			soundManager.addSound("GameLoop", assets.getSound("GameLoop"));
			soundManager.addSound("greatJobSnd", assets.getSound("GreatJobSound"));
			soundManager.addSound("greatJobVO", assets.getSound("GreatJobVO"));
			soundManager.addSound("slide", assets.getSound("slide piece"));
			soundManager.addSound("Bonus", assets.getSound("bonus round"));

			// Add large background scaled to wide shot
			bg = new Image(assets.getTexture("BG"));
			sceneRoot.addChild(bg);

			score = 0;
			tmpScore = 0;
			changingScore = 0;
			risingScore = 0;
			multiplier = 1;
			tmpMultiplier = 1;
			gameOver = false;

			bejeweled = new Bejeweled(assets);
			bejeweled.x = bejeweledOffset.x + Costanza.STAGE_OFFSET;
			bejeweled.y = bejeweledOffset.y;
			bejeweled.visible = false;
			bejeweled.touchable = false;

			bejeweled.SignalJewel.add(JewelHandler);
			bejeweled.signalEndGame.add(endGame);
			//add something here?
			addChild(bejeweled);

			var tmpImg:Image;

			// Particles
			starParticles = new PDParticleSystem(assets.getXml("BejeweledStarSparkle"), assets.getTexture("BejeweledStarSparkle"));
			Starling.juggler.add(starParticles);
			addChild(starParticles);

			moveParticles = new PDParticleSystem(assets.getXml("BejeweledMove"), assets.getTexture("BejeweledMove"));
			Starling.juggler.add(moveParticles);
			addChild(moveParticles);

			resetParticles = new PDParticleSystem(assets.getXml("BejeweledReset"), assets.getTexture("BejeweledReset"));
			resetParticles.emitterX = bejeweled.x + 330;
			resetParticles.emitterY = bejeweled.y + 330;
			Starling.juggler.add(resetParticles);
			addChild(resetParticles);

			// Mover bulbs
			bulbMoverBlue = assets.getTexture("Gem_1");

			bulbMoverPurple = assets.getTexture("Gem_2");

			bulbMoverRed = assets.getTexture("Gem_3");

			bulbMoverYellow = assets.getTexture("Gem_4");

			bulbMoverNut6 = assets.getTexture("Gem_5");
			
			moverGemRound = assets.getTexture("Gem_6");

			// TextFields
			messageText = new TextField(640,640, "", "DancingSuperserif");
			messageText.fontSize = 80; // the native bitmap font size, no scaling
			messageText.color = Color.WHITE; // use white to use the texture as it is (no tinting)
			messageText.hAlign = "center";
			messageText.vAlign = "center";
			messageText.touchable = false;
			messageText.alpha = 0;
			messageText.x = bejeweled.x;
			messageText.y = bejeweled.y;
			messageText.nativeFilters = [new GlowFilter(0x000000,1,6,6,12)];
			addChild(messageText);

			//score
			scoreCounter = new ScoreCounter(assets.getTexture("SCORE"));
			scoreCounter.x = Costanza.SCREEN_START_X + Costanza.STAGE_OFFSET;
			scoreCounter.y = 0;

			sceneRoot.addChild(scoreCounter);

			//Jewel
			// DragonBones Setup
			factory = new StarlingFactory();
			
			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("Jewel_skeleton"));
			factory.addSkeletonData(skeletonData);
			
			var texture:Texture = assets.getTexture("Jewel_texture");
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("Jewel_texture"),true);
			factory.addTextureAtlas(textureAtlas);
			
			// Build the armature
			jewelArmature = factory.buildArmature("Characters/Jewel");
			jewelArmature.animation.gotoAndPlay("idle01");
			
			WorldClock.clock.add(jewelArmature);
			
			jewelClip = jewelArmature.display as Sprite;
			
			jewelClip.x = 360 + Costanza.STAGE_OFFSET;
			jewelClip.y = 575;
			jewelClip.addEventListener(TouchEvent.TOUCH, tapJewel);
			
			sceneRoot.addChild(jewelClip);
			
			// Star Meter
			
			starMeterEmpty = new Image(assets.getTexture("Bonus_Under"));
			starMeterEmpty.alignPivot();
			starMeterEmpty.touchable = false;
			starMeterEmpty.alpha = 1;
			starMeterEmpty.scaleX = 0;
			starMeterEmpty.scaleY = 0;
			sceneRoot.addChild(starMeterEmpty);
			
			starMeterText = new Image(assets.getTexture("Bonus_Text"));
			starMeterText.alignPivot();
			starMeterText.touchable = false;
			starMeterText.alpha = 1;
			starMeterText.scaleX = 0;
			starMeterText.scaleY = 0;
			starMeterText.x = bejeweled.x + bejeweled.width/2;
			starMeterText.y = bejeweled.y + bejeweled.height/2;
			addChild(starMeterText);
			
			starMeterFull = new Image(assets.getTexture("Overlay"));
			starMeterFull.touchable = false;
			starMeterFull.alignPivot();
			
			starMeterClip = new Sprite();
			starMeterClip.alignPivot();
			starMeterClip.touchable = false;
			starMeterClip.addChild(starMeterFull);
			starMeterClip.clipRect = starMeterClipRect;
			starMeterClip.visible = false;
			addChild(starMeterClip);
			
			starMeterEmpty.x = 325 + Costanza.STAGE_OFFSET;
			starMeterEmpty.y = 450;
			
			starMeterClip.x = starMeterEmpty.x;
			starMeterClip.y = starMeterEmpty.y;
			
			//hide the gems initially
			var b:Bone;
			for(var i:int = 1; i <= 20; i++)
			{
				b = jewelArmature.getBone("gemNecklaceSmall" + i);
				(b.display as Image).alpha = 0;
			}

			//MAKE SURE TO ADD AFTER JEWEL SO ON TOP
			board = new Image(assets.getTexture("RIO_BEJEWELED_PANEL"));
			board.x = 466 + Costanza.STAGE_OFFSET;
			board.y = 15;
			sceneRoot.addChild(board);
			
			OL_1 = new Image(assets.getTexture("BG_OL_Left"));
			OL_1.y = Costanza.STAGE_HEIGHT - OL_1.height;
			sceneRoot.addChild(OL_1);
			
			OL_2 = new Image(assets.getTexture("RIO_BEJEWELED_OL_RIGHT"));
			OL_2.y = Costanza.STAGE_HEIGHT - OL_2.height;
			OL_2.x = Costanza.STAGE_WIDTH - OL_2.width;
			sceneRoot.addChild(OL_2);

			// Main scene back/exit button
			backButton = new Button(Root.assets.getTexture("Home_Rio"), "", Root.assets.getTexture("Home_Rio"));
			backButton.x = Costanza.SCREEN_START_X + Costanza.STAGE_OFFSET;
			backButton.y = 650;
			backButton.name = "exit";
			backButton.addEventListener(TouchEvent.TOUCH, GoBack);
			sceneRoot.addChild(backButton);

			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);

			// setup no moves signal
			bejeweled.SignalNoMoves.add(noMovesMessage);
			
			//setup score
			bejeweled.signalScore.add(AccumulateScore);

			//comic.showSkip();
			EnableGame();
		}
		
		private function onEnterFrameHandler(evt:EnterFrameEvent):void
		{
			WorldClock.clock.advanceTime(-1);
		}

		private function EnableGame():void
		{
			bejeweled.visible = true;
			TweenMax.delayedCall(4, startGame);
			//this.touchable = false;
			soundManager.playSound("Tutorial", Costanza.soundVolume);
		}

		private function startGame():void
		{
			Rio.Cycle();
			//this.touchable = true;
			bejeweled.touchable = true;
			Bejeweled.gameOver = false;

			soundManager.playSound("GameLoop", Costanza.musicVolume, 999);
		}

		private function JewelHandler(type:String, pos:Point,numMatches:int,currentNum,destroyCount:int):void
		{
			movingBulb = false;
			var nutMover:Image;
			var addedToGem:Boolean = false;

			switch(type)
			{
				case "Gem_1":
				{
					if(!bejeweled.starMode)
					{
						movingBulb = true;
						nutMover = new Image(bulbMoverBlue);
						nutMover.name = "1";
					}
					break;
				}
				case "Gem_2":
				{
					if(!bejeweled.starMode)
					{
						movingBulb = true;
						nutMover = new Image(bulbMoverPurple);
						nutMover.name = "2";
					}
					break;
				}
				case "Gem_3":
				{
					if(!bejeweled.starMode)
					{
						movingBulb = true;
						nutMover = new Image(bulbMoverRed);
						nutMover.name = "3";
					}
					break;
				}
				case "Gem_4":
				{
					if(!bejeweled.starMode)
					{
						movingBulb = true;
						nutMover = new Image(bulbMoverYellow);
						nutMover.name = "4";
					}
					break;
				}
				case "Gem_5":
				{
					if(!bejeweled.starMode)
					{
						movingBulb = true;
						nutMover = new Image(bulbMoverNut6);
						nutMover.name = "5";
					}
					break;
				}
				case "Star":
				{
					trace("Star");
					movingBulb = true;
					nutMover = new Image(moverGemRound);
					nutMover.name = "6";
					break;
				}
				default:
				{
					break;
				}
			}
			
			this.setChildIndex(moveParticles, this.numChildren-1);
			
			if(nutMover)
				sceneRoot.addChild(nutMover);
			
			if(movingBulb && !bejeweled.starMode)
			{
				var bone:Bone = jewelArmature.getBone("gemNecklaceSmall" + numMatches);
				var pnt:Point = new Point(bone.global.x, bone.global.y);
				nutMover.x = pos.x + bejeweledOffset.x;
				nutMover.y = pos.y + bejeweledOffset.y;
				nutMover.visible = true;
				trace("send nuts over");
				
				if(numMatches == bejeweled.finalMove)
				{
					soundManager.playSound("Bonus",Costanza.soundVolume);
					//SETS STAR MODE
					bejeweled.addStarObject();
					bejeweled.ResetGame();
					
					TweenMax.allTo([starMeterEmpty,starMeterText],0.5,{alpha:1, scaleX:1, scaleY:1, ease:Back.easeOut, onComplete:tweenText});
					sceneRoot.setChildIndex(starMeterEmpty,sceneRoot.numChildren-1);
					setChildIndex(starMeterText,numChildren-1);
					setChildIndex(starMeterClip,numChildren-1);
					//this.setChildIndex(addParticles,this.numChildren-1);
					this.setChildIndex(starParticles,this.numChildren-1);
				}
				//sends currentNum==destroyedNum to see if the gem being sent is the last one, which fires the image swap
				TweenMax.to(nutMover,0.5,{bezier:[{x:movingToPoint.x + ((nutMover.x-movingToPoint.x)/2), y:movingToPoint.y - 300}, {x:jewelArmature.display.x + pnt.x - bone.display.width/2, y:jewelArmature.display.y + pnt.y - bone.display.height/2}],scaleX:0.2,scaleY:0.2, onComplete:HideMover, onCompleteParams:[nutMover,numMatches,true]});
			}
			else if(bejeweled.starMode && type == "Star")
			{
				starCount = numMatches;

				nutMover.x = pos.x + bejeweledOffset.x;
				nutMover.y = pos.y + bejeweledOffset.y;
				nutMover.visible = true;
				
				if(!starMeterClip.visible){starMeterClip.visible = true};
				
				//if(starMeterClip.clipRect)
				TweenMax.to(starMeterClip.clipRect,0.3,{y:135 - (starCount*((135*2)/starAmount)), delay:0.4});
				
				
				if(!tweening)
				{
					tweening = true;
					TweenMax.allTo([starMeterClip,starMeterEmpty],.8,{scaleX:1.2, scaleY:1.2, yoyo:true, repeat:1, onComplete:function setTweening():void{tweening = false;}});
				}
				
				TweenMax.to(nutMover,0.5,{x:starMeterClip.x-40,y:starMeterClip.y-40, alpha:0});
				
				if(starCount >= starAmount)
				{
					//GAME OVER
					var _bone:Bone = jewelArmature.getBone("gemNecklaceSmall20");
					var _pnt:Point = new Point(_bone.global.x, _bone.global.y);
					
					TweenMax.delayedCall(0.5,function hideGem():void{starMeterEmpty.visible = starMeterText.visible = false;});
					
					//destroy mask
					jewelArmature.animation.gotoAndPlay("idle01");
					jewelArmature.animation.stop();
					TweenMax.to(starMeterClip,2,{delay:.6,width:_bone.display.width + 10, height:_bone.display.height + 10, x:jewelArmature.display.x + _bone.global.x, y:jewelArmature.display.y + _bone.global.y, onComplete:swapImage,onCompleteParams:[_bone]});
					
					bejeweled.removeListener();
					endGame();
					bejeweled.EndGame();
				}
			}
		}
		
		private function tweenText():void
		{
			TweenMax.to(starMeterText,1,{delay:1,x:starMeterEmpty.x, y:starMeterEmpty.y, scaleX:0.4, scaleY:0.4});
		}
		
		private function swapImage(b:Bone):void
		{
			var img:Image = new Image(assets.getTexture("Bonus_GemSlot"));
			//img.alignPivot();
			//assign image to bone.display for chaging clothes effect. (using bone.display to dispose)
			(b.display as Image).alpha = 1;
			b.display.dispose();
			b.display = img;
			starMeterClip.visible = false;
			jewelArmature.animation.gotoAndPlay("gameWin");
		}
		
		private function tapJewel(e:TouchEvent):void
		{
			if(e.getTouch(this) && e.getTouch(this).phase == TouchPhase.BEGAN && jewelArmature.animation.movementID == "idle01")
			{
				var num:int = Math.random()*2 + 2;
				jewelArmature.animation.gotoAndPlay("idle0" + num);
			}
		}

		private function HideMover(nutMover:Image,num:int,swap:Boolean=false):void
		{
			if(swap)
			{
				//swap gem, show gem alpha,
				var _image:Image = factory.getTextureDisplay("!!GemPieces/gemNecklaceSmall0" + nutMover.name) as Image;
				//assign image to bone.display for chaging clothes effect. (using bone.display to dispose)
				var _bone:Bone = jewelArmature.getBone("gemNecklaceSmall" + num);
				(_bone.display as Image).alpha = 1;
				_bone.display.dispose();
				_bone.display = _image;
			}
			
			if(jewelArmature.animation.movementID == "idle01")
			{
				jewelArmature.animation.gotoAndPlay("addJewel0"+jewelAnimNum);
				jewelAnimNum++;
				if(jewelAnimNum > 3){jewelAnimNum=1;}
			}
			
			//jewelArmature.animation
			sceneRoot.removeChild(nutMover);
		}

		private function endGame():void
		{
			if(!gameOver)
			{
				gameOver = true;
				if(soundManager.soundIsPlaying("GameLoop"))
				{
					soundManager.stopSound("GameLoop");
				}
				
				bejeweled.DestroyNutsGameOver(0);
	
				//TweenMax.delayedCall(1, bejeweled.DestroyNutsGameOver, [0]);
				TweenMax.delayedCall(1, bejeweled.DestroyNutsGameOver, [1]);
				TweenMax.delayedCall(2, bejeweled.DestroyNutsGameOver, [2]);
				TweenMax.delayedCall(3, bejeweled.DestroyNutsGameOver, [3]);
				TweenMax.delayedCall(4, bejeweled.DestroyNutsGameOver, [4]);
				TweenMax.delayedCall(5, dropGameOver);
			}
		}
		
		private function dropGameOver():void
		{
			greatJob = new Image(assets.getTexture("Great"));
			greatJob.x = 400 + Costanza.STAGE_OFFSET;
			greatJob.y = -400;
			greatJob.alpha = 1;
			greatJob.addEventListener(TouchEvent.TOUCH, ChecHighScores);
			addChild(greatJob);
			TweenMax.to(greatJob, 0.5,{y:100, ease:Cubic.easeIn});
			
			soundManager.playSound("greatJobSnd", Costanza.soundVolume);
			soundManager.playSound("greatJobVO", Costanza.soundVolume);
		}

		private function noMovesMessage():void
		{
			trace("Rescrambling");
			setChildIndex(messageText, this.numChildren - 1);
			messageText.text = "Reshuffle"
			TweenMax.to(messageText,0.5,{alpha:1, yoyo:true, repeat:3});
		}

		private function GoBack(e:TouchEvent):void
		{
			//soundManager.playSound("Back",Costanza.soundVolume);
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				GameOver();
			}
		}

		private function ChecHighScores(e:TouchEvent):void
		{
			//soundManager.playSound("Back",Costanza.soundVolume);
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				removeChild(greatJob);
				if(soundManager.soundIsPlaying("GameLoop"))
					soundManager.stopSound("GameLoop");
				soundManager.stopAllSounds();
				var highScore:HighScore = new HighScore("JewelsGems", score);
				sceneRoot.addChild(highScore);
			}
		}

		private function GameOver():void
		{
			if(soundManager.soundIsPlaying("GameLoop"))
				soundManager.stopSound("GameLoop");
			soundManager.stopAllSounds();

			bejeweled.starMode = false;
			starCount = 0;

			TweenMax.killAll();
			Starling.juggler.purge();
			// signaling back up to load lobby
			soundManager.stopAllSounds();
			dispatchEventWith(Root.LOAD_LOBBY, true);
		}

		private function ShowGameOverScreen():void
		{
			//TweenMax.killAll();
			greatJob.alpha = 0;
			removeChild(greatJob);
			bejeweled.visible = false;

			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);

			//Move the score Counter to it's new position
		}

		private function TouchReset():void
		{
			addObjects();
		}

		private function AccumulateScore():void
		{
			score++;
			scoreCounter.score = score;
		}

		private function Reset(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				TouchReset();
			}
		}

		private function disposeScene():void
		{
			removeChild(greatJob,true);
			moveParticles.dispose();
			starParticles.dispose();
			resetParticles.dispose();
			// dispose everything to remove from GPU
			bejeweled.destroy();
			bejeweled.dispose();
			jewelArmature.dispose();
			WorldClock.clock.remove(jewelArmature);
			assets.dispose();
			//soundManager.removeSound("AllSprucedUp");
			sceneRoot.dispose();
		}
	}
}