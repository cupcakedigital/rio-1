package games.bejeweled
{
	import com.cupcake.Utils;
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import org.osflash.signals.Signal;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.Color;

	public class Bejeweled extends Sprite
	{
		private var gems_array:Array=new Array();
		private var aGem:Jewel;

		private var selectorBox:Image;

		private var selectorRow:int=-6;
		private var selectorColumn:int=-6;

		private var parentAssets:AssetManager;

		private var moveState:Boolean=false;

		public var SignalJewel:Signal = new Signal(String,Point);
		public var signalEndGame:Signal = new Signal();
		public var SignalNoMoves:Signal = new Signal();
		public var signalFloatText:Signal = new Signal(int);
		public var signalScore:Signal = new Signal();

		private var jewel1:Texture;
		private var jewel2:Texture;
		private var jewel3:Texture;
		private var jewel4:Texture;
		private var jewel5:Texture;
		private var jewel6:Texture;
		
		private var nutPlosionSmall:Vector.<Texture>;
		private var nutPlosionMedium:Vector.<Texture>;
		private var nutPlosionLarge:Vector.<Texture>;

		// Particle effects
		private var hintParticlesA:PDParticleSystem;
		private var hintParticlesB:PDParticleSystem;

		private var nutTextureArray:Array;
		private var colours_array_star:Array;

		private var clickPossible:Boolean=false;

		private var inaRow:uint=0;
		private var match:Boolean = true;
		private var swapState:Boolean = false;

		private var gemContainer:Sprite = new Sprite();

		private var delayHint:TweenMax;
		public static var gameOver:Boolean = false;
		public  var gameLocked:Boolean = false;

		private var gemCount:Number = 5;
		private var selectorOffsetX:Number = 9;
		private var selectorOffsetY:Number = 7;

		private var destroyCount:int = 0;

		private var endOfGameTimer:int = 0;

		private var soundManager:SoundManager;
		
		private static const ITEMSIZE:Number = 109;
		
		public var jewelCount:int = 0;
		
		public var finalMove:int = 19;//when to start bonus round
		
		public var starMode:Boolean = false;
		
		private var starGemMatched:Boolean = false;
		
		private var soundNum:int = 1;
		
		private var jewelSent:Boolean = false;

		public function Bejeweled(assets:AssetManager) 
		{
			// Game initiation
			parentAssets = assets;

			// Instance soundmanager and add objects
			soundManager = SoundManager.getInstance();

			selectorBox = new Image(assets.getTexture("selector"));
			selectorBox.touchable = false;
			selectorBox.visible = false;
			addChild(selectorBox);

			jewel1 = parentAssets.getTexture("Gem_1");
			jewel2 = parentAssets.getTexture("Gem_2");
			jewel3 = parentAssets.getTexture("Gem_3");
			jewel4 = parentAssets.getTexture("Gem_4");
			jewel5 = parentAssets.getTexture("Gem_5");
			jewel6 = parentAssets.getTexture("Gem_6");

			nutPlosionSmall = parentAssets.getTextures("gemSmashSmall_00");
			nutPlosionMedium = parentAssets.getTextures("gemSmashMed_00");
			nutPlosionLarge = parentAssets.getTextures("gemSmashLarge_00");

			nutTextureArray = new Array(jewel1,jewel2,jewel3,jewel4,jewel5);
			colours_array_star = new Array(jewel1,jewel2,jewel6,jewel4,jewel5);

			// Place container.
			gemContainer.x = 45;
			gemContainer.y = 45;
			addChild(gemContainer);

			// Create Gems in rows and columns
			for (var i:uint=0; i < 6; i++)
			{
				gems_array[i] = new Array();
				for (var j:uint=0; j < 6; j++)
				{
					do
					{
						gems_array[i][j]=Math.floor(Math.random()*gemCount);
					}
					while (RowLineLength(i, j) > 2 || ColumnLineLength(i, j) > 2);
					aGem = new Jewel(nutTextureArray[gems_array[i][j]], 12, gems_array[i][j],starMode);
					aGem.name = i + "_" + j;
					aGem.x = j * ITEMSIZE;
					aGem.y = i * ITEMSIZE;
					gemContainer.addChild(aGem);
				}
			}
			// Create and style selector box
			addChild(selectorBox);

			// Add particles for hints
			hintParticlesA = new PDParticleSystem(assets.getXml("BejeweledSelect"), assets.getTexture("BejeweledSelect")),
			Starling.juggler.add(hintParticlesA);
			addChild(hintParticlesA);

			hintParticlesB = new PDParticleSystem(assets.getXml("BejeweledSelect"), assets.getTexture("BejeweledSelect")),
			Starling.juggler.add(hintParticlesB);
			addChild(hintParticlesB);

			// Listen for user input
			this.addEventListener(starling.events.TouchEvent.TOUCH,touchHandler);
			this.addEventListener(starling.events.EnterFrameEvent.ENTER_FRAME,EveryFrame);
		}
		
		public function removeListener():void
		{
			this.removeEventListener(starling.events.EnterFrameEvent.ENTER_FRAME,EveryFrame);
		}

		// Every frame...
		private function EveryFrame(e:starling.events.EnterFrameEvent):void
		{
			if(!swapState)
			{
				//Assume that gems are not falling
				var gemsAreFalling:Boolean=false;
				// Check each gem for space below it
				for (var i:int=4; i>=0; i--)
				{
					for (var j:uint=0; j<6; j++)
					{
						// If a spot contains a gem, and has an empty space below...
						if (gems_array[i][j] != -1 && gems_array[i+1][j]==-1)
						{
							gemsAreFalling=true;
							gems_array[i+1][j]=gems_array[i][j];
							gems_array[i][j]=-1;
							TweenMax.to(gemContainer.getChildByName(i+"_"+j),0.2,{y:(i*ITEMSIZE) + ITEMSIZE});
							//getChildByName(i+"_"+j).y+=itemSize;
							gemContainer.getChildByName(i+"_"+j).name=(i+1)+"_"+j;
							break;
						}
					}
					// If a gem is falling
					if (gemsAreFalling)
					{
						// don't allow any more to start falling
						break;
					}
				}
				// If no gems are falling
				if (!gemsAreFalling)
				{
					// Assume no new gems are needed
					var needNewGem:Boolean=false;
					// but check all spaces...
					for (i=5; i>=0; i--)
					{
						for (j=0; j<6; j++)
						{
							// and if a spot is empty
							if (gems_array[0][j]==-1)
							{
								// now we know we need a new gem
								needNewGem=true;
								// pick a random color for the gem
								gems_array[0][j]=Math.floor(Math.random()*gemCount);

								// create the gem
								aGem = new Jewel(nutTextureArray[gems_array[0][j]],12,gems_array[0][j],starMode);
								aGem.name= "0_" + j;
								// position it
								aGem.x = j*ITEMSIZE;
								aGem.y = -50;

								TweenMax.to(aGem,0.2,{y:0});
								// show it
								gemContainer.addChild(aGem);
								// stop creating new gems
								break;
							}
						}
						// if a new gem was created, stop checking
						if (needNewGem)
						{
							break;
						}
					}
					// If no new gems were needed...
					if (!needNewGem && !gameLocked)
					{
						// assume no more/new lines are on the board
						var moreLinesAvailable:Boolean=false;
						// creat a new array, set the gem type of the line, and where it is
						var lineGems:Array = new Array();
						destroyCount = 0;
						// check all gems
						for (i=5; i>=0; i--)
						{
							for (j=0; j<6; j++)
							{
								// if a line is found
								if (RowLineLength(i,j)>2 || ColumnLineLength(i,j)>2)
								{
									// then we know more lines are available
									moreLinesAvailable = true;

									var gemType:uint=gems_array[i][j];
									var linePosition:int;
									trace("GEM     TYPE" + gemType);
									if(gemType == 2){starGemMatched = true;}
									// check t's a horizontal line...
									if (RowLineLength(i,j)>2)
									{
										// if so, find out how long it is and put all the line's gems into the array
										linePosition = j;
										while (SameGemIsHere(gemType,i,linePosition-1))
										{
											linePosition--;
											if(!IsDuplicateNut(lineGems, i+"_"+linePosition))
											{
												lineGems.push(i+"_"+linePosition);
												destroyCount++;
											}
										}
										linePosition = j;
										while (SameGemIsHere(gemType,i,linePosition+1))
										{
											linePosition++;
											if(!IsDuplicateNut(lineGems, i+"_"+linePosition))
											{
												lineGems.push(i+"_"+linePosition);
												destroyCount++;
											}
										}
									}
									// check t's a vertical line...
									if (ColumnLineLength(i,j) > 2)
									{
										// if so, find out how long it is and put all the line's gems into the array
										linePosition=i;
										while (SameGemIsHere(gemType,linePosition-1,j))
										{
											linePosition--;
											if(!IsDuplicateNut(lineGems, linePosition+"_"+j))
											{
												lineGems.push(linePosition+"_"+j);
												destroyCount++;
											}
										}
										linePosition=i;
										while (SameGemIsHere(gemType,linePosition+1,j))
										{
											linePosition++;
											if(!IsDuplicateNut(lineGems, linePosition+"_"+j))
											{
												lineGems.push(linePosition+"_"+j);
												destroyCount++;
											}
										}
									}
									
									// pause to wait for line 
									TweenMax.delayedCall((0.6),destroyLine,[lineGems]);
									//soundManager.playSound("Match1");
									// change the state so we dont check until its done.
									swapState = true;
									resetHintTimer();
								}
							}
						}
						// if no more lines were available...
						if (!moreLinesAvailable)
						{
							// allow new moves to be made
							clickPossible = true;
							//Accumulate score into total once the cascade is done
							inaRow=0;
						}
						if(lineGems.length > 0)
						{
							endOfGameTimer = 0;
							//we have matches, lets blow them up, but only after a slight delay
							if(!starMode || (starMode && starGemMatched))
							{
								if(!jewelSent)
								{
									jewelCount++;
								}
								starGemMatched = false;
							}
							TweenMax.delayedCall(0.2,DestroyNuts,[lineGems]);
						}
						else
						{
							jewelSent = false;
							soundNum = 1;
						}
					}
				}
			}
		}

		public function addStarObject():void
		{
			if(!starMode)
			{
				starMode = true;
				jewelCount = 0;
				nutTextureArray = colours_array_star;
			}
		}

		private function DestroyNuts(lineGems:Array):void
		{
			// explode lines
			var tmpVector:Vector.<Texture>;
			if(destroyCount < 4)
			{
				tmpVector = nutPlosionSmall;
				soundManager.playSound("Match1-" + soundNum,Costanza.soundVolume);
				if(soundNum < 4){soundNum++;}
			}
			else if(destroyCount > 3 && destroyCount < 7)
			{
				tmpVector = nutPlosionMedium;
				soundManager.playSound("Match2",Costanza.soundVolume);
			}
			else
			{
				tmpVector = nutPlosionLarge;
				soundManager.playSound("Match3",Costanza.soundVolume);
			}

			for (var i:int = 0; i < lineGems.length; i++)
			{
				var jewel:Jewel = gemContainer.getChildByName(lineGems[i]) as Jewel;
				if(jewel)
				{
					jewel.visible = false;

					var crackedNut:MovieClip = new MovieClip(tmpVector, 24);

					switch(jewel.itemType)
					{
						case "Star":
						{
							//White
							crackedNut.color = Color.WHITE;
							break;
						}
						case "Gem_1":
						{
							//purple
							crackedNut.color = Color.PURPLE;
							break;
						}
						case "Gem_2":
						{
							//blue
							crackedNut.color = Color.BLUE;
							break;
						}
						case "Gem_3":
						{
							//green
							crackedNut.color = Color.GREEN;
							break;
						}
						case "Gem_4":
						{
							//yellow
							crackedNut.color = Color.YELLOW;
							break;
						}
						case "Gem_5":
						{
							//red
							crackedNut.color = Color.RED;
							break;
						}
					}

					crackedNut.x = jewel.x - 50;
					crackedNut.y = jewel.y - 50;

					crackedNut.loop = false;
					crackedNut.addEventListener(Event.COMPLETE, RemoveNutPlosion);
					Starling.juggler.add(crackedNut);
					addChild(crackedNut);
					
					if(!jewelSent && !starMode)
					{
						SignalJewel.dispatch(jewel.itemType,new Point(jewel.x, jewel.y),jewelCount,i+1,destroyCount);
						jewelSent = true;
					}
					else if (starMode)
					{
						SignalJewel.dispatch(jewel.itemType,new Point(jewel.x, jewel.y),jewelCount,i+1,destroyCount);
					}

					signalScore.dispatch();
				}
			}
			//TweenMax.delayedCall(0.5,signalFloatText.dispatch,[destroyCount,crackedNut.x,crackedNut.y]);
		}

		public function DestroyNutsGameOver(destroyType:int):void
		{
			// explode lines
			var tmpVector:Vector.<Texture>;
			tmpVector = nutPlosionLarge;
			soundManager.playSound("Match3",Costanza.soundVolume);
			//SceneBejeweled.j

			for (var row:int=5; row>=0; row--)
			{
				for (var column:int=0; column<6; column++)
				{
					if(gems_array[row][column] == destroyType)
					{
						var jewel:Jewel = gemContainer.getChildByName(row+"_"+column) as Jewel;
						
						jewel.visible = false;
		
						var crackedNut:MovieClip = new MovieClip(tmpVector, 12);

						crackedNut.x = jewel.x - 70;
						crackedNut.y = jewel.y - 70;

						crackedNut.loop = false;
						crackedNut.addEventListener(Event.COMPLETE, RemoveNutPlosion);
						Starling.juggler.add(crackedNut);
						addChild(crackedNut);
					}
				}
			}
		}
		
		private function IsDuplicateNut(lineGems:Array, gem:String):Boolean
		{
			for(var i:int = 0; i < lineGems.length; i++)
			{
				if(gem == lineGems[i])
				{
					return true;
				}
			}
			return false;
		}

		private function destroyLine(lineGems:Array):void
		{
			// for all gems in the line...
			
			for (var i:int = 0; i < lineGems.length; i++)
			{
				var mc:Jewel = gemContainer.getChildByName(lineGems[i]) as Jewel;
				// remove it from the program
				gemContainer.removeChild(mc);
				// find where it was in the array
				var cd:Array = lineGems[i].split("_");
				// set it to an empty gem space
				gems_array[cd[0]][cd[1]] = -1;
				// set the new score
				//score += inaRow;
				// set the score setter up
				inaRow++;
				//Dispatch Id of Jewel for our blenders
			}

			swapState = false;
			startHintTimer();
		}

		private function RemoveNutPlosion(e:Event):void
		{
			var crackedNut:MovieClip = e.currentTarget as MovieClip;
			crackedNut.stop();
			Starling.juggler.remove(crackedNut);
			removeChild(crackedNut);
		}

		private function touchHandler(e : TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if (touch == null || gameOver) return;

			var position:Point = touch.getLocation(this);

			switch(touch.phase)
			{
				case TouchPhase.BEGAN :
				{
					jewelEvent(position);
					break;
				}
				case TouchPhase.MOVED :
				{
					
					if(moveState && !swapState){
						jewelEvent(position);
					};
					break;
				}
				case TouchPhase.ENDED :
				{
					moveState = false;
					break;
				}
				default:
				{
					break;
				}
			}
		}

		// When the user clicks
		private function jewelEvent(pos:Point):void
		{
			// If a click is allowed
			resetHintTimer();
			if(pos.x < 0 || pos.y < 0)
			{ return;}
			if (clickPossible && !swapState)
			{
				moveState = true;
				// Find which row and column were clicked

				var clickedRow:uint = Math.floor(pos.y/ITEMSIZE);
				var clickedColumn:uint = Math.floor(pos.x/ITEMSIZE);

				clickedRow = Utils.Clamp( clickedRow, 0, 5 );
				clickedColumn = Utils.Clamp( clickedColumn, 0, 5 );

				this.setChildIndex(selectorBox,this.numChildren-1);

				// Check if the clicked gem is adjacent to the selector
				// If not...
				if (!(((clickedRow==selectorRow+1 || clickedRow==selectorRow-1)&&clickedColumn==selectorColumn)||((clickedColumn==selectorColumn+1 || clickedColumn==selectorColumn-1) && clickedRow==selectorRow)))
				{
					// Find row and colum the selector should move to
					selectorRow=clickedRow;
					selectorColumn=clickedColumn;
					// Move it to the chosen position
					selectorBox.x=(ITEMSIZE*selectorColumn)-selectorOffsetX;
					selectorBox.y=(ITEMSIZE*selectorRow)-selectorOffsetY;
					// If hidden, show it.
					selectorBox.visible = true;
				}
					// If it is not next to it...
				else 
				{
					// enter swap state
					swapState = true;
					TweenMax.delayedCall(0.4, swapStateOff);

					function swapStateOff():void
					{
						swapState = false;
						selectorRow=-1;
						selectorColumn=-1;
						selectorBox.x=-ITEMSIZE-selectorOffsetX;
						selectorBox.x=-ITEMSIZE-selectorOffsetX;
					}
					
					soundManager.playSound("slide",Costanza.soundVolume);

					// If hidden, show it.
					selectorBox.visible = true;
					selectorBox.x=(ITEMSIZE*selectorColumn)-selectorOffsetX;;
					selectorBox.y=(ITEMSIZE*selectorRow)-selectorOffsetY;
					TweenMax.to(selectorBox,0.3,{x:(ITEMSIZE*clickedColumn)-selectorOffsetX,y:(ITEMSIZE*clickedRow)-selectorOffsetY, onComplete:function hideSelect():void{selectorBox.visible = false}});

					// Swap the gems;
					SwapGems(selectorRow,selectorColumn,clickedRow,clickedColumn);
					// identify the gems
					var gem1:Jewel = gemContainer.getChildByName(selectorRow+"_"+selectorColumn) as Jewel;
					var gem2:Jewel = gemContainer.getChildByName(clickedRow+"_"+clickedColumn) as Jewel;

					// If they make a line...
					if (RowLineLength(selectorRow,selectorColumn)>2 || ColumnLineLength(selectorRow,selectorColumn)>2||RowLineLength(clickedRow,clickedColumn)>2 || ColumnLineLength(clickedRow,clickedColumn)>2)
					{
						// remove the hint 
						resetHintTimer();
						// dis-allow a new move until cascade has ended (removes glitches)
						clickPossible=false;
						moveState = false;
						//soundManager.playSound("Match");
						// move and rename the gems

						TweenMax.to(gem1,0.3,{x:clickedColumn*ITEMSIZE,y:clickedRow*ITEMSIZE});
						TweenMax.to(gem2,0.3,{x:selectorColumn*ITEMSIZE,y:selectorRow*ITEMSIZE});

						gem1.name="t";
						gem2.name=selectorRow+"_"+selectorColumn;

						gemContainer.getChildByName("t").name=clickedRow+"_"+clickedColumn;
						match = true;
					}
					else
					{
						// Switch them back
						soundManager.playSound("slide",Costanza.soundVolume);
						TweenMax.to(gem1,0.3,{x:clickedColumn*ITEMSIZE,y:clickedRow*ITEMSIZE});
						TweenMax.to(gem2,0.3,{x:selectorColumn*ITEMSIZE,y:selectorRow*ITEMSIZE});
						TweenMax.to(gem2,0.1,{x:clickedColumn*ITEMSIZE,y:clickedRow*ITEMSIZE, delay:0.3});
						TweenMax.to(gem1,0.1,{x:selectorColumn*ITEMSIZE,y:selectorRow*ITEMSIZE, delay:0.3});
						
						SwapGems(selectorRow,selectorColumn,clickedRow,clickedColumn);
						match = false;
						
						startHintTimer();
					}
				}
			}
		}

		private function resetHintTimer():void
		{
			hintParticlesA.stop(true);
			hintParticlesB.stop(true);
			
			TweenMax.killDelayedCallsTo(HintJems);
		}
		
		public function startHintTimer():void
		{
			if(!hintParticlesA.isEmitting && !gameOver)
			{
				TweenMax.delayedCall(6,HintJems);
			}
		}
		
		private function HintJems():void
		{
			var movesLeft:Boolean = false;
			
			// For gems in all rows...
			for (var i:uint=0; i<=5; i++)
			{
				// and columns...
				for (var j:uint=0; j<=5; j++)
				{
					// if they're not too close to the side... 
					if (i<=4)
					{
						// swap them horizontally
						SwapGems(i,j,i+1,j);
						// check if they form a line
						if ((RowLineLength(i,j)>2||ColumnLineLength(i,j)>2||RowLineLength(i+1,j)>2||ColumnLineLength(i+1,j)>2))
						{
							this.setChildIndex(hintParticlesA,0);
							this.setChildIndex(hintParticlesB,0);
							
							hintParticlesA.emitterX = j*ITEMSIZE + 40;
							hintParticlesA.emitterY = i*ITEMSIZE + 48;
							hintParticlesA.start();
							
							hintParticlesB.emitterX = (j)*ITEMSIZE + 40;
							hintParticlesB.emitterY = (i+1)*ITEMSIZE + 48;
							hintParticlesB.start();
							trace("vertical hint");
							
							movesLeft = true;
							
							//hint_txt.text = (i+1).toString()+","+(j+1).toString()+"->"+(i+2).toString()+","+(j+1).toString();
						}
						// swap the gems back
						SwapGems(i,j,i+1,j);
					}
					// then if they're not to close to the bottom...
					if (j<=4)
					{
						// swap it vertically
						SwapGems(i,j,i,j+1);
						// check if it forms a line
						if ((RowLineLength(i,j)>2||ColumnLineLength(i,j)>2||RowLineLength(i,j+1)>2||ColumnLineLength(i,j+1)>2) ) 
						{
							// if so, name it
							this.setChildIndex(hintParticlesA,0);
							this.setChildIndex(hintParticlesB,0);

							hintParticlesA.emitterX = j*ITEMSIZE + 40;
							hintParticlesA.emitterY = i*ITEMSIZE + 48;
							hintParticlesA.start();

							hintParticlesB.emitterX = (j+1)*ITEMSIZE + 40;
							hintParticlesB.emitterY = (i)*ITEMSIZE + 48;
							hintParticlesB.start();

							trace("horizontal hint");

							movesLeft = true;
						}
						// swap the gems back
						SwapGems(i,j,i,j+1);
					}
				}
			}
			
			if(!movesLeft)
			{
				trace("no more moves! resetting");
				ResetGame();
				SignalNoMoves.dispatch();
			}
		}

		//Swap given gems
		private function SwapGems(fromRow:uint,fromColumn:uint,toRow:uint,toColumn:uint):void
		{
			//Save the original position
			var originalPosition:uint=gems_array[fromRow][fromColumn];
			//Move original gem to new position
			gems_array[fromRow][fromColumn]=gems_array[toRow][toColumn];
			//move second gem to saved, original gem's position
			gems_array[toRow][toColumn]=originalPosition;
		}
		//Find out if there us a horizontal line
		private function RowLineLength(row:uint,column:uint):uint
		{
			var gemType:uint=gems_array[row][column];
			var lineLength:uint=1;
			var checkColumn:int=column;
			//check how far left it extends
			while (SameGemIsHere(gemType,row,checkColumn-1))
			{
				checkColumn--;
				lineLength++;
			}
			checkColumn=column;
			//check how far right it extends
			while (SameGemIsHere(gemType,row,checkColumn+1))
			{
				checkColumn++;
				lineLength++;
			}
			// return total line length
			return (lineLength);
		}
		//Find out if there is a vertical line
		private function ColumnLineLength(row:uint,column:uint):uint
		{
			var gemType:uint=gems_array[row][column];
			var lineLength:uint=1;
			var checkRow:int=row;
			//check how low it extends
			while (SameGemIsHere(gemType,checkRow-1,column))
			{
				checkRow--;
				lineLength++;
			}
			//check how high it extends
			checkRow=row;
			while (SameGemIsHere(gemType,checkRow+1,column))
			{
				checkRow++;
				lineLength++;
			}
			// return total line length
			return (lineLength);
		}
		private function SameGemIsHere(gemType:uint,row:int,column:int):Boolean
		{
			//Check there are gems in the chosen row
			if (gems_array[row]==null)
			{
				return false;
			}
			//If there are, check if there is a gem in the chosen slot
			if (gems_array[row][column]==null)
			{
				return false;
			}
			//If there is, check if it's the same as the chosen gem type
			return gemType==gems_array[row][column];
		}

		public function EndGame():void
		{
			gameOver = true;
			//TweenMax.delayedCall(1,signalEndGame.dispatch);
			resetHintTimer();
		}
		
		public function ResetGame():void
		{
			gameLocked = true;
			resetHintTimer();
			gemContainer.removeChildren(0,gemContainer.numChildren-1);

			for (var i:uint=0; i<6; i++)
			{
				for (var j:uint=0; j<6; j++)
				{
					gems_array[i][j]=-1;
				}
			}

			TweenMax.delayedCall(1,function unlockGame():void{gameLocked = false;});
		}

		public function destroy():void
		{
			trace("DESTROY");
			parentAssets.dispose();
			Starling.juggler.remove(hintParticlesA);
			Starling.juggler.remove(hintParticlesB);
			this.removeEventListener(starling.events.TouchEvent.TOUCH,touchHandler);
			this.removeEventListener(starling.events.EnterFrameEvent.ENTER_FRAME,EveryFrame);
			TweenMax.killDelayedCallsTo(HintJems);
			this.removeChildren(0,-1,true);
		}
	}
}
