package games.bejeweled
{
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class Jewel extends Image
	{

		public var itemType:String = "";

		public function Jewel(texture:Texture, fps:Number=12, typeInt:int=0, star:Boolean = false)
		{
			super(texture);
			pivotX = width/2;
			pivotY = height/2;

			switch(typeInt)
			{
				case 0:
				{
					itemType = "Gem_1";
					break;
				}
				case 1:
				{
					itemType = "Gem_2";
					break;
				}
				case 2:
				{
					if(star){
						itemType = "Star";
					} else {
						itemType = "Gem_3";
					}
					break;
				}
				case 3:
				{
					itemType = "Gem_4";
					break;
				}
				case 4:
				{
					itemType = "Gem_5";
					break;
				}	
			}
		}
	}
}