package games.SideScroller
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.AssetManager;
	
	public class Background extends Sprite
	{
		private var background:Image;
		private var bgLayer1:BgLayer;
		private var bgLayer2:BgLayer;
		private var bgLayer3:BgLayer;
		private var bgLayer4:BgLayer;

		private var _scrollSpeed:Number = 0;
		
		private var assets:AssetManager;

		public function Background(newAssets:AssetManager)
		{
			super();
			assets = newAssets;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

			background = new Image(assets.getTexture("Background"));
			addChild(background);

			bgLayer1 = new BgLayer(new Image(assets.getTexture("clouds")),0);
			bgLayer1.parallax = 0.05;
			addChild(bgLayer1);

			bgLayer2 = new BgLayer(new Image(assets.getTexture("BackBushes")),350);
			bgLayer2.parallax = 0.2;
			addChild(bgLayer2);

			bgLayer3 = new BgLayer(new Image(assets.getTexture("BackTrees")),385);
			bgLayer3.parallax = 0.3;
			addChild(bgLayer3);

			bgLayer4 = new BgLayer(new Image(assets.getTexture("MidTrees")), 410);
			bgLayer4.parallax = 0.5;
			addChild(bgLayer4);
		}

		public function Update(timePassed:Number):void
		{
			bgLayer1.Move(scrollSpeed * timePassed);
			bgLayer2.Move(scrollSpeed * timePassed);
			bgLayer3.Move(scrollSpeed * timePassed);
			bgLayer4.Move(scrollSpeed * timePassed);
		}

		public function get scrollSpeed():Number
		{
			return _scrollSpeed;
		}

		public function set scrollSpeed(value:Number):void
		{
			_scrollSpeed = value;
		}
	}
}