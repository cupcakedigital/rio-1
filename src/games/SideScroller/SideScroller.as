package games.SideScroller
{
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.utils.getTimer;
	
	import dragonBones.animation.WorldClock;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	import starling.utils.Color;
	
	import ui.HighScore;
	import ui.ScoreCounter;
	
	import utils.FastCollisions;
	import utils.MathHelpers;
	import utils.ParticleSystem.ParticleEmitter;

	public class SideScroller extends Sprite
	{
		// Assets
		private var assets:AssetManager;
		private var soundManager:SoundManager;
		private var appDir:File = File.applicationDirectory;

		// Scene Objects
		private var sceneRoot:Sprite;

		private var background:Background;
		private var foreground:Foreground;

		private var treeSprites:Sprite;
		private var treeLayer:Array = new Array();

		private var backBtn:Button;

		private var greatJob:Image;
		private var gameOverImage:Image;
		//time counters for controlliong updating
		private var lastTime:Number;
		private var currentTime:Number;

		private var paused:Boolean = false;
		private var endingStarted:Boolean = false;
		private var stopUpdating:Boolean = false;

		private var nigelWarning:MovieClip;
		private var warningCount:int = 0;

		//fruit
		private var fruitArray:Array = new Array();
		private var fruitLayer:Sprite = new Sprite();

		//our Hero
		private var blu:Blu;

		//the bad guy
		private var nigel:Nigel;

		//the princess
		private var jewel:Jewel;

		//the scoreCounter
		private var scoreCounter:ScoreCounter;

		//the energy meter
		private var energyMeter:EnergyMeter;

		//and the energy
		private var energy:Number = 100;

		//the counter
		private var counterBar:Counter;

		//the juice particles
		private var juiceParticles:ParticleEmitter;

		//gameOver
		private var gameOver:Boolean = false;

		//testQuad
		private var testQuad:Quad;

		//cages freed
		private var birdsFreed:int = 0;

		//tree creation rate
		private var treeRate:Number = 3;

		//scrolling speed
		private var scrollSpeed:Number = 150.0;

		//highscore
		private var highScore:HighScore;

		//Difficulty
		private var difficultySelection:Image;
		private var tutorialImage:Image;
		private var easyButton:Button;
		private var hardButton:Button;
		private var isEasy:Boolean = true;

		private var modeSelected:Boolean = false;

		private var tutorialMode:Boolean = true;

		private var assetsLoaded:Boolean = false;

		private static const MIN_SCROLLSPEED:Number = 150.0;
		//layer markers, so we know where to add items at
		private static const BGTREE_LAYER:int = 0;
		private static const MIDTREE_LAYER:int = 1;
		private static const TREE_LAYER:int = 2;

		private static const BACKTREES_HEIGHT:int = 400;
		private static const MIDTREES_HEIGHT:int = 425;
		private static const FGTREES_HEIGHT:int = 650;

		private static const BLU_START_X:int = 400 + Costanza.STAGE_OFFSET;

		private static const BIRD_RESCUE_COUNT:int = 10;
		private static const MAX_ENERGY:int = 100;

		public var GAME_TYPE:int = 0;

		public function SideScroller(num:int)
		{
			addEventListener(HighScore.GO_HOME,  GameOver);
			addEventListener(HighScore.RESTART_GAME,  TouchReset);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE, onAppActivate);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE, onAppDeactivate);

			soundManager = SoundManager.getInstance();
			trace("SideScroller game");
			// Create main scene root
			sceneRoot = new Sprite();
			addChild(sceneRoot);

			//Scene Inited, listen for dipose now
			addEventListener(Root.DISPOSE,  KillAll);

			// Create new asset manager and load content
			assets = new AssetManager(Costanza.SCALE_FACTOR, Costanza.mipmapsEnabled);
			Root.currentAssetsProxy = assets;

			assets.verbose = Capabilities.isDebugger;
			assets.enqueue(
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/SideScroller"),
				appDir.resolvePath("audio/games/SideScroller")
			);

			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1)
				{
					trace("Loaded Scene Assets");
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
					AddObjects();
					assetsLoaded = true;
				}
			});
		}

		private function AddObjects():void
		{
			currentTime = 0.0;
			lastTime = 0.0;
			soundManager.addSound("EasyVO",assets.getSound("EasyVO"));
			soundManager.addSound("HardVO",assets.getSound("HardVO"));
			soundManager.addSound("mainLoop",assets.getSound("mainLoop"));
			soundManager.addSound("gameOver",assets.getSound("gameOver"));
			soundManager.addSound("cheer",assets.getSound("cheer"));
			soundManager.addSound("freebird1",assets.getSound("freebird1"));
			soundManager.addSound("freebird2",assets.getSound("freebird2"));
			soundManager.addSound("fruitSplash",assets.getSound("fruitSplash"));
			soundManager.addSound("monkeyAttack1",assets.getSound("monkeyAttack1"));
			soundManager.addSound("monkeyAttack2",assets.getSound("monkeyAttack2"));
			soundManager.addSound("monkeyHit",assets.getSound("monkeyHit"));
			soundManager.addSound("monkeyLoop",assets.getSound("monkeyLoop"));
			soundManager.addSound("nigelAttack",assets.getSound("nigelAttack"));
			soundManager.addSound("nigelHit",assets.getSound("nigelHit"));
			soundManager.addSound("nigelWarning",assets.getSound("nigelWarning"));
			soundManager.addSound("wingflap",assets.getSound("wingflap"));

			treeLayer = new Array();
			paused = false;
			endingStarted = false;
			stopUpdating = false;
			warningCount = 0;
			fruitArray = new Array();
			fruitLayer = new Sprite();
			energy = 100;
			gameOver = false;
			birdsFreed = 0;
			treeRate = 3;
			scrollSpeed = 150.0;

			background = new Background(assets);
			background.scrollSpeed = scrollSpeed;
			sceneRoot.addChild(background);

			backBtn = new Button(Root.assets.getTexture("Home_Rio"),"",Root.assets.getTexture("Home_Rio"));
			backBtn.x = Costanza.SCREEN_START_X + Costanza.STAGE_OFFSET;
			backBtn.y = Costanza.STAGE_HEIGHT - backBtn.height;
			backBtn.addEventListener(starling.events.Event.TRIGGERED, GoBack);

			energyMeter = new EnergyMeter(assets);
			energyMeter.x = Costanza.SCREEN_START_X + Costanza.STAGE_OFFSET;
			energyMeter.y = 0;
			energyMeter.SetEnergy(energy);

			counterBar = new Counter(assets);
			counterBar.x = Costanza.STAGE_WIDTH - counterBar.width - 171 - Costanza.STAGE_OFFSET;
			counterBar.y = Costanza.STAGE_HEIGHT - counterBar.height;

			scoreCounter = new ScoreCounter(assets.getTexture("SCORE"));
			scoreCounter.x = Costanza.STAGE_WIDTH/2 - scoreCounter.width/2;
			scoreCounter.y = 0;

			greatJob = new Image(assets.getTexture("GreatJob"));
			greatJob.x = Costanza.STAGE_WIDTH/2 - greatJob.width/2;
			greatJob.y = Costanza.STAGE_HEIGHT/2 - greatJob.height/2;;
			greatJob.alpha = 0;

			gameOverImage = new Image(assets.getTexture("GameOver"));
			gameOverImage.x = Costanza.STAGE_WIDTH/2 - gameOverImage.width/2;
			gameOverImage.y = Costanza.STAGE_HEIGHT/2 - gameOverImage.height/2;;
			gameOverImage.alpha = 0;

			nigelWarning = new MovieClip(assets.getTextures("NigelWarning_"),12);
			nigelWarning.x = Costanza.STAGE_WIDTH - nigelWarning.width - 171 - Costanza.STAGE_OFFSET;
			nigelWarning.y = 0;
			nigelWarning.stop();
			nigelWarning.loop = false;
			nigelWarning.visible = false;

			juiceParticles = new ParticleEmitter(new Image(assets.getTexture("juiceDroplet")),false);
			juiceParticles.x = 0;
			juiceParticles.y = 0;

			//add Stuff to scene
			SetupLevel();
			sceneRoot.addChild(juiceParticles);
			sceneRoot.addChild(fruitLayer);
			SetupBadGuys();
			testQuad = new Quad(50,50, Color.YELLOW);
			testQuad.visible = false;
			sceneRoot.addChild(testQuad);
			SetupPrincess();
			SetupHero();

			foreground = new Foreground(assets);
			foreground.scrollSpeed = scrollSpeed;

			sceneRoot.addChild(foreground);
			sceneRoot.addChild(backBtn);
			sceneRoot.addChild(greatJob);
			sceneRoot.addChild(gameOverImage);
			sceneRoot.addChild(nigelWarning);
			sceneRoot.addChild(energyMeter);
			sceneRoot.addChild(counterBar);
			sceneRoot.addChild(scoreCounter);

			Starling.juggler.add(nigelWarning);

			if(tutorialMode)
			{
				difficultySelection = new Image(assets.getTexture("Difficulty"));
				sceneRoot.addChild(difficultySelection);
				easyButton = new Button(assets.getTexture("easy"),"",assets.getTexture("easy over"));
				easyButton.x = 225 + Costanza.STAGE_OFFSET;
				easyButton.y = 265;
				easyButton.addEventListener(starling.events.Event.TRIGGERED, easyMode);
				sceneRoot.addChild(easyButton);

				hardButton = new Button(assets.getTexture("hard"),"",assets.getTexture("hard over"));
				hardButton.x = 702 + Costanza.STAGE_OFFSET;
				hardButton.y = 265;
				hardButton.addEventListener(starling.events.Event.TRIGGERED, hardMode);
				sceneRoot.addChild(hardButton);

				//Start the game
				dispatchEventWith(Root.SCENE_LOADED,true);
				//TweenMax.delayedCall(0.0, StartGame);
			}
			else
			{
				blu.easyMode = isEasy;
				blu.start = true;
				TweenMax.delayedCall(treeRate, AddTree);
				TweenMax.delayedCall(20,NigelWarning);
				StartGame();
			}
		}

		private function easyMode(event:starling.events.Event):void
		{
			//trace("EASY MODE");

			if(!modeSelected)
			{
				modeSelected = true;
				sceneRoot.removeChild(easyButton);
				sceneRoot.removeChild(hardButton);
				sceneRoot.removeChild(difficultySelection);
				GAME_TYPE = 0;
				tutorialImage = new Image(assets.getTexture("TUTORIAL easy"));
				sceneRoot.addChild(tutorialImage);
				tutorialImage.addEventListener(TouchEvent.TOUCH, TapTutorial);
				blu.easyMode = true;
				isEasy = true;
				StartGame();
			}
		}

		private function hardMode(event:starling.events.Event):void
		{
			//trace("HARD MODE");

			if(!modeSelected)
			{
				modeSelected = true;
				sceneRoot.removeChild(easyButton);
				sceneRoot.removeChild(hardButton);
				sceneRoot.removeChild(difficultySelection);
				GAME_TYPE = 1;
				tutorialImage = new Image(assets.getTexture("TUTORIAL hard"));
				sceneRoot.addChild(tutorialImage);
				tutorialImage.addEventListener(TouchEvent.TOUCH, TapTutorial);
				blu.easyMode = false;
				isEasy = false;
				StartGame();
			}
		}

		private function StartGame():void
		{
			currentTime = getTimer();
			lastTime = currentTime;

			energy = 100;
			energyMeter.SetEnergy(energy);
			currentTime = getTimer();
			lastTime = currentTime;

			sceneRoot.addEventListener(TouchEvent.TOUCH, Tap);
			sceneRoot.addEventListener(starling.events.Event.ENTER_FRAME, Update);
			if(tutorialMode)
			{
				TweenMax.delayedCall(3.5,soundManager.playSound,["mainLoop",Costanza.musicVolume, 999]);
				if(isEasy)
					soundManager.playSound("EasyVO",Costanza.voiceVolume);
				else
					soundManager.playSound("HardVO",Costanza.voiceVolume);
				TweenMax.delayedCall(10,soundManager.playSound,["monkeyLoop",Costanza.musicVolume, 999]);
			}
			else
			{
				soundManager.playSound("mainLoop",Costanza.musicVolume, 999);
				TweenMax.delayedCall(6.5,soundManager.playSound,["monkeyLoop",Costanza.musicVolume, 999]);
			}
		}

		private function onAppActivate(e:flash.events.Event):void
		{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(paused == true)
			{
				//we need to catch our time up
				currentTime = getTimer();
				lastTime = currentTime;
				paused = false;
			}
		}

		private function onAppDeactivate(e:flash.events.Event):void
		{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if(paused == false) { paused = true; }
		}

		private function SetupLevel():void
		{
			treeSprites = new Sprite();
			sceneRoot.addChild(treeSprites);
			if(!treeLayer.length > 0)
			{
				for(var i:int = 0; i < 5; i++)
				{
					//Add A Tree
					var tree:Tree = new Tree(MathHelpers.randomIntRange(1,4), assets);
					tree.x = 1600 + Costanza.STAGE_OFFSET;
					treeLayer.push(tree);
					treeSprites.addChild(tree);
					tree.DeActivate();
				}
			}
		}

		private function SetupBadGuys():void
		{
			//Nigel is going to start offscreen and come in
			//from the far right. He'll move in quite fast and
			//B-Line straight for the player.
			nigel = new Nigel(assets);
			nigel.x = 1600;
			nigel.y = 0;
			sceneRoot.addChild(nigel);
		}

		private function SetupPrincess():void
		{
			jewel = new Jewel(assets);
			jewel.x = 2600;
			sceneRoot.addChild(jewel);
		}

		private function SetupHero():void
		{
			blu = new Blu(assets);
			blu.x = BLU_START_X;
			blu.y = 350;
			sceneRoot.addChild(blu);
		}

		public function Update(e:starling.events.Event):void
		{
			if(paused == false)
			{
				//update the Timer
				currentTime = getTimer();
				var timePassed:Number = (currentTime - lastTime) * 0.001;
				WorldClock.clock.advanceTime(-1);
				if(!stopUpdating && !gameOver)
				{
					//move our layers forward
					MoveLayers(timePassed);
					UpdateFruit(timePassed);
					blu.Update(timePassed);
					nigel.Update(timePassed);
					if(endingStarted)
					{
						MoveJewel(timePassed);
					}
					else
					{
						if(!tutorialMode)
							UpdateEnergy(timePassed);
					}
					//time for checking collisions, this is for all non-tree related collisions
					CheckCollision();
					//set our current time to our last time for next frame
				}
				lastTime = currentTime;
			}
		}

		private function CheckCollision():void
		{
			//this Collision is for Nigel and Fruit. Fruit Keeps us alive,
			//Nigel prevents us from actually getting to the fruit, thus killing us.
			var r1p1:Point, r1p2:Point, r1p3:Point, r1p4:Point // rectangle 1 corners
			var r2p1:Point, r2p2:Point, r2p3:Point, r2p4:Point // rectangle 2 corners
			var quad1:Quad = blu.GetQuad();

			r1p1 = new Point(quad1.x, quad1.y);
			//trace(r1p1.toString());
			r1p2 = new Point(quad1.x + quad1.width, quad1.y);
			//trace(r1p2.toString());
			r1p3 = new Point(quad1.x + quad1.width, quad1.y + quad1.height);
			//trace(r1p3.toString());
			r1p4 = new Point(quad1.x, quad1.y + quad1.height);
			//trace(r1p4.toString());
			r2p1 = nigel.GetTopLeft();
			//trace(r2p1.toString());
			r2p2 = nigel.GetTopRight();
			//trace(r2p2.toString());
			r2p3 = nigel.GetBottomRight();
			//trace(r2p3.toString());
			r2p4 = nigel.GetBottomLeft();
			//trace(r2p4.toString());
			var collision:Boolean = FastCollisions.rectangles(
				r1p1.x, r1p1.y, r1p2.x, r1p2.y, r1p3.x, r1p3.y, r1p4.x, r1p4.y,
				r2p1.x, r2p1.y, r2p2.x, r2p2.y, r2p3.x, r2p3.y, r2p4.x, r2p4.y);
			if(collision)
			{
				if(!blu.IsHit())
				{
					energy -= 20;
					soundManager.playSound("nigelHit",Costanza.soundVolume);
				}
				blu.BigHit();
				BigKnockBackBlu();
			}
			//Fruit Collision
			for(var i:int = fruitArray.length-1; i >= 0; --i)
			{
				var tmpFruit:Fruit = fruitArray[i] as Fruit;
				if(blu.GetQuad().bounds.intersects(tmpFruit.GetBounds()))
				{
					//play the splash
					if(!tmpFruit.exploded)
					{
						tmpFruit.Explode();
						juiceParticles.CreatePointedBurst(5,new Point(energyMeter.x + energyMeter.width/2 ,energyMeter.y + energyMeter.height - 20),new Point(tmpFruit.x,tmpFruit.y));
						energy += 10;
						scoreCounter.score += 10;
						if(energy > MAX_ENERGY)
							energy = MAX_ENERGY;
						soundManager.playSound("fruitSplash",Costanza.soundVolume);
					}
				}
			}
		}

		private function MoveLayers(timePassed:Number):void
		{
			background.Update(timePassed);
			for(var l:int = treeLayer.length - 1; l >= 0; --l)
			{
				var sprTree:Tree = treeLayer[l] as Tree;
				if(sprTree.isActive() == true)
				{
					if(MoveTree(sprTree, timePassed) == false)
					{
						sprTree.DeActivate();
					}
					else
					{
						CheckTreeCollision(sprTree);
					}
				}
			}
			foreground.Update(timePassed);
		}

		private function UpdateFruit(timePassed:Number):void
		{
			for(var i:int = fruitArray.length-1; i >= 0; --i)
			{
				var tmpFruit:Fruit = fruitArray[i] as Fruit;
				tmpFruit.x -= scrollSpeed * timePassed;
				if(tmpFruit.x + tmpFruit.width < 0)
				{
					tmpFruit.Destroy();
					fruitArray.splice(i, 1);
					fruitLayer.removeChild(tmpFruit);
				}
			}
		}

		private function MoveJewel(timePassed:Number):void
		{
			if(jewel.x > 600)
				jewel.x -= scrollSpeed * timePassed;
			else
				SaveJewel();
		}

		private function SaveJewel():void
		{
			soundManager.stopSound("mainLoop");
			soundManager.stopSound("monkeyLoop");
			soundManager.playSound("cheer",Costanza.soundVolume);
			sceneRoot.removeEventListener(TouchEvent.TOUCH, Tap);
			stopUpdating = true;
			scrollSpeed = 0;
			background.scrollSpeed = 0;
			foreground.scrollSpeed = 0;
			blu.PlayEndGlide();
			TweenMax.to(blu, 1.5, {x: 760, y: 380, onComplete:RescueJewel});
		}

		private function RescueJewel():void
		{
			blu.EndAttack();
			TweenMax.delayedCall(0.7, TweenMax.to,[blu, 0.5,{x: blu.x - 60, y:blu.y - 40}]);
			TweenMax.delayedCall(0.8, jewel.Free);
			TweenMax.delayedCall(2, ShowEnding);
		}

		private function ShowEnding():void
		{
			TweenMax.to(greatJob, 2, {alpha: 1});
			sceneRoot.addEventListener(TouchEvent.TOUCH, CheckHighscores);
		}

		private function CheckTreeCollision(tree:Tree):void
		{
			//we need to check the hero vs all the cages/monkeys
			if((tree.x + tree.GetMonkey().x) < blu.x + 125)
			{
				//attack Blu
				if(!tree.GetMonkey().attacked)
				{
					tree.GetMonkey().AttackBlu();
					var rand:int = MathHelpers.randomIntRange(1,2);
					soundManager.playSound("monkeyAttack"+rand,Costanza.soundVolume);
				}
			}
			if(blu.IsHit() == false)
			{
				if(blu.GetQuad().bounds.intersects(tree.GetMonkeyCollision().bounds))
				{
					if(!blu.IsHit())
						energy -= 10;
					blu.Hit();
					var direction:int = blu.y > tree.GetMonkey().y + tree.GetMonkey().height/2 ? 1:-1;
					KnockAwayBlu(direction);
					soundManager.playSound("monkeyHit",Costanza.soundVolume);
				}
				if(tree.hasCage)
				{
					if(blu.GetQuad().bounds.intersects(tree.GetCageCollision().bounds))
					{
						tree.hasCage = false;
						trace("Attacking");
						blu.Attack();
						tree.DropCage();

						var rnd:int = MathHelpers.randomIntRange(1,2);
						var birdSound:String = "freebird"+rnd;
						soundManager.playSound(birdSound,Costanza.soundVolume);
						birdsFreed++;
						scoreCounter.score += 100;

						if(GAME_TYPE == 0)
						{
							scrollSpeed += 10;
							treeRate -= 0.15;
						}
						else
						{
							scrollSpeed += 20;
							treeRate -= 0.2;
						}
						background.scrollSpeed = scrollSpeed;
						foreground.scrollSpeed = scrollSpeed;
						if(treeRate < 0.1)
							treeRate = 0.1;

						var id:int = tree.GetCageID();
						TweenMax.delayedCall(1.5, counterBar.AddBird,[id]);
						//counterBar.AddBird(tree.GetCageID());
						if(birdsFreed >= BIRD_RESCUE_COUNT)
						{
							if(!endingStarted)
							{
								endingStarted = true;
								StartEnding();
							}
						}
					}
				}
			}
		}

		private function ScrollObject(img:Image, speed:Number, timePassed:Number):Boolean
		{
			img.x -= speed * timePassed;
			if((img.x + img.width) < 0)
			{
				return false;
			}
			return true;
		}

		private function AddTree():void
		{
			var tree:Tree;
			var treefound:Boolean = false;
			//first we go through our list of trees and see if any are deactivated, and activate them
			for(var t:int = 0; t < treeLayer.length; t++)
			{
				tree = treeLayer[t] as Tree;
				if(tree.isActive() == false)
				{
					trace("recycling a tree");
					treefound = true;
					tree.Activate(assets);
					break;
				}
			}
			if(!treefound)
			{
				trace("Adding a tree");
				//Add A Tree
				tree = new Tree(MathHelpers.randomIntRange(1,4), assets);
				tree.x = 1600 + Costanza.STAGE_OFFSET;
				treeLayer.push(tree);
				treeSprites.addChild(tree);
			}

			//then add some fruit based on which tree type it is
			//then we add between 3 and 5 fruit in a line
			var fruitHeight:Number;
			var tmpFruit:Fruit;
			switch(tree.GetId())
			{
				case 1:
					fruitHeight = MathHelpers.randomNumberRange(350,550);
					for(var i:int = 0; i < 3; i++)
					{
						tmpFruit = new Fruit(assets);
						tmpFruit.x = tree.x+125 + i * 75;
						tmpFruit.y = fruitHeight;
						fruitArray.push(tmpFruit);
						fruitLayer.addChild(tmpFruit);
					}
					break;
				case 2:
					fruitHeight = MathHelpers.randomNumberRange(0,1) == 1 ? 150 : 500;
					for(var j:int = 0; j < 3; j++)
					{
						tmpFruit = new Fruit(assets);
						tmpFruit.x = tree.x+75 + j * 75;
						tmpFruit.y = fruitHeight;
						fruitArray.push(tmpFruit);
						fruitLayer.addChild(tmpFruit);
					}
					break;
				case 3:
					fruitHeight = MathHelpers.randomNumberRange(150,350);
					for(var k:int = 0; k < 3; k++)
					{
						tmpFruit = new Fruit(assets);
						tmpFruit.x = tree.x + 50 + k * 75;
						tmpFruit.y = fruitHeight;
						fruitArray.push(tmpFruit);
						fruitLayer.addChild(tmpFruit);
					}
					break;
				case 4:
					fruitHeight = MathHelpers.randomNumberRange(150,350);
					for(var l:int = 0; l < 3; l++)
					{
						tmpFruit = new Fruit(assets);
						tmpFruit.x = tree.x + 50 + l * 75;
						tmpFruit.y = fruitHeight;
						fruitArray.push(tmpFruit);
						fruitLayer.addChild(tmpFruit);
					}
					break;
			}
			
			TweenMax.delayedCall(treeRate, AddTree);
		}

		private function MoveTree(spr:Sprite, timePassed):Boolean
		{
			spr.x -= scrollSpeed * timePassed;
			if((spr.x + spr.width) < 0)
			{
				return false;
			}
			return true;
		}

		private function StartEnding():void
		{
			trace("Ending Started");
			TweenMax.killDelayedCallsTo(NigelWarning);
			TweenMax.killDelayedCallsTo(AddTree);
		}

		private function UpdateEnergy(timePassed:Number):void
		{
			//reduce energy by 2 per second
			//trace("energyBefore: " + energy);
			energy -= 2 * timePassed;
			//trace("energyAfter: " + energy);
			if(energy <= 50)
			{
				blu.StartSweating(true);
			}
			else if(energy > 50)
			{
				blu.StartSweating(false);
			}
			if(energy <= 0)
			{
				energy = 0;
				EndGame();
			}
			energyMeter.SetEnergy(Math.floor(energy));
		}

		private function Tap(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(stage);
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				trace("Tappy tappy");
				blu.StartFly(new Point(touch.globalX-140, touch.globalY-40));
			}
			else if(touch.phase == TouchPhase.MOVED)
			{
				blu.Fly(new Point(touch.globalX-140, touch.globalY-40));
			}
			else if(touch.phase == TouchPhase.ENDED)
			{
				blu.EndFly();
			}
		}

		private function TapTutorial(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(stage);
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				tutorialImage.removeEventListener(TouchEvent.TOUCH, TapTutorial);
				sceneRoot.removeChild(tutorialImage);
				tutorialMode = false;
				blu.start = true;
				RemoveTutorial();
			}
		}

		private function RemoveTutorial():void
		{
			sceneRoot.removeChild(tutorialImage);
			TweenMax.delayedCall(treeRate, AddTree);
			TweenMax.delayedCall(20,NigelWarning);
		}

		private function KnockAwayBlu(direction:int):void
		{
			TweenMax.to(blu, 0.5, {y: blu.y + 200 * direction});
		}

		private function BigKnockBackBlu():void
		{
			if(!blu.IsHit())
				TweenMax.to(blu, 0.5, {x: blu.x - 100});
		}

		private function NigelWarning():void
		{
			//trace("Start the Warning");
			warningCount = 0;
			blu.Warning();
			nigelWarning.visible = true;
			nigelWarning.play();
			nigelWarning.addEventListener(starling.events.Event.COMPLETE, LoopWarning);
			soundManager.playSound("nigelWarning", Costanza.soundVolume);
		}

		private function LoopWarning():void
		{
			nigelWarning.stop();
			//trace("Looping the Warning");
			if(warningCount < 2)
			{
				warningCount++;
				nigelWarning.play();
			}
			else
			{
				nigelWarning.visible = false;
				nigelWarning.removeEventListener(starling.events.Event.COMPLETE, LoopWarning);
				if(!endingStarted)
				{
					LaunchNigel();
					soundManager.playSound("nigelAttack",Costanza.soundVolume);
				}
			}
		}

		private function LaunchNigel():void
		{
			nigel.Launch(blu.x+20, blu.y+50);
			TweenMax.delayedCall(MathHelpers.randomIntRange(10,20),NigelWarning);
		}

		private function EndGame():void
		{
			scrollSpeed = 0;
			background.scrollSpeed = 0;
			foreground.scrollSpeed = 0;
			TweenMax.killAll();
			soundManager.stopAllSounds();
			soundManager.playSound("gameOver",Costanza.musicVolume);
			gameOver = true;
			sceneRoot.removeEventListener(TouchEvent.TOUCH, Tap);
			TweenMax.to(blu, 3, { y: 900 });
			TweenMax.to(gameOverImage, 2, {alpha: 1});
			sceneRoot.addEventListener(TouchEvent.TOUCH, CheckHighscores);
		}

		private function CheckHighscores(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(sceneRoot);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(soundManager.soundIsPlaying("mainLoop")) { soundManager.stopSound("mainLoop"); }
				if(soundManager.soundIsPlaying("monkeyLoop")) { soundManager.stopSound("monkeyLoop"); }
				if(soundManager.soundIsPlaying("gameOver")) { soundManager.stopSound("gameOver"); }
				if(soundManager.soundIsPlaying("cheer")) { soundManager.stopSound("cheer"); }

				for(var i:int = 0; i < treeLayer.length; i++)
				{
					var tree:Tree = treeLayer[i] as Tree;
					tree.DeActivate();
				}
				nigelWarning.removeEventListener(starling.events.Event.COMPLETE, LoopWarning);
				sceneRoot.removeEventListener(TouchEvent.TOUCH, Tap);
				sceneRoot.removeEventListener(starling.events.Event.ENTER_FRAME, Update);
				sceneRoot.removeEventListener(TouchEvent.TOUCH, CheckHighscores);
				if(GAME_TYPE == 0) { highScore = new HighScore("BlusRescueEasy",scoreCounter.score); }
					else { highScore = new HighScore("BlusRescueHard",scoreCounter.score); }
				sceneRoot.addChild(highScore);
				//GameOver();
			}
		}

		private function GoBack(e:starling.events.Event):void
		{
			if(soundManager.soundIsPlaying("mainLoop")) { soundManager.stopSound("mainLoop"); }
			if(soundManager.soundIsPlaying("monkeyLoop")) { soundManager.stopSound("monkeyLoop"); }
			GameOver();
		}

		private function GameOver():void
		{
			TweenMax.killAll();
			soundManager.stopAllSounds();

			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.ACTIVATE, onAppActivate);
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.DEACTIVATE, onAppDeactivate);

			dispatchEventWith(Root.LOAD_LOBBY, true);
		}

		private function TouchReset():void
		{
			//sceneRoot.removeChildren();
			//AddObjects();
			ReplayGame();
		}

		private function ReplayGame():void
		{
			sceneRoot.removeChild(highScore);
			gameOverImage.alpha = 0;
			greatJob.alpha = 0;
			for(var i:int = fruitArray.length-1; i >= 0; --i)
			{
				var tmpFruit:Fruit = fruitArray[i] as Fruit;
				tmpFruit.Destroy();
				fruitArray.splice(i, 1);
				fruitLayer.removeChild(tmpFruit);
			}

			currentTime = 0.0;
			lastTime = 0.0;

			paused = false;
			endingStarted = false;
			stopUpdating = false;
			warningCount = 0;
			energy = 100;
			gameOver = false;
			birdsFreed = 0;
			treeRate = 3;
			scrollSpeed = 150.0;

			scoreCounter.score = 0;

			nigel.x = 1600;
			nigel.y = 0;

			jewel.x = 2600;
			jewel.Reset();

			blu.x = BLU_START_X;
			blu.y = 350;
			blu.Reset();

			background.scrollSpeed = scrollSpeed;
			foreground.scrollSpeed = scrollSpeed;

			counterBar.Reset();

			blu.easyMode = isEasy;
			blu.start = true;
			TweenMax.delayedCall(treeRate, AddTree);
			TweenMax.delayedCall(20,NigelWarning);
			StartGame();
		}

		private function KillAll():void
		{
			Starling.juggler.purge();
			// signaling back up to load lobby
			for(var i:int = 0; i < treeLayer.length; i++)
			{
				var tree:Tree = treeLayer[i] as Tree;
				tree.Destroy();
			}
			treeLayer.length = 0;
			treeSprites.dispose();
			assets.dispose();
			blu.Destroy();
			nigel.Destroy();
			jewel.Destroy();
		}
	}
}