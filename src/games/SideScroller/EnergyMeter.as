package games.SideScroller
{
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.AssetManager;

	public class EnergyMeter extends Sprite
	{
		private var topImg:Image;

		private var fillImg:Image;
		private var fillMask:Sprite;

		private var bottomImg:Image;

		private var meterHeight:Number;

		public static const MAX_ENERGY:int = 100;

		public function EnergyMeter(assets:AssetManager)
		{
			super();
			topImg = new Image(assets.getTexture("Meter_Top"));

			fillImg = new Image(assets.getTexture("Juice"));
			fillImg.x = 13;
			fillImg.y = 10;

			meterHeight = fillImg.height;

			fillMask = new Sprite();
			fillMask.addChild(fillImg);
			fillMask.clipRect = new Rectangle(13,0,fillImg.width, fillImg.height);

			bottomImg = new Image(assets.getTexture("Meter_Bottom"));

			addChild(bottomImg);
			addChild(fillMask);
			addChild(topImg);

			SetEnergy(MAX_ENERGY);
		}

		public function SetEnergy(energy:int):void
		{
			//trace("settingEnergy: " + energy);
			fillImg.y = (meterHeight - meterHeight * (energy/MAX_ENERGY)) + 15;
			//fillMask.y = -(meterHeight - meterHeight * (energy/MAX_ENERGY));
			//fillMask.height = meterHeight * (energy/MAX_ENERGY);
		}
	}
}