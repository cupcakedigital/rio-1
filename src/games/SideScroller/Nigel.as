package games.SideScroller
{
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.Color;

	public class Nigel extends Sprite
	{
		// Dragon Bones
		private var factory:StarlingFactory;

		private var nigelArmature:Armature;
		private var nigelSprite:Sprite;

		private var target:Point = new Point(0,0);

		private var dive:Boolean = false;
		private var attack:Boolean = false;
		private var angle:Number = 0;
		
		private var hitQuad:Quad;
		
		private static const SPEED:Number = 750;

		public function Nigel(assets:AssetManager)
		{
			super();

			target = new Point(0,0);
			dive = false;
			attack = false;
			angle = 0;

			Load(assets);
		}

		private function Load(assets:AssetManager):void
		{
			// DragonBones Setup
			factory = new StarlingFactory();

			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("nigelskeleton"));
			factory.addSkeletonData(skeletonData);

			var texture:Texture = assets.getTexture("nigeltexture");
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("nigeltexture"),true);
			factory.addTextureAtlas(textureAtlas);

			nigelArmature = factory.buildArmature("Nigel");
			nigelArmature.animation.gotoAndPlay("dive");
			WorldClock.clock.add(nigelArmature);

			pivotX = width/2;
			pivotY = height/2;

			nigelArmature.display.x = 0;
			nigelArmature.display.y = 0;
			nigelSprite = nigelArmature.display as Sprite;
			addChild(nigelSprite);

			hitQuad = new Quad(140,50, Color.GREEN);
			hitQuad.pivotX = hitQuad.width/2;
			hitQuad.pivotY = hitQuad.height/2;
			hitQuad.x = nigelSprite.x;
			hitQuad.y = nigelSprite.y;
			hitQuad.visible = false;
			addChild(hitQuad);
		}

		public function Destroy():void
		{
			WorldClock.clock.remove(nigelArmature);
			factory.dispose(false);
			nigelArmature.dispose();
			nigelSprite.dispose();
			dispose();
		}

		public function Update(timePassed:Number):void
		{
			if(dive)
			{
				var dx:int = Math.cos(angle) * SPEED;
				var dy:int = Math.sin(angle) * SPEED;

				x -= dx * timePassed;
				y -= dy * timePassed;
				hitQuad.x = nigelSprite.x;
				hitQuad.y = nigelSprite.y;
			}
			if(x < -150)
				Reset();
		}

		public function Launch(newX:Number, newY:Number):void
		{
			target = new Point(newX, newY);
			angle = Math.atan2(y - target.y,x - target.x);
			nigelSprite.rotation = angle;
			dive = true;
		}

		public function GetTopLeft():Point
		{
			return new Point(x + hitQuad.x - 50, y + hitQuad.y - 50);
		}

		public function GetTopRight():Point
		{
			return new Point(x + hitQuad.x + hitQuad.width, y + hitQuad.y);
		}

		public function GetBottomRight():Point
		{
			return new Point(x + hitQuad.x + hitQuad.width/4, y + hitQuad.y + hitQuad.height/4);
		}

		public function GetBottomLeft():Point
		{
			return new Point(x + hitQuad.x, y + hitQuad.y + hitQuad.height);
		}

		private function GetRotatedRectPoint(point:Point):Point
		{
			var ix:Number = hitQuad.pivotX;
			var iy:Number = hitQuad.pivotY;

			var m:Matrix = new Matrix( 1,0,0,1, point.x - ix, point.y - iy);
			m.rotate(angle);
			return new Point( m.tx + ix, m.ty + iy);
		}

		private function Reset():void
		{
			attack = false;
			dive = false;
			x = 1600;
			y = 0;
		}
	}
}