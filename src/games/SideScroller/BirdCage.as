package games.SideScroller
{
	import com.greensock.TweenMax;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;

	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	import utils.MathHelpers;

	public class BirdCage extends Sprite
	{
		// Dragon Bones
		private var factory:StarlingFactory;

		private var birdArmature:Armature;
		private var birdSprite:Sprite;

		private var cage:Image;

		private var ID:int;

		private var hitQuad:Quad;
		private var freed:Boolean = false;

		public function BirdCage(assets:AssetManager)
		{
			//random Bird
			ID = MathHelpers.randomIntRange(1,4);
			// DragonBones Setup
			factory = new StarlingFactory();

			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("birdskeleton"+ID));
			factory.addSkeletonData(skeletonData);

			var texture:Texture = assets.getTexture("birdtexture"+ID);
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("birdtexture"+ID),true);
			factory.addTextureAtlas(textureAtlas);
			trace("ID: " + ID);
			switch(ID)
			{
				case 1:
					birdArmature = factory.buildArmature("Characters/BirdA");
					break;
				case 2:
					birdArmature = factory.buildArmature("Characters/BirdB");
					break;
				case 3:
					birdArmature = factory.buildArmature("Characters/BirdC");
					break;
				case 4:
					birdArmature = factory.buildArmature("Characters/BirdD");
					break;
			}

			birdArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(birdArmature);

			birdArmature.display.x = 0;
			birdArmature.display.y = 0;
			birdSprite = birdArmature.display as Sprite;
			addChild(birdSprite);

			cage = new Image(assets.getTexture("cage"));
			cage.x = -45;
			addChild(cage);

			hitQuad = new Quad(75,100, Color.YELLOW);
			hitQuad.visible = false;
			hitQuad.x = -75;
			hitQuad.y = 0;
			addChild(hitQuad);
		}

		public function GetRec():Quad
		{
			return hitQuad;
		}
		
		public function GetID():int
		{
			return ID;
		}

		public function Break():void
		{
			if(!freed)
			{
				freed = true;
				birdArmature.animation.gotoAndPlay("free");
				TweenMax.to(birdSprite, 5, {x: -400, y: -400, onComplete: Reset});
				TweenMax.to(cage, 2, {y: y + 200, rotation: deg2rad(90), alpha: 0});
			}
		}

		public function Reset():void
		{
			birdArmature.animation.gotoAndPlay("idle");
			cage.x = -45;
			cage.y = 0;
			cage.alpha = 1;
			cage.rotation = 0;
			birdArmature.display.x = 0;
			birdArmature.display.y = 0;
			birdSprite.x = 0;
			birdSprite.y = 0;
			freed = false;
		}

		public function Destroy():void
		{
			WorldClock.clock.remove(birdArmature);
			factory.dispose(false);
			birdArmature.dispose();
			birdSprite.dispose();
			dispose();
		}
	}
}