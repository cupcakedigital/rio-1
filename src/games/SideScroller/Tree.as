package games.SideScroller
{
	import com.greensock.TweenMax;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	
	import utils.MathHelpers;

	public class Tree extends Sprite
	{
		private var treeImage:Image;
		private var id:int;
		private var monkey:Monkey;
		private var birdCage:BirdCage;

		public var hasCage:Boolean = false;
		public var treeActive:Boolean = true;

		public function Tree(newId:int, assets:AssetManager)
		{
			super();
			id = newId;
			treeImage = new Image(assets.getTexture("Tree_0" + id));
			addChild(treeImage);
			monkey = new Monkey(assets, 0);
			birdCage = new BirdCage(assets);
			birdCage.visible = false;
			//on top of the tree, we need to create the cages with birds
			//and the monkeys who will be attack the player.
			PlacePoints(assets);
			addChild(birdCage);
		}

		private function PlacePoints(assets:AssetManager):void
		{
			switch(id)
			{
				case 1: // Big Tree, holds the most cages and monkeys
				{
					y = 0;
					//Monkeys: swinging
					//monkey = new Monkey(assets, 1);
					monkey.SetMonkey(1);
					switch(MathHelpers.randomIntRange(0,1))
					{
						case 0:
							monkey.x = 115;
							monkey.y = 250;
							break;
						case 1:
							monkey.x = 370;
							monkey.y = 220;
							break;
					}
					addChild(monkey);
					//Adding the birds
					switch(MathHelpers.randomIntRange(1,6)) //6 different places to have a cage on the big tree
					{
						case 1:
							birdCage.x = 105;
							birdCage.y = 380;
							break;
						case 2:
							birdCage.x = 160;
							birdCage.y = 280;
							break;
						case 3:
							birdCage.x = 360;
							birdCage.y = 350;
							break;
						case 4:
							birdCage.x = 285;
							birdCage.y = 450;
							break;
						case 5:
							birdCage.x = 215;
							birdCage.y = 450;
							break;
						case 6:
							birdCage.x = 250;
							birdCage.y = 145;
							break;
					}
					birdCage.visible = true;
					hasCage = true;
					break;
				}
				case 2: // Smaller tree, holds less cages and monkey
				{
					y = MathHelpers.randomIntRange(230,275);
					//Monkeys: grabber
					//monkey = new Monkey(assets, 0);
					monkey.SetMonkey(0);
					monkey.x = 130;
					monkey.y = 0;
					addChild(monkey);
					break;
				}
				case 3://bigger monkey tree, holds up to 2 monkeys
				{
					y = MathHelpers.randomIntRange(400,500);
					//Monkeys: grabber
					//monkey = new Monkey(assets, 0);
					monkey.SetMonkey(0);
					monkey.x = 130;
					monkey.y = 60;
					addChild(monkey);
					break;
				}
				case 4://small monkey tree, only holds 1 monkey to swipe up (low tree)
				{
					y = 500;
					//Monkeys: grabber
					//monkey = new Monkey(assets, 0);
					monkey.SetMonkey(0);
					monkey.x = 115;
					monkey.y = 0;
					addChild(monkey);
					break;
				}
			}
		}

		public function GetMonkeyCollision():Quad
		{
			var hitQuad:Quad = new Quad(monkey.GetRec().width,monkey.GetRec().height);
			switch(monkey.ID)
			{
				case 0:
					hitQuad.x = x + monkey.x - 25;
					hitQuad.y = y + monkey.y - 75;
					break;
				case 1:
					hitQuad.x = x + monkey.x - 25;
					hitQuad.y = y + monkey.y;
					break;
			}
			return hitQuad;
		}

		public function GetCageCollision():Quad
		{
			var hitQuad:Quad = new Quad(birdCage.GetRec().width, birdCage.GetRec().height);
			hitQuad.x = x + birdCage.x + -75;
			hitQuad.y = y + birdCage.y;
			return hitQuad;
		}

		public function DropCage():void
		{
			TweenMax.delayedCall(0.5, birdCage.Break);
		}

		public function GetMonkey():Monkey
		{
			return monkey;
		}
		
		public function GetId():int
		{
			return id;
		}
		
		public function GetCageID():int
		{
			return birdCage.GetID();
		}

		public function DeActivate():void
		{
			treeActive = false;
			treeImage.visible = false;
			if(monkey)
				removeChild(monkey);

			birdCage.visible = false;
			hasCage = false;

			x = 1600 + Costanza.STAGE_OFFSET;
		}

		public function Activate(assets:AssetManager):void
		{
			id = MathHelpers.randomIntRange(1,4);
			removeChild(treeImage);
			treeImage = new Image(assets.getTexture("Tree_0" + id));
			treeImage.visible = true;
			addChild(treeImage);
			this.setChildIndex(birdCage, this.numChildren -1);
			PlacePoints(assets);
			treeActive = true;
		}

		public function isActive():Boolean
		{
			return treeActive;
		}

		public function Destroy():void
		{
			if(monkey)
				monkey.Destroy();
			if(birdCage)
				birdCage.Destroy();
			treeImage.dispose();
			dispose();
		}
	}
}