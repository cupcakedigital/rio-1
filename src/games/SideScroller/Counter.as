package games.SideScroller
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	
	public class Counter extends Sprite
	{
		private var counterBar:Image;
		private var bird1:Image;
		private var bird2:Image;
		private var bird3:Image;
		private var bird4:Image;

		private var birdLayer:Sprite = new Sprite();

		private var count:int = 0;
		public function Counter(assets:AssetManager)
		{
			super();
			birdLayer = new Sprite();
			counterBar = new Image(assets.getTexture("counterBar"));
			bird1 = new Image(assets.getTexture("CounterBird1"));
			bird2 = new Image(assets.getTexture("CounterBird2"));
			bird3 = new Image(assets.getTexture("CounterBird3"));
			bird4 = new Image(assets.getTexture("CounterBird4"));

			addChild(birdLayer);
			addChild(counterBar);
		}

		public function AddBird(ID:int):void
		{
			if(count < 10)
			{
				var tmpBird:Image = new Image(this["bird"+ID].texture);
				tmpBird.x = (counterBar.width / 10) * count;
				tmpBird.y = -53;
				birdLayer.addChild(tmpBird);
				count++;
			}
		}

		public function Reset():void
		{
			count = 0;
			birdLayer.removeChildren();
		}
	}
}