package games.SideScroller
{
	import com.greensock.TweenMax;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.deg2rad;
	
	public class Jewel extends Sprite
	{
		// Dragon Bones
		private var factory:StarlingFactory;

		private var jewelArmature:Armature;
		private var jewelSprite:Sprite;

		private var background:Image;
		private var cage:Image;
		
		private var effect:MovieClip;

		public function Jewel(assets)
		{
			super();
			Load(assets);
		}

		private function Load(assets:AssetManager):void
		{
			background = new Image(assets.getTexture("Tree_01"));
			addChild(background);
			// DragonBones Setup
			factory = new StarlingFactory();

			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("jewelskeleton"));
			factory.addSkeletonData(skeletonData);

			var texture:Texture = assets.getTexture("jeweltexture");
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("jeweltexture"),true);
			factory.addTextureAtlas(textureAtlas);

			jewelArmature = factory.buildArmature("Characters/JewelBackView");
			jewelArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(jewelArmature);

			effect = new MovieClip(assets.getTextures("jewelSave_"),12);
			effect.pivotX = effect.width/2;
			effect.pivotY = effect.height/2;
			effect.x = 215;
			effect.y = 425;
			effect.visible = false;
			effect.loop = false;
			effect.stop();
			Starling.juggler.add(effect);
			addChild(effect);

			jewelArmature.display.x = 225;
			jewelArmature.display.y = 425;
			jewelSprite = jewelArmature.display as Sprite;
			addChild(jewelSprite);

			cage = new Image(assets.getTexture("jewelCage"));
			cage.pivotX = cage.width/2;
			cage.pivotY = cage.height/2;
			cage.x = 230;
			cage.y = 415;
			addChild(cage);
		}

		public function Free():void
		{
			effect.visible = true;
			effect.play();
			jewelArmature.animation.gotoAndPlay("free");
			TweenMax.to(cage, 4, {y: cage.y + 200, rotation: deg2rad(90), alpha: 0});
		}

		public function Reset():void
		{
			jewelArmature.animation.gotoAndPlay("idle");
			cage.x = 230;
			cage.y = 415;
			cage.rotation = 0;
			cage.alpha = 1;
		}

		public function Destroy():void
		{
			Starling.juggler.remove(effect);
			WorldClock.clock.remove(jewelArmature);
			factory.dispose(false);
			jewelArmature.dispose();
			jewelSprite.dispose();
			dispose();
		}
	}
}