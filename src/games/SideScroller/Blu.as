package games.SideScroller
{
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.Color;

	public class Blu extends Sprite
	{
		// Dragon Bones
		private var factory:StarlingFactory;

		private var bluArmature:Armature;
		private var bluSprite:Sprite;

		private var moveToPos:Point;
		private var velocity:Point;

		private var flying:Boolean = false;

		private var hitQuad:Quad;
		private var hit:Boolean = false;
		private var attacking:Boolean = false;
		
		public var start:Boolean = false;

		private var warning:Image;

		private var smallHitEffect:MovieClip;
		private var bigHitEffect:MovieClip;

		private var leftSweat:MovieClip;
		private var rightSweat:MovieClip;

		public var easyMode:Boolean = true;
		
		private static const GRAVITY:Number = 600.0;

		public function Blu(assets:AssetManager)
		{
			super();
			
			flying = false;
			hit = false;
			attacking = false;

			LoadBlu(assets);
			moveToPos = new Point(0,0);
			velocity = new Point(0,0);
		}

		private function LoadBlu(assets:AssetManager):void
		{
			// DragonBones Setup
			factory = new StarlingFactory();

			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("bluskeleton"));
			factory.addSkeletonData(skeletonData);

			var texture:Texture = assets.getTexture("blutexture");
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("blutexture"),true);
			factory.addTextureAtlas(textureAtlas);

			bluArmature = factory.buildArmature("Characters/Blu_comp");
			bluArmature.animation.gotoAndPlay("fly");
			WorldClock.clock.add(bluArmature);

			bigHitEffect = new MovieClip(assets.getTextures("largeImpact_"),12);
			bigHitEffect.pivotX = bigHitEffect.width/2;
			bigHitEffect.pivotY = bigHitEffect.height/2;
			bigHitEffect.loop = false;
			bigHitEffect.visible = false;
			Starling.juggler.add(bigHitEffect);
			bigHitEffect.addEventListener(Event.COMPLETE, BigHitComplete);
			addChild(bigHitEffect);
			
			smallHitEffect = new MovieClip(assets.getTextures("smallImpact_"),12);
			smallHitEffect.pivotX = smallHitEffect.width/2;
			smallHitEffect.pivotY = smallHitEffect.height/2;
			smallHitEffect.loop = false;
			smallHitEffect.visible = false;
			Starling.juggler.add(smallHitEffect);
			smallHitEffect.addEventListener(Event.COMPLETE, SmallHitComplete);
			addChild(smallHitEffect);

			bluArmature.display.x = 0;
			bluArmature.display.y = 0;
			bluSprite = bluArmature.display as Sprite;
			addChild(bluSprite);

			leftSweat = new MovieClip(assets.getTextures("BLU_SWEAT_"),18);
			leftSweat.loop = true;
			leftSweat.x = 35;
			leftSweat.y = -15;
			leftSweat.play();
			leftSweat.visible = false;
			Starling.juggler.add(leftSweat);
			addChild(leftSweat);

			rightSweat = new MovieClip(assets.getTextures("BLU_SWEAT_"),18);
			rightSweat.scaleX = -1;
			rightSweat.loop = true;
			rightSweat.x = 80;
			rightSweat.y = -15;
			rightSweat.play();
			rightSweat.visible = false;
			Starling.juggler.add(rightSweat);
			addChild(rightSweat);

			hitQuad = new Quad(50,50,Color.RED);
			hitQuad.visible = false;
			addChild(hitQuad);

			warning = new Image(assets.getTexture("exclaim"));
			warning.pivotX = warning.width/2;
			warning.pivotY = warning.height;
			warning.x = 60;
			warning.y = -15;
			warning.scaleY = 0;
			addChild(warning);
		}

		public function Update(timePassed:Number):void
		{
			//trace("Mode: " + easyMode.toString());
			//trace("Start: " + start);
			if(easyMode)
			{
				if(flying)
				{
					var dX:Number = moveToPos.x - x;
					var dY:Number = moveToPos.y - y;
					
					var angle:Number = Math.atan2(dY, dX);
					
					var vX:Number = Math.cos(angle) * 500;
					var vY:Number = Math.sin(angle) * 500;
					
					velocity.x = vX;
					velocity.y = vY;
					
					x += velocity.x * timePassed;
					y += velocity.y * timePassed;
					
					if(dX < 5 && dX > -5)
						x = moveToPos.x;
					if(dY < 5 && dY > -5)
						y = moveToPos.y;
					
					if( x < Costanza.SCREEN_START_X + 160) 
						x = Costanza.SCREEN_START_X + 160;
					if( x > Costanza.STAGE_WIDTH + Costanza.STAGE_OFFSET)
						x = Costanza.STAGE_WIDTH + Costanza.STAGE_OFFSET;
					if(y < 0)
						y = 0;
					if(y > Costanza.STAGE_HEIGHT)
						y = Costanza.STAGE_HEIGHT;
				}
			}
			else if(start)
			{
				if(!flying)
				{
					velocity.y += (GRAVITY * timePassed);
				}
				else if(flying && hit)
				{
					velocity.y += (GRAVITY * timePassed);
				}
				else
				{
					velocity.y -= 30;
				}

				if(velocity.y < -300)
					velocity.y = -300;
				y += velocity.y * timePassed;
				if(y > 700)
				{
					velocity.y = 0;
					y = 700;
				}
				else if(y < 50)
					y = 50;
			}
		}

		public function GetQuad():Quad
		{
			var quad:Quad = new Quad(50,50);
			quad.x = x;
			quad.y = y;
			return quad;
		}

		public function StartFly(newPos:Point):void
		{
			flying = true;
			moveToPos = newPos;
			if(!hit)
			{
				bluArmature.animation.gotoAndPlay("fly");
			}
		}

		public function Fly(newPos:Point):void
		{
			moveToPos = newPos;
		}

		public function EndFly():void
		{
			flying = false;
			bluArmature.animation.gotoAndPlay("glide");
		}

		public function Hit():void
		{
			if(!hit)
			{
				hit = true;
				smallHitEffect.visible = true;
				smallHitEffect.play();
				TweenMax.delayedCall(0.2, ResetHit);
			}
		}

		public function BigHit():void
		{
			if(!hit)
			{
				bluArmature.animation.gotoAndPlay("tumble");
				hit = true;
				bigHitEffect.visible = true;
				bigHitEffect.play();
				TweenMax.delayedCall(1.25, ResetHit);
			}
		}

		public function Warning():void
		{
			TweenMax.to(warning, 0.2,{scaleY:1.2, scaleY:0.8, onComplete:SettleWarning});
		}

		private function SettleWarning():void
		{
			TweenMax.to(warning, 0.2,{scaleY:1, scaleY:1});
			TweenMax.delayedCall(1, RemoveWarning);
		}

		private function RemoveWarning():void
		{
			trace("removing warning");
			warning.scaleY = 0;
		}

		public function Attack():void
		{
			if(!attacking)
			{
				trace("Attacking");
				bluArmature.animation.gotoAndPlay("hit");
				attacking = true;
				TweenMax.delayedCall(1, ResetAttack);
			}
		}

		public function IsHit():Boolean
		{
			return hit;
		}

		public function SetHit(value:Boolean):void
		{
			hit = value;
		}

		public function ResetHit():void
		{
			hit = false;
			if(flying)
				bluArmature.animation.gotoAndPlay("fly");
		}
		
		public function PlayEndGlide():void
		{
			bluArmature.animation.gotoAndPlay("glide");
		}

		public function EndAttack():void
		{
			trace("Attacking");
			bluArmature.animation.gotoAndPlay("hit");
			//TweenMax.delayedCall(1, bluArmature.animation.gotoAndPlay,["fly"]);
		}

		public function StartSweating(value:Boolean):void
		{
			leftSweat.visible = value;
			rightSweat.visible = value;
		}

		private function ResetAttack():void
		{
			attacking = false;
			bluArmature.animation.gotoAndPlay("fly");
		}

		private function BigHitComplete(e:Event):void
		{
			bigHitEffect.stop();
			bigHitEffect.visible = false;
		}

		private function SmallHitComplete(e:Event):void
		{
			smallHitEffect.stop();
			smallHitEffect.visible = false;
		}
		
		public function Reset():void
		{
			hit = false;
			attacking = false;
			flying = false;
		}

		public function Destroy():void
		{
			Starling.juggler.remove(bigHitEffect);
			Starling.juggler.remove(smallHitEffect);
			WorldClock.clock.remove(bluArmature);
			factory.dispose(false);
			bluArmature.dispose();
			bluSprite.dispose();
			dispose();
		}
	}
}