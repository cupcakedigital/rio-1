package games.SideScroller
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.AssetManager;

	public class Foreground extends Sprite
	{
		private var bgLayer1:BgLayer;

		private var _scrollSpeed:Number = 0;
		private var assets:AssetManager;

		public function Foreground(newAssets:AssetManager)
		{
			super();
			assets = newAssets;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

			bgLayer1 = new BgLayer(new Image(assets.getTexture("bottomPlants")),Costanza.STAGE_HEIGHT - 100);
			bgLayer1.parallax = 1;
			addChild(bgLayer1);
		}

		public function Update(timePassed:Number):void
		{
			bgLayer1.Move(scrollSpeed * timePassed);
		}
		
		public function get scrollSpeed():Number
		{
			return _scrollSpeed;
		}
		
		public function set scrollSpeed(value:Number):void
		{
			_scrollSpeed = value;
		}
	}
}