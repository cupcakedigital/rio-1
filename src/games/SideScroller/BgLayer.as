package games.SideScroller
{
	import feathers.layout.ViewPortBounds;
	
	import starling.display.Image;
	import starling.display.Sprite;

	public class BgLayer extends Sprite
	{
		private var image1:Image;
		private var image2:Image;

		private var _parallax:Number;

		public function BgLayer(img:Image, height:Number)
		{
			super();

			image1 = new Image(img.texture);
			image2 = new Image(img.texture);

			image1.x = 0;
			image1.y = height;

			image2.x = image2.width;
			image2.y = image1.y;

			addChild(image1);
			addChild(image2);
		}

		public function Move(speed:Number):void
		{
			image1.x -= speed * parallax;
			if(image1.x < -image1.width) image1.x = image2.x + image2.width - 1;

			image2.x -= speed * parallax;
			if(image2.x < -image2.width) image2.x = image1.x + image1.width - 1;
		}

		public function get parallax():Number
		{
			return _parallax;
		}

		public function set parallax(value:Number):void
		{
			_parallax = value;
		}
	}
}