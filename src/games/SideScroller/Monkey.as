package games.SideScroller
{
	import flash.geom.Rectangle;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.Color;
	
	import utils.MathHelpers;
	
	public class Monkey extends Sprite
	{
		// Dragon Bones
		private var factory:StarlingFactory;

		private var monkeyArmature:Armature;
		private var monkeySprite:Sprite;
		
		public var ID:int;
		private var hitQuad:Quad;
		
		public var attacked:Boolean = false;

		public function Monkey(assets:AssetManager, id:int)
		{
			super();
			ID = id;
			// DragonBones Setup
			factory = new StarlingFactory();
			
			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("monkeyskeleton"));
			factory.addSkeletonData(skeletonData);
			
			var texture:Texture = assets.getTexture("monkeytexture");
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("monkeytexture"),true);
			factory.addTextureAtlas(textureAtlas);

			monkeyArmature = factory.buildArmature("Characters/Monkey");
			hitQuad = new Quad(50,75, Color.BLUE);
			hitQuad.visible = false;
			switch(ID)
			{
				case 0: //standing monkey
					monkeyArmature.animation.gotoAndPlay("standingIdle" + MathHelpers.randomIntRange(1,3));
					hitQuad.x = x - 25;
					hitQuad.y = y - 75;
					break;
				case 1: //swinging monkey
					monkeyArmature.animation.gotoAndPlay("swingingIdle" + MathHelpers.randomIntRange(1,3));
					hitQuad.x = x - 25;
					hitQuad.y = y;
					break;
			}

			WorldClock.clock.add(monkeyArmature);

			monkeyArmature.display.x = 0;
			monkeyArmature.display.y = 0;
			monkeySprite = monkeyArmature.display as Sprite;
			addChild(monkeySprite);
			addChild(hitQuad);
		}

		public function AttackBlu():void
		{
			if(!attacked)
			{
				attacked = true;
				switch(ID)
				{
					case 0:
						monkeyArmature.animation.gotoAndPlay("standingGrab");
						break;
					case 1:
						monkeyArmature.animation.gotoAndPlay("swingingGrab");
						break;
				}
			}
		}

		public function GetRec():Quad
		{
			return hitQuad;
		}

		public function SetMonkey(id:int):void
		{
			attacked = false;
			ID = id;
			switch(ID)
			{
				case 0: //standing monkey
					monkeyArmature.animation.gotoAndPlay("standingIdle" + MathHelpers.randomIntRange(1,3));
					hitQuad.x = x - 25;
					hitQuad.y = y - 75;
					break;
				case 1: //swinging monkey
					monkeyArmature.animation.gotoAndPlay("swingingIdle" + MathHelpers.randomIntRange(1,3));
					hitQuad.x = x - 25;
					hitQuad.y = y;
					break;
			}
		}

		public function Destroy():void
		{
			WorldClock.clock.remove(monkeyArmature);
			factory.dispose(false);
			monkeyArmature.dispose();
			monkeySprite.dispose();
			dispose();
		}
	}
}