package games.SideScroller
{
	import com.greensock.TweenMax;
	
	import flash.geom.Rectangle;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.AssetManager;
	
	import utils.MathHelpers;
	
	public class Fruit extends Sprite
	{
		private var img:Image;
		private var splash:MovieClip;

		private var id:int;
		public var exploded:Boolean = false;
		public function Fruit(assets:AssetManager)
		{
			super();
			id = MathHelpers.randomIntRange(1,7);
			img = new Image(assets.getTexture("Fruit_0"+id));
			img.pivotX = img.width/2;
			img.pivotY = img.height/2;
			splash = new MovieClip(assets.getTextures("FruitPop_"),12);
			splash.loop = false;
			splash.visible = false;
			splash.pivotX = img.width/2;
			splash.pivotY = img.height/2;
			Starling.juggler.add(splash);
			splash.stop();

			TweenMax.to(img, 1, {y:y-40, yoyo:true, repeat:30});
			addChild(img);
			addChild(splash);
		}

		public function Explode():void
		{
			TweenMax.killTweensOf(img);
			img.visible = false;
			splash.x = img.x-20;
			splash.y = img.y-10;
			splash.visible = true;
			splash.play();
			splash.addEventListener(Event.COMPLETE, SplashComplete);
			exploded = true;
		}

		public function GetBounds():Rectangle
		{
			return new Rectangle(x + img.x,y + img.y,img.width, img.height);
		}

		private function SplashComplete():void
		{
			splash.visible = false;
			trace("Splash finished Playing");
			Destroy();
		}

		public function Destroy():void
		{
			if(!exploded)
				TweenMax.killTweensOf(img);
			Starling.juggler.remove(splash);
		}
	}
}