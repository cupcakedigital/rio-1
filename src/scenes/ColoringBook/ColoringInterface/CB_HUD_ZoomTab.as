package scenes.ColoringBook.ColoringInterface{
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import com.cupcake.DeviceInfo;
	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_HUD_ZoomTab extends Sprite{
		
		//Props.
		private var StateStatus:String;
		
		//Buttons.
		private var ZoomIn:InterfaceButton;		
		private var ZoomOut:InterfaceButton;		
		
		public function CB_HUD_ZoomTab(Core_Assets:External_PNG_Sheet_Manager){
			
			super();
			
			StateStatus = "Out";
			
			ZoomIn  	= new InterfaceButton(Core_Assets.Get_Image("ZH_ZoomIn_Static"),  Core_Assets.Get_Image("ZH_ZoomIn_Glow"));
			ZoomOut 	= new InterfaceButton(Core_Assets.Get_Image("ZH_ZoomOut_Static"), Core_Assets.Get_Image("ZH_ZoomOut_Glow"));	
			
			if(DeviceInfo.isSlow){
				ZoomIn.visible  = false;
				ZoomOut.visible = false;
			}
			
			this.addChild(ZoomIn);
			
		}
		public function Destroy():void{
			
			switch(StateStatus){
				case "Out":{
					this.removeChild(ZoomIn);	
				}break;
				case "In":{
					this.removeChild(ZoomOut);	
				}break;
			}
			
			ZoomIn.Destroy();
			ZoomOut.Destroy();
			
		}
		
		//Inputs.
		public function Down(MousePosition:Point):String{			
			
			if(DeviceInfo.isSlow){return "Nothing";}
			
			if(ZoomIn.Down(MousePosition)){  return "ZoomIn";}
			if(ZoomOut.Down(MousePosition)){ return "ZoomOut";}
			
			return "Nothing";
		}
		public function Up(MousePosition:Point):String{
			
			if(DeviceInfo.isSlow){return "Nothing";}
			
			ZoomIn.Reset();
			ZoomOut.Reset();
			
			if(ZoomIn.Up(MousePosition)){Update_State(); return "ZoomIn";}
			if(ZoomOut.Up(MousePosition)){Update_State();return "ZoomOut";}
			
			return "Nothing";
			
		}		
		
		//States.
		public function Update_State():void{
			
			if(DeviceInfo.isSlow){return;}
			
			switch(StateStatus){
				case "Out":{
					this.removeChild(ZoomIn);	
					this.addChild(ZoomOut);
					StateStatus = "In";
				}break;
				case "In":{
					this.removeChild(ZoomOut);	
					this.addChild(ZoomIn);
					StateStatus = "Out";
				}break;
			}
			
		}	
		
	}
}