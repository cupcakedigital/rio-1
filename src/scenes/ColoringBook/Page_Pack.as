package scenes.ColoringBook{
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Expo;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.AssetManager;
	
	import utils.SB_Button;
	import utils.Starling_TextBox;
	
	public class Page_Pack extends Sprite{	
		
		private var Display_Product_Background:Image;
		private var Display_Product_Frame:Image;
		private var Display_Product_Locked:Image;	
		
		private var Special_Icon:SB_Button;
		
		private var Buy_Pack_Button:SB_Button;
		private var Select_Pack_Button:SB_Button;
		
		private var Pack_Selling_Tag:Starling_TextBox;
		
		//Trigger function.
		private var Trigger:Function;
		
		public function Page_Pack(pAssets:AssetManager, newTrigger:Function, Plate:String, Special:Boolean, Purchased:Boolean, Info:String, Cost:String){
			
			super();
			
			var Purchase_Tag_Name:String
			if(Special){
				Purchase_Tag_Name = "Buy_Deal_BTN";
			}else{Purchase_Tag_Name = "Buy_Reg_BTN";}			
			
			//Frame.
			Display_Product_Frame 			= new Image(pAssets.getTexture("Pack_Frame"));
			Display_Product_Frame.x         = -(Display_Product_Frame.width  / 2);
			Display_Product_Frame.y         = -(Display_Product_Frame.height / 2);
			
			//Back Plate.
			Display_Product_Background 		= new Image(pAssets.getTexture(Plate));
			Display_Product_Background.x    = Display_Product_Frame.x + 14;
			Display_Product_Background.y    = Display_Product_Frame.y + 12;		
			
			//Locked Icon.
			Display_Product_Locked 			= new Image(pAssets.getTexture("Lock_Icon"));
			Display_Product_Locked.x    	= Display_Product_Frame.x + 222;
			Display_Product_Locked.y    	= Display_Product_Frame.y + 270;
			Display_Product_Locked.visible 	= false;
			if(!Purchased){Display_Product_Locked.visible = true;}
			
			//Special Icon.
			Special_Icon 					= new SB_Button(pAssets, "Pack_Special_Icon", "Special", 0xf3a8f2, 0xfbe334, "Tastic", 40, null);			
			Special_Icon.x 					= Display_Product_Frame.x + 20;
			Special_Icon.y 					= Display_Product_Frame.y + 20;
			Special_Icon.rotation 			= ((-25 * Math.PI ) / 180);	
			if(!Special){Special_Icon.visible = false;}
			
			//Buy Pack Here we will have to add the price.
			Buy_Pack_Button 				= new SB_Button(pAssets, Purchase_Tag_Name, ("Purchase\n" + Cost), 0xf3a8f2, 0xffffff, "Tastic", 24, null, 30);			
			Buy_Pack_Button.x 				= Display_Product_Frame.x + 142;
			Buy_Pack_Button.y 				= Display_Product_Frame.y + 445;	
			Buy_Pack_Button.visible 		= false;
			if(!Purchased){Buy_Pack_Button.visible = true;}
			
			//Buy Pack Here we will have to add the price.
			Select_Pack_Button 				= new SB_Button(pAssets, "Selec_Reg_BTN", "Color", 0xf3a8f2, 0xffffff, "Tastic", 30);			
			Select_Pack_Button.x 			= Display_Product_Frame.x + 142;
			Select_Pack_Button.y 			= Display_Product_Frame.y + 445;
			Select_Pack_Button.visible 		= false;
			if(Purchased){Select_Pack_Button.visible = true;}
			
			Pack_Selling_Tag				= new Starling_TextBox(Info, 30, "Tastic", 0x000000, 0xffffff, 20, Display_Product_Frame.width, 600);
			Pack_Selling_Tag.x 				= Display_Product_Frame.x + (Display_Product_Frame.width / 2);
			Pack_Selling_Tag.y 				= (Display_Product_Frame.y + 200);
			
			//Reset trigger.
			Trigger = newTrigger;
			if(Trigger != null){
				if(Buy_Pack_Button.visible){			
					Buy_Pack_Button.addEventListener(starling.events.Event.TRIGGERED, Trigger);
				}else{
					Select_Pack_Button.addEventListener(starling.events.Event.TRIGGERED, Trigger);
				}				
			}
			
			this.addChild(Display_Product_Background);
			this.addChild(Display_Product_Frame);
			this.addChild(Display_Product_Locked);
			this.addChild(Special_Icon);
			this.addChild(Buy_Pack_Button);
			this.addChild(Select_Pack_Button);
			this.addChild(Pack_Selling_Tag);
			
			Special_Up();
			
		}
		public function Dispose():void{
			
			TweenMax.killTweensOf(Special_Icon);
			TweenMax.to(Special_Icon, 0.5, {scaleX:1.2, scaleY:1.2, ease:Expo.easeInOut, onComplete:Special_Down});	
			
			//Reset Trigger if nedded.
			if(Trigger != null){
				if(Buy_Pack_Button.visible){					
					Buy_Pack_Button.removeEventListener(starling.events.Event.TRIGGERED, Trigger);
				}else{
					Select_Pack_Button.removeEventListener(starling.events.Event.TRIGGERED, Trigger);
				}
				Trigger = null;
			}
			
			this.removeChild(Display_Product_Background);
			this.removeChild(Display_Product_Frame);
			this.removeChild(Display_Product_Locked);
			this.removeChild(Special_Icon);
			this.removeChild(Buy_Pack_Button);
			this.removeChild(Select_Pack_Button);
			this.removeChild(Pack_Selling_Tag);			
			
			Pack_Selling_Tag.Dispose();
			Select_Pack_Button.Dispose();
			Buy_Pack_Button.Dispose();
			Special_Icon.Dispose();
			Display_Product_Locked.dispose();
			Display_Product_Background.dispose();
			Display_Product_Frame.dispose();
			
			trace("Properly Disposed");
			
		}
		
		
		
		private function Special_Up():void{			
			TweenMax.killTweensOf(Special_Icon);
			TweenMax.to(Special_Icon, 0.5, {scaleX:1.2, scaleY:1.2, ease:Expo.easeInOut, onComplete:Special_Down});			
		}
		private function Special_Down():void{
			TweenMax.killTweensOf(Special_Icon);
			TweenMax.to(Special_Icon, 0.5, {scaleX:1.0, scaleY:1.0, ease:Expo.easeInOut, onComplete:Special_Up});
		}
		
	}
}