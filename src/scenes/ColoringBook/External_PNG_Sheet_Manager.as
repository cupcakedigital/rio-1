package scenes.ColoringBook
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.XMLLoader;
	import com.greensock.loading.display.ContentDisplay;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.display.StageQuality;
	
	public class External_PNG_Sheet_Manager{		
		
		private var Master_Loader:LoaderMax;
		private var Master_Path:String;
		private var Master_Sheets:Array;
		private var isLoading:Boolean;
		private var Progress:Number;
		
		private var CallBack:Function;
		
		public function External_PNG_Sheet_Manager(pPath:String, pCallBack:Function = null){			
			CallBack			= pCallBack;
			isLoading			= false;
			Progress			= 100;
			Master_Path 		= pPath;
			Master_Sheets 		= new Array();
			Master_Loader 		= new LoaderMax({onComplete:Loading_Completed});		
		}
		public function Update_Path(NewPath:String):void{
			Master_Path = NewPath;
		}
		public function Append_Assets(SheetName:String, SheetType:String = ".png"):void{		
			if(isLoading){return;}
			Append_Image(SheetName, SheetType);
		}
		public function Load_Assets():void{	
			if(isLoading){return;}
			isLoading = true;
			Master_Loader.load(false);			
		}
		private function Loading_Progress(event:LoaderEvent):void {
			//trace(event.target.progress);
			//Progress = event.target.progress;
		}
		private function Loading_Completed(e:LoaderEvent):void{	
			//trace(Loading Completed!);
			isLoading 	= false;
			Progress 	= 100;
			if(CallBack != null){CallBack();}
		}
		public function Purge_Assets():void{		
			
			for(var X:int = 0; X < Master_Sheets.length; X++){				
				
				ContentDisplay(Master_Sheets[X][0].getChildAt(0)).dispose();
				Master_Sheets[X][0] = null;
				Master_Sheets[X][1] = null;
				Master_Sheets[X][2] = null;
				Master_Sheets[X][3].length = 0;
				Master_Sheets.splice(X, 1);
				X--;	
				
				//Master_Sheets[X][0].removeChildren(0, (Master_Sheets[X][0].numChildren - 1));
				//Master_Sheets[X][0] = null;
				//Master_Sheets.splice(X, 1);
				//X--;				
			}
			
			Master_Sheets.length = 0;
		}
		public function Kill_Me():void{
			Master_Loader.dispose(true);
		}
		
		
		//Get an image from sheet.
		public function Image_Exists(iName:String):Boolean{
			for(var X:int = 0; X < Master_Sheets.length; X++){
				for(var S:int = 0; S < Master_Sheets[X][3].length; S++){
					if(Master_Sheets[X][3][S][0] == iName){
						return true;
					}
				}
			}
			
			return false;
		}
		//Get an image from sheet.
		public function Get_Image(iName:String):flash.display.Sprite{
			
			var rSprite:flash.display.Sprite = new flash.display.Sprite();		
			
			for(var X:int = 0; X < Master_Sheets.length; X++){
				for(var S:int = 0; S < Master_Sheets[X][3].length; S++){
					if(Master_Sheets[X][3][S][0] == iName){
						//trace("found the image");
						var Rec:Rectangle 	= Master_Sheets[X][3][S][1];
						var rBitmap:Bitmap 	= new Bitmap();
						rBitmap.bitmapData 	= new BitmapData(Rec.width, Rec.height, true, 0xFFFFFF);						
						var iMatrix:Matrix 	= new Matrix();						
						iMatrix.translate(-Rec.x, -Rec.y);
						//iMatrix.scale(Costanza.SCALE_FACTOR, Costanza.SCALE_FACTOR);
						//rBitmap.bitmapData.draw(Master_Sheets[X][0], iMatrix);
						rBitmap.bitmapData.drawWithQuality(Master_Sheets[X][0], iMatrix, null, null, null, true, StageQuality.BEST);	
						rBitmap.smoothing = true;
						rSprite.addChild(rBitmap);
						rSprite.x = 0;
						rSprite.y = 0;
						return rSprite;
					}
					
				}				
				
			}
			
			//Returns Empty.
			return rSprite;						
			
		}		
		
		//Processing Core Image Loading.
		private function Append_Image(Image_Name:String, File_Type:String = ".png"):void{
			
			var Container_Info:Array = new Array();			
			
			var Container:flash.display.Sprite = new flash.display.Sprite();
			var ImageName:String = Image_Name;
			var FileType:String  = File_Type;				
			
			Master_Loader.append( new ImageLoader( Master_Path + (ImageName + FileType), { container:Container, autoDispose:true } ) );			
			
			Container_Info.push(Container);
			Container_Info.push(ImageName);
			Container_Info.push(FileType);
			
			Master_Sheets.push(Container_Info);
			
			//now to check if we even have an XML.
			Load_Art_XML(Master_Path, ImageName);
			
		}		
		private function Load_Art_XML(Path:String, Name:String):void{
			var loader:XMLLoader = new XMLLoader((Path + Name + ".xml"), {name:Name, onComplete:XML_Loaded, autoDispose:true});
			loader.load();			
		}
		private function XML_Loaded(e:LoaderEvent):void{
			
			var FRS:String 					= (e.target.toString());
			//trace(FRS);
			var XN:String  					= FRS.substr((FRS.indexOf("'") + 1), FRS.length);
			//trace(XN);
			var FXN:String 					= XN.substr(0, XN.indexOf("'"));
			//trace(FXN);			
			var Sheet_XML:XML   			= LoaderMax.getContent(FXN);
			var FullSheetString:String      = Sheet_XML.toString();
			
			//trace("there is a total of " + Sheet_XML.SubTexture.length() + " Elements");
			var Elements_Array:Array 		= new Array();
			var Elements_String:String 		= FullSheetString.substr((FullSheetString.indexOf(">", Element_String_Start) + 1), FullSheetString.length) + "_END";
			var Element_String_Start:int 	= 0;
			var Element_String_End:int   	= 0;
			
			for(var X:int = 0; X < 1; X++){
				
				//End Location substr.
				//End Location substr.
				Element_String_End   = Elements_String.indexOf(">", Element_String_Start);				
				if(Element_String_End == -1){X = 1; continue;}
				else{Element_String_End++;}			
				
				//Parce Properties of Image Data.				
				var Element_Parce:String = Elements_String.substr(Element_String_Start, (Element_String_End - Element_String_Start));
				var Splicer_S:int = 0;
				var Splicer_E:int = 0;	
				
				//trace(Element_Parce);
				if(Element_Parce == "</TextureAtlas>"){X = 1; continue;}	
				
				//Actual parced info storing.
				var Parced_Data:Array 	= new Array();
				var iName:String 		= "";
				var Rec:Rectangle 		= new Rectangle(0, 0, 0, 0);
				
				//Now parce all the info.
				Splicer_S 	= (Element_Parce.indexOf("name=") + 6);
				Splicer_E 	= (Element_Parce.indexOf("x=", Splicer_S) - 2);				
				iName 		= Element_Parce.substr(Splicer_S, (Splicer_E - Splicer_S));
				
				Splicer_S 	= (Element_Parce.indexOf("x=") + 3);
				Splicer_E 	= (Element_Parce.indexOf("y=", Splicer_S) - 2);				
				Rec.x 		= int(Element_Parce.substr(Splicer_S, (Splicer_E - Splicer_S)));
				
				Splicer_S 	= (Element_Parce.indexOf("y=") + 3);
				Splicer_E 	= (Element_Parce.indexOf("width=", Splicer_S) - 2);				
				Rec.y 		= int(Element_Parce.substr(Splicer_S, (Splicer_E - Splicer_S)));
				
				Splicer_S 	= (Element_Parce.indexOf("width=") + 7);
				Splicer_E 	= (Element_Parce.indexOf("height=", Splicer_S) - 2);				
				Rec.width 	= int(Element_Parce.substr(Splicer_S, (Splicer_E - Splicer_S)));
				
				Splicer_S 	= (Element_Parce.indexOf("height=") + 8);
				Splicer_E 	= (Element_Parce.indexOf(String.fromCharCode(34), Splicer_S));				
				Rec.height 	= int(Element_Parce.substr(Splicer_S, (Splicer_E - Splicer_S)));
												
				//populate.
				Parced_Data.push(iName);
				Parced_Data.push(Rec);				
				Elements_Array.push(Parced_Data);					
				
				//Start Location substr.
				Element_String_Start = Elements_String.indexOf("<", Element_String_End);
				if(Element_String_Start == -1){X = 1; continue;}
				else{X--;}
			}
			
			for(var cS:int = 0; cS < Master_Sheets.length; cS++){				
				if(Master_Sheets[cS][1] == FXN){
					//trace("found are Home");
					Master_Sheets[cS].push(Elements_Array);
					cS = Master_Sheets.length;
					continue;
				}				
			}
			
		}
		
	}
}