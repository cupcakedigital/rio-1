package scenes
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Bounce;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.system.Capabilities;
	import flash.system.System;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.SoundManager;
	import starling.filters.BlurFilter;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	import ui.Help;
	import ui.HighScore;
	import ui.Settings;
	
	import utils.ForParentsScreen;
	import utils.MoreApps;

	public class Menu extends starling.display.Sprite
	{
		private static var assets:AssetManager;
		public  static var currentAssetsProxy:AssetManager;
		private 	   var appDir:File;
		private 	   var assetsLoaded:Boolean;
		
		private var soundManager:SoundManager;

		private var settingsPopUp:Settings;

		private var bg:Image;
		private var logo:Image;

		//buttons
		private var storyEnter:Button;
		private var gameEnter:Button;
		private var game2Enter:Button;
		private var coloringEnter:Button;
		private var highscoreEnter:Button;
		private var Parents:Button;
		private var Options:Button;
		private var Apps:Button;
		
		private var appsTxt:Image;
		private var optionsTxt:Image;
		private var parentsTxt:Image;

		//highscores
		private var highScore:HighScore;

		private var canTap:Boolean = true;
		
		private var appsSparkle:PDParticleSystem;	
		private var appBtnSprite:flash.display.Sprite;
		private var appImageDisplay:Button;
		private var loader:LoaderMax;
		private var ldr:ImageLoader;

		public static const LOAD_ROOM:String = "loadRoom";
		public static const LOAD_COLOR:String = "loadColor";

		public function Menu(num:int)
		{
			assetsLoaded 			= false;
			appDir 					= File.applicationDirectory;			
			
			assets 					= new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps 		= Costanza.mipmapsEnabled;
			Root.currentAssetsProxy = assets;
			assets.verbose 			= Capabilities.isDebugger;
			
			assets.enqueue(
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/menu"),
				appDir.resolvePath("audio/menu"),
				appDir.resolvePath("particles/MoreAppsSparkleTexture.png"),
				appDir.resolvePath("particles/MoreAppsSparkle.pex")
			);
			
			assets.loadQueue( onProgress );
		}
		
		private function onProgress(ratio:Number):void
		{				
			if(ratio == 1){
				Starling.juggler.delayCall(function():void{	
					assetsLoaded = true;							
					init();
				}, 0.15);
				System.pauseForGCIfCollectionImminent(0);
				System.gc();	
			};
		}
		
		public function MoreAppsLoaded():void
		{
			if ( MoreApps.moreAppsLoaded && MoreApps.apps.length > 0 )
			{
				appsTxt.visible		= true;
				appsTxt.alpha		= 0;
				Apps.visible		= true;
				Apps.alpha			= 0;
				Apps.touchable		= true;
				
				parentsTxt.visible	= true;
				parentsTxt.alpha	= 0;
				Parents.visible		= true;
				Parents.alpha		= 0;
				Parents.touchable	= true;
				
				TweenMax.allTo( [ appsTxt, Apps, parentsTxt, Parents ], 1, { alpha: 1 } );
				TweenMax.delayedCall(3,loadSparklyAppButton);
			}
		}
		
		private function loadSparklyAppButton():void
		{
			loader = new LoaderMax( { onComplete:showSparklyAppButton } );
			ldr = new ImageLoader("app-storage:/" + MoreApps.apps[0].altFile );
			trace("app-storage:/" + MoreApps.apps[0].altFile);
			loader.append( ldr ); 
			loader.load();
		}
		
		private function showSparklyAppButton(e:LoaderEvent):void
		{
			var bmd:Bitmap = ldr.rawContent;
			
			appImageDisplay			= new Button(Texture.fromBitmap(bmd));
			appImageDisplay.filter	= BlurFilter.createDropShadow();
			appImageDisplay.name	= "TossOut";
			appImageDisplay.x		= 540 + Costanza.STAGE_OFFSET;
			appImageDisplay.y		= 768;
			appImageDisplay.addEventListener(starling.events.Event.TRIGGERED, TapApps);
			this.addChildAt(appImageDisplay,this.getChildIndex(Apps)+1);
			TweenMax.to(appImageDisplay,.8,{y:640,delay:0.6,ease:Bounce.easeOut, onComplete:showSparkles});
			
			loader.dispose(true);
			loader = null;
			
			function showSparkles():void
			{
				appsSparkle.emitterX = appImageDisplay.x + appImageDisplay.width/2; 
				appsSparkle.emitterY = appImageDisplay.y + appImageDisplay.height; 
				appsSparkle.start();
				appsSparkle.parent.setChildIndex(appsSparkle,getChildIndex(appImageDisplay)+1);
			}
		}
		
		private function init():void
		{
			soundManager = SoundManager.getInstance();
			soundManager.addSound("apps", assets.getSound("apps button"));
			soundManager.addSound("blue", assets.getSound("blues's rescue button"));
			soundManager.addSound("jewel", assets.getSound("Jewel's Gems button"));
			soundManager.addSound("paint", assets.getSound("main menu paint button"));
			soundManager.addSound("settings", assets.getSound("options button"));
			soundManager.addSound("parents", assets.getSound("parents button"));
			soundManager.addSound("story", assets.getSound("story button"));
			soundManager.addSound("Menu", assets.getSound("Menu"));
			soundManager.addSound("forwardButton", assets.getSound("forwardButton"));
			soundManager.addSound("backwardButton", assets.getSound("backwardButton"));
			soundManager.addSound("star1", assets.getSound("star1"));
			soundManager.addSound("star2", assets.getSound("star2"));
			soundManager.addSound("star3", assets.getSound("star3"));

			soundManager.playSound("Menu", Costanza.musicVolume,999);

			addEventListener(Settings.HIDE_SETTINGS,  HideSettings);

			MoreApps.Setup( MoreAppsLoaded, "en");

			// Instance soundmanager and add objects
			soundManager = SoundManager.getInstance();

			bg = new Image(assets.getTexture("BG"));

			Apps = new Button(assets.getTexture("Apps"));
			Apps.alignPivot();
			Apps.x = 725 + Costanza.STAGE_OFFSET;
			Apps.y = 688;
			Apps.addEventListener(starling.events.Event.TRIGGERED, TapApps);
			Apps.visible = false;
			Apps.touchable = false;

			Options = new Button(assets.getTexture("Settings"));
			Options.alignPivot();
			Options.x = 1100 + Costanza.STAGE_OFFSET;
			Options.y = 688;
			Options.addEventListener(starling.events.Event.TRIGGERED, TapOptions);

			Parents = new Button(assets.getTexture("Parents"));
			Parents.alignPivot();
			Parents.x = 900 + Costanza.STAGE_OFFSET;
			Parents.y = 688;
			Parents.addEventListener(starling.events.Event.TRIGGERED, TapParents);
			Parents.visible = false;
			Parents.touchable = false;
			
			appsTxt = new Image(assets.getTexture("Apps_Text"));
			appsTxt.alignPivot();
			appsTxt.x = Apps.x;
			appsTxt.y = Apps.y + Apps.height/1.3;
			appsTxt.visible = false;
			appsTxt.touchable = false;
			
			optionsTxt = new Image(assets.getTexture("Settings_Text"));
			optionsTxt.alignPivot();
			optionsTxt.x = Options.x;
			optionsTxt.y = Options.y + Options.height/1.3;
			optionsTxt.touchable = false;
			
			parentsTxt = new Image(assets.getTexture("Parents_Text"));
			parentsTxt.alignPivot();
			parentsTxt.x = Parents.x;
			parentsTxt.y = Parents.y + Parents.height/1.3;
			parentsTxt.visible = false;
			parentsTxt.touchable = false;

			settingsPopUp = new Settings(assets);
			settingsPopUp.x = Costanza.STAGE_WIDTH/2 - settingsPopUp.width/2;
			settingsPopUp.y = Costanza.STAGE_HEIGHT/2 - settingsPopUp.height/2;
			settingsPopUp.visible = false;
			settingsPopUp.touchable = false;

			storyEnter = new Button(assets.getTexture("Story"), "", assets.getTexture("Story_Over"));
			storyEnter.x = 550 + Costanza.STAGE_OFFSET;
			storyEnter.y = 70;
			storyEnter.addEventListener(starling.events.Event.TRIGGERED, EnterStory);

			highscoreEnter = new Button(Root.assets.getTexture("Board"), "", Root.assets.getTexture("Board_Over"));
			highscoreEnter.x = Costanza.SCREEN_START_X + Costanza.STAGE_OFFSET;
			highscoreEnter.y = Costanza.STAGE_HEIGHT - highscoreEnter.height;
			highscoreEnter.addEventListener(starling.events.Event.TRIGGERED, EnterHighScores);

			//text for button
			var storyText:Image = new Image(assets.getTexture("Story_Text"));
			storyText.x = 600 + Costanza.STAGE_OFFSET;
			storyText.y = 310;
			storyText.touchable = false;

			gameEnter = new Button(assets.getTexture("BlueRescue"),"",assets.getTexture("BlueRescue_Over"));
			gameEnter.x = 650 + Costanza.STAGE_OFFSET;
			gameEnter.y = 400;
			gameEnter.addEventListener(starling.events.Event.TRIGGERED, EnterSideScroller);

			//text for button
			var gameText:Image = new Image(assets.getTexture("Blue_Text"));
			gameText.x = 675 + Costanza.STAGE_OFFSET;
			gameText.y = 535;
			gameText.touchable = false;

			game2Enter = new Button(assets.getTexture("JewelsGems"),"",assets.getTexture("JewelsGems_Over"));
			game2Enter.x = 880 + Costanza.STAGE_OFFSET;
			game2Enter.y = 400;
			game2Enter.addEventListener(starling.events.Event.TRIGGERED, EnterJewel);

			//text for button
			var game2Text:Image = new Image(assets.getTexture("Jewel_Text"));
			game2Text.x = 895 + Costanza.STAGE_OFFSET;
			game2Text.y = 535;
			game2Text.touchable = false;

			coloringEnter = new Button(assets.getTexture("Paint"),"",assets.getTexture("Paint_Over"));
			coloringEnter.x = 880 + Costanza.STAGE_OFFSET;
			coloringEnter.y = 70;
			coloringEnter.addEventListener(starling.events.Event.TRIGGERED, EnterColoring);

			//text for button
			var coloringText:Image = new Image(assets.getTexture("Paint_Text"));
			coloringText.x = 940 + Costanza.STAGE_OFFSET;
			coloringText.y = 310;
			coloringText.touchable = false;

			addChild(bg);
			addChild(storyEnter);
			addChild(gameEnter);
			addChild(game2Enter);
			addChild(coloringEnter);
			addChild(Parents);
			addChild(Apps);
			addChild(Options);
			addChild(storyEnter);
			addChild(highscoreEnter);
			addChild(gameText);
			addChild(game2Text);
			addChild(coloringText);
			addChild(storyText);
			addChild(settingsPopUp);
			
			addChild(appsTxt);
			addChild(optionsTxt);
			addChild(parentsTxt);
			
			appsSparkle = new PDParticleSystem(assets.getXml("MoreAppsSparkle"), assets.getTexture("MoreAppsSparkleTexture"));
			this.addChild(appsSparkle);
			Starling.juggler.add(appsSparkle);

			this.addEventListener(Root.DISPOSE,  disposeScene);	
			Starling.current.nativeStage.addEventListener(flash.events.Event.ACTIVATE, FocusGained);
			
			dispatchEventWith( Root.SCENE_LOADED, true );
		}
		
		private function disposeScene():void
		{
			trace( "DISPOSING MENU" );
			this.removeEventListener( Settings.HIDE_SETTINGS, HideSettings );
			this.removeEventListener( Root.DISPOSE, disposeScene );
			Starling.current.nativeStage.removeEventListener(flash.events.Event.ACTIVATE, FocusGained);
			
			//Destroy assets.
			assets.purge();
			assets.dispose();
		}

		private function EnterStory(e:starling.events.Event):void
		{
			trace( "Menu.EnterStory()" );
			if( canTap )
			{
				canTap = false;
				trace("Entering Story");
				soundManager.tweenVolume("Menu",0,0.5);
				soundManager.playSound("story",Costanza.soundVolume);
				TweenMax.delayedCall(0.3,dispatchEventWith,[LOAD_ROOM, true,[1]]);
			}
		}

		private function EnterHighScores(e:starling.events.Event):void
		{
			trace( "Menu.EnterHighScores()" );
			if( canTap )
			{
				trace("Enter High Scores");
				soundManager.tweenVolume("Menu",0,0.5);
				soundManager.playSound("story",Costanza.soundVolume);
				highScore = new HighScore("BlusRescueEasy",0,true);
				this.addChild(highScore);
			}
		}

		private function EnterColoring(e:starling.events.Event):void
		{
			trace("EnterColoring: CanTap = " + canTap.toString());
			if( canTap )
			{
				canTap = false;
				trace("Enter Coloring");
				soundManager.playSound("paint",Costanza.soundVolume);
				TweenMax.delayedCall(0.3,dispatchEventWith,[LOAD_COLOR, true]);
			}
		}

		private function EnterSideScroller(e:starling.events.Event):void
		{
			trace("EnterSideScroller: CanTap = " + canTap.toString());
			if( canTap )
			{
				canTap = false;
				trace("Enter Side Scroller");
				soundManager.tweenVolume("Menu",0,0.5);
				soundManager.playSound("blue",Costanza.soundVolume);
				TweenMax.delayedCall(0.3,dispatchEventWith,[LOAD_ROOM, true]);
			}
		}

		private function EnterJewel(e:starling.events.Event):void
		{
			trace("EnterJewel: CanTap = " + canTap.toString());
			if( canTap )
			{
				canTap = false;
				trace("Enter Coloring");
				soundManager.tweenVolume("Menu",0,0.5);
				soundManager.playSound("jewel",Costanza.soundVolume);
				TweenMax.delayedCall(0.3,dispatchEventWith,[LOAD_ROOM, true,[2]]);
			}
		}

		private function FocusGained(e:flash.events.Event):void
		{
			//starling stage restarts when focus is lost and gained.
			//check to see if stage isnt visible, meaning for parents or more apps is visible.
			//If they are, stop the stage again manually, prevents starling buttons from working underneath more apps
			if(!Starling.current.stage3D.visible) { Starling.current.stop(); }
		}

		private function TapApps(e:starling.events.Event):void
		{
			if ( canTap )
			{
				canTap = false;
				Starling.current.stop();
				var moreAppsHolder:flash.display.Sprite = new flash.display.Sprite();
				moreAppsHolder.x = 171 + Costanza.STAGE_OFFSET;
				Starling.current.nativeOverlay.addChild(moreAppsHolder);
				MoreApps.Show(moreAppsHolder);
				soundManager.playSound("apps",Costanza.soundVolume);
				TweenMax.delayedCall(2, function resetTap():void { canTap = true; });
			}
		}

		private function TapOptions(e:starling.events.Event):void
		{
			if ( canTap )
			{
				soundManager.playSound("settings",Costanza.soundVolume);
				HideSettings();
			}
		}

		private function TapParents(e:starling.events.Event):void
		{
			if ( canTap )
			{
				canTap = false;
				soundManager.playSound("parents",Costanza.soundVolume);
				Starling.current.stop();
				var forParentsHolder:flash.display.Sprite = new flash.display.Sprite();
				forParentsHolder.x = 171 + Costanza.STAGE_OFFSET;
				Starling.current.nativeOverlay.addChild(forParentsHolder);
				ForParentsScreen.Show(forParentsHolder,this);
				TweenMax.delayedCall(2, function resetTap():void { canTap = true; });
			}
		}

		public function showHelpPage():void
		{
			var hlp:Help = new Help(assets.getTexture("Credits"), assets.getTexture("Help"));
			addChild(hlp);
		}

		public function enableButtons():void
		{
			// trace("Menu.enableButtons() called");
			this.parent.setChildIndex(this, this.numChildren-1);
			this.touchable = true;
		}
		
		private function HideSettings():void
		{
			settingsPopUp.visible = !settingsPopUp.visible;
			settingsPopUp.touchable = !settingsPopUp.touchable;
			Apps.touchable = Options.touchable = Parents.touchable = storyEnter.touchable = game2Enter.touchable = gameEnter.touchable = coloringEnter.touchable = !settingsPopUp.visible;
			
			soundManager.setVolume("Menu",Costanza.musicVolume);
		}
	}
}