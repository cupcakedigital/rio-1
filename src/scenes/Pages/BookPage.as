﻿package scenes.Pages
{
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.utils.getTimer;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.utils.AssetManager;
	
	public class BookPage extends Sprite
	{
		private static const TEXT_COLOR:uint		= 0x000000;
		private static const TEXT_HEIGHT:int		= 120;
		private static const TEXT_HIGHLIGHT:uint	= 0x0C20FF;
		private static const TEXT_LEFT:int			= 315;
		private static const TEXT_SPACE:int			= 30;
		private static const TEXT_TOP_DOUBLE:int	= 50;
		private static const TEXT_TOP_MULTI:int		= 40;
		private static const TEXT_TOP_SINGLE:int	= 60;
		private static const TEXT_WIDTH:int			= 765;
		
		public var currentPage:int					= 0;
		
		// Scene Objects
		private var appDir:File						= File.applicationDirectory;
		private var assets:AssetManager;
		private var background:Image;
		private var bg:Image;
		private var narrationChannel:SoundChannel 	= new SoundChannel;
		private var narrationImage:Image;
		private var oldBackground:Image;
		private var readerBox:Sprite;
		private var sceneRoot:Sprite				= new Sprite;
		
		private var cueVector:Vector.<int>;
		private var lines:Vector.<Sprite>;
		private var wordBoxes:Vector.<TextField>	= new <TextField>[];
		private var words:Vector.<String>;
		
		private var cuePointPausePoint:int = 0;
		private var cueTimer:int 					= 0;
		private var currentTime:int 				= 0;
		private var elapsedTime:int 				= 0;
		private var narrationDone:Boolean			= true;
		private var narrationPausePoint:int			= 0;
		private var narrationStarted:Boolean		= false;
		private var oldBGTexture:String				= "";
		private var pageLoaded:Boolean				= false;
		private var paused:Boolean					= false;
		private var previousTime:int 				= 0;
		private var transitionForward:Boolean		= true;
		
		public function BookPage(readerBar:Image)
		{
			addChild( sceneRoot );
			
			assets 					= new AssetManager(Costanza.SCALE_FACTOR,Costanza.mipmapsEnabled);
			assets.useMipMaps		= Costanza.mipmapsEnabled;
			Root.currentAssetsProxy	= assets;
			assets.verbose			= Capabilities.isDebugger;
			
			//load up the first page and the navobjects and music we are going to be using
			assets.enqueue( appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Pages/Page0") );
			narrationImage = readerBar;
			
			//load the individual words for the currently selected page (in this case we just started so 1)
			LoadWords();
			
			assets.loadQueue( OnInitialProgress );
		}
		
		private function OnInitialProgress(ratio:Number):void
		{
			if (ratio == 1)
			{
				Starling.juggler.delayCall(function():void
				{
					// now would be a good time for a clean-up 
					System.pauseForGCIfCollectionImminent(0);
					System.gc();
				}, 0.15);
				BuildInitialScene();
			}
		}
		
		private function BuildInitialScene():void
		{
			oldBackground = new Image(assets.getTexture("Scene0"));
			sceneRoot.addChildAt(oldBackground, 0);
			background = new Image(assets.getTexture("Scene0"));
			sceneRoot.addChildAt(background, 1);
			
			dispatchEventWith(Root.SCENE_LOADED,true);
			
			AddReaderBox();
			
			PositionWords();
			FadeInOutText(true);
			
			Resume(true);
			
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE, FocusGained);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE, FocusLost);
		}
		
		private function AddReaderBox():void
		{
			readerBox = new Sprite();
			
			sceneRoot.addChildAt(readerBox, 2);
			
			narrationImage.x = -Costanza.STAGE_OFFSET;
			
			readerBox.addChild(narrationImage);
			readerBox.x = Costanza.STAGE_OFFSET;
			readerBox.y = Costanza.STAGE_HEIGHT - readerBox.height;
			
			var cuesArray:Array = PageManager.NarrationXML.Page[currentPage].CuePoints.split(", ");
			cueVector = new <int>[];
			
			for (var j:int = 0; j < cuesArray.length; j++)
			{
				cueVector.push(int(cuesArray[j]));
			}
			
			var text:TextField;
			for(var x:int = 0; x < words.length; x++)
			{
				text = null;
				text = new TextField(1,1,words[x], PageManager.NarrationXML.@FontClass, PageManager.NarrationXML.@FontSize);
				text.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
				text.addEventListener(TouchEvent.TOUCH, PressWord);
				wordBoxes.push(text);
			}
		}
		
		private function EmptyReaderBox():void
		{
			readerBox.removeChildren(0, readerBox.numChildren - 1);
			wordBoxes.length = 0;
		}
		
		private function FocusLost(e:flash.events.Event):void
		{
			trace("BookPage.FocusLost()");
			if(!narrationDone && !paused && narrationChannel != null && narrationStarted)
			{
				trace( "Pausing narration" );	
				narrationPausePoint = narrationChannel.position;
				cuePointPausePoint = cueTimer;
				narrationChannel.stop();
				narrationChannel = null;
				paused = true;
			}
		}
		
		private function FocusGained(e:flash.events.Event):void
		{
			if(paused == true)
			{
				paused = false;
				
				trace("Gain Focus");
				
				currentTime = flash.utils.getTimer();
				previousTime = flash.utils.getTimer();
				
				cueTimer = cuePointPausePoint;
				
				if ( narrationChannel != null ) { narrationChannel.stop(); narrationChannel = null; }
				var soundTrans:SoundTransform = new SoundTransform(Costanza.voiceVolume);
				narrationChannel = PageManager.NarrationSound.play(narrationPausePoint,0,soundTrans);
			}
		}
		
		private function PositionWords():void
		{
			lines = new Vector.<Sprite>;
			var currentLine:Sprite = new Sprite;
			lines.push( currentLine );
			readerBox.addChild( currentLine );
			
			for(var x:int = 0; x < wordBoxes.length; x++)
			{
				if ( currentLine.width + wordBoxes[ x ].width > TEXT_WIDTH )
				{
					currentLine = new Sprite;
					lines.push( currentLine );
					readerBox.addChild( currentLine );
				}
				
				wordBoxes[ x ].x = currentLine.width;
				currentLine.addChild( wordBoxes[ x ] );
			}
			
			switch( lines.length )
			{
				case 0: trace( "Something has gone horribly wrong, this page has no lines of text?!" ); break;
				case 1: lines[ 0 ].y = TEXT_TOP_SINGLE; break;
				case 2: lines[ 0 ].y = TEXT_TOP_DOUBLE; lines[ 1 ].y = lines[ 0 ].y + TEXT_SPACE; break;
				
				default:
					for ( var t:int = 0 ; t < lines.length ; t++ ) { lines[ t ].y = TEXT_TOP_MULTI + ( t * TEXT_SPACE ); }
					break;
			}
			
			for each ( var line:Sprite in lines ) { line.x = TEXT_LEFT + int( ( TEXT_WIDTH - line.width ) / 2 ); }
		}
		
		public function PressWord(e:TouchEvent):void
		{
			if( narrationDone || PageManager.muted )
			{
				var touch:Touch = e.getTouch(stage);
				if(touch == null){return;}
				if(touch.phase == TouchPhase.BEGAN)
				{
					var text:TextField = e.currentTarget as TextField;
					for(var x:int = 0; x < words.length; x++)
					{
						if(text.text == words[x])
						{
							var WordSound:Sound = assets.getSound( FilterWord( words[x] ));
							WordSound.play(0,0,new SoundTransform(Costanza.voiceVolume));
							text.color = TEXT_HIGHLIGHT;
							TweenMax.delayedCall(0.5, FadeText, [text]);
							break;
						}
					}
				}
			}
		}
		
		public function FadeText(text:TextField):void { text.color = TEXT_COLOR; }
		
		public function CuePointTimer(e:starling.events.Event):void
		{
			if( !PageManager.muted && !narrationDone && !paused )
			{
				previousTime	= currentTime;
				currentTime		= flash.utils.getTimer();
				elapsedTime		= currentTime - previousTime;
				cueTimer		+= elapsedTime;
				
				for(var x:int = 0; x < cueVector.length; x++)
				{
					if(cueTimer > cueVector[x])
					{
						if(wordBoxes.length > x) { wordBoxes[x].color = TEXT_HIGHLIGHT; }
						if(x > 0) { wordBoxes[x-1].color = TEXT_COLOR; }
					}
				}
				
				if(cueTimer >= cueVector[cueVector.length-1])
				{
					trace("Stop Sound");
					StopSound();
					narrationDone = true;
				}
			}
		}
		
		public function InitializeTimer():void
		{
			cueTimer		+= cueVector[0];
			currentTime		= flash.utils.getTimer();
			previousTime	= flash.utils.getTimer();
		}
		
		public function LoadWords():void
		{
			var wordsArray:Array	= PageManager.NarrationXML.Page[currentPage].PageText.split(" ");
			words					= new <String>[];
			
			for (var i:int = 0; i < wordsArray.length; i++)
			{
				words.push(wordsArray[i] + " ");
				assets.enqueue(
					appDir.resolvePath("audio/IndividualWords/" + FilterWord( words[i] ) + ".mp3")
				);
				//trace("Word: " + wordsArray[i]);
			}
		}
		
		private function UnloadWords():void
		{
			if ( assets != null )
			{
				for ( var i:int = 0 ; i < words.length ; i++ )
					{ assets.removeSound( FilterWord( words[ i ] ) ); }
			}
		}
		
		public function FilterWord( word:String ):String
			{ return word.split(" ").join("").split(".").join("").split(",").join("").split("!").join("").split("\"").join("").split(":").join("").split("-").join("").split(";").join("").split("?").join("").toUpperCase(); } 
		
		public function TurnPage( nextPage:Boolean = true ):void
		{
			Stop();
			FadeInOutText();
			UnloadWords();
			
			narrationDone		= true;
			narrationStarted	= false;
			pageLoaded			= false;
			transitionForward	= nextPage;
			
			//advance our page
			if ( nextPage )
			{
				oldBGTexture = "Scene" + ( currentPage - 1 );
				if (currentPage - 1 < 0 ){ oldBGTexture = "Scene0"; }
				currentPage++;
			}
			else
			{
				oldBGTexture = "Scene" + currentPage;
				if( currentPage > PageManager.MAX_PAGE ){ oldBGTexture = "Scene" + PageManager.MAX_PAGE; }
				currentPage--;
			}
			
			//saves last page read to memory
			Costanza.lastPageRead = currentPage;
			Rio.savedVariablesObject.data.lastPageRead = Costanza.lastPageRead;
			Rio.savedVariablesObject.flush();
			
			sceneRoot.removeChild(oldBackground);
			oldBackground.dispose();
			oldBackground = new Image(background.texture);
			sceneRoot.addChildAt(oldBackground,0);
			trace("currentPage " + currentPage);
			assets.enqueue( appDir.resolvePath( "textures/" + Costanza.SCALE_FACTOR + "x/Pages/Page" + currentPage ) );
			LoadWords();
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1)
				{
					Starling.juggler.delayCall(function():void
					{
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
					Transition();
				}
			});
		}
		
		private function Transition():void
		{
			trace("BookPage.Transition()");
			sceneRoot.removeChild(background);
			background.dispose();
			background = new Image(assets.getTexture("Scene" + currentPage));
			sceneRoot.addChildAt(background,1);
			background.x = transitionForward ? 1500 : -background.width;
			TweenMax.to(background, 1.5, {x:0, onComplete:RemoveOldTexture});
			if ( !transitionForward ) { TweenMax.to(readerBox, 0.5, {alpha: 1}); }
			EmptyReaderBox();
			AddReaderBox();
			PositionWords();
			Resume();
			FadeInOutText(true);
			dispatchEventWith(PageManager.TRANSITION_FINISHED, true);
		}
		
		private function FadeInOutText(fadeIn:Boolean = false):void
		{
			if(fadeIn)
			{
				for each ( var inLine:Sprite in lines )
					{ TweenMax.delayedCall( 2, TweenMax.to, [ inLine, 0.5, { alpha: 1 } ] ); }
			}
			else
			{
				for each ( var outLine:Sprite in lines )
					{ TweenMax.to( outLine, 0.5, { alpha: 0 } ); }
			}
		}
		
		public function Resume( startPage:Boolean = false ):void
		{
			trace( "Resume" );
			cueTimer		= 0;
			narrationDone	= false;
			paused			= false;
			
			TweenMax.delayedCall( ( startPage ? 4 : 3 ), BeginNarration);
		}
		
		public function Stop():void
		{
			for(var x:int = 0; x < words.length; x++)
				{ wordBoxes[x].color = TEXT_COLOR; }
			
			StopSound();
			removeEventListener(starling.events.Event.ENTER_FRAME, CuePointTimer);
		}
		
		public function Destroy():void
		{
			TweenMax.killAll();
			UnloadWords();
			if(assets != null) { assets.dispose(); }
			
			removeEventListener(starling.events.Event.ENTER_FRAME, CuePointTimer);
			//Starling.juggler.purge();
			
			StopSound();
			
			for(var j:int = 0; j < wordBoxes.length - 1; j++)
				{ wordBoxes[j].dispose(); }
			
			words.length = 0;
			
			readerBox.dispose();
			
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.ACTIVATE, FocusGained);
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.DEACTIVATE, FocusLost);
			
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
		}
		
		public function BeginNarration():void
		{
			pageLoaded = true;
			if(!PageManager.muted)
			{
				narrationStarted = true;
				InitializeTimer();
				
				addEventListener(starling.events.Event.ENTER_FRAME, CuePointTimer);
				trace("Begin Narration");
				narrationChannel = PageManager.NarrationSound.play(cueVector[0],0,new SoundTransform(Costanza.voiceVolume));
			}
		}
		
		public function MuteNarration(mute:Boolean):void
		{
			if(mute)
			{
				StopSound();
				narrationDone = true;
				for(var x:int = 0; x < words.length; x++) 
					{ wordBoxes[x].color = TEXT_COLOR; }
			}
			else if ( pageLoaded )
			{
				narrationDone = false;
				cueTimer = 0;
				BeginNarration();
			}
		}
		
		private function RemoveOldTexture():void
		{
			trace(oldBGTexture);
			assets.removeTexture(oldBGTexture,true);
		}
		
		private function StopSound():void 
		{
			if ( narrationChannel != null )
			{
				narrationChannel.stop();
				narrationChannel = null;
			}
		}
	}
}
