﻿package scenes.Pages
{
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.utils.getTimer;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.utils.AssetManager;

	public class BookPage extends Sprite
	{
		// Scene Objects
		public var sceneRoot:Sprite;
		public var bg:Image;

		public var Reader_BOX:Sprite;

		public var CueVector:Vector.<int>;
		public var WordBoxes:Vector.<TextField> = new <TextField>[];
		public var NarrationSounds:Vector.<Sound> = new Vector.<Sound>();

		public var buttonPressed:Boolean = false;

		// Standard display
		public var stageWidth:int;
		public var stageHeight:int;

		public var wordsArray:Array;

		public var Text:TextField;

		public var currentPage:int;

		public var NarrationChannel:SoundChannel = new SoundChannel();

		public var words:Vector.<String>;

		public var PreviousTime:int = 0;
		public var CurrentTime:int = 0;
		public var ElapsedTime:int = 0;
		public var CueTimer:int = 0;

		public var assets:AssetManager;

		public var appDir:File = File.applicationDirectory;

		//backgrounds
		private var background:Image;
		private var oldBackground:Image;

		//lines
		private var LineOne:Sprite = new Sprite();
		private var LineTwo:Sprite = new Sprite();
		private var LineThree:Sprite = new Sprite();
		private var LineFour:Sprite = new Sprite();
		
		private var Line1Done:Boolean = false;
		private var Line2Done:Boolean = false;
		private var Line3Done:Boolean = false;
		
		private var assetsLoaded:Boolean = false;
		
		private var pageLoaded:Boolean = false;

		public var paused:Boolean = false;

		public var NarrationPausePoint:int = 0;
		public var CuePointPausePoint:int = 0;

		public var NarrationImage:Image;

		public var NarrationDone:Boolean = true;
		public var NarrationStarted:Boolean = false;

		public function BookPage(readerBar:Image)
		{
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE, FocusGained);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE, FocusLost);
			//start on the first page
			currentPage = 0;

			// Create main scene root
			sceneRoot = new Sprite();
			addChild(sceneRoot);

			assets = new AssetManager(Costanza.SCALE_FACTOR,Costanza.mipmapsEnabled);
			assets.useMipMaps = Costanza.mipmapsEnabled;
			Root.currentAssetsProxy = assets;
			assets.verbose = Capabilities.isDebugger;
			//load up the first page and the navobjects and music we are going to be using

			NarrationImage = readerBar;

			assets.enqueue(
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Pages/Page0")
			);

			//load the individual words for the currently selected page (in this case we just started so 1)
			LoadWords();

			assets.loadQueue( onProgress );
		}
		
		private function onProgress(ratio:Number):void
		{
			if (ratio == 1)
			{
				Starling.juggler.delayCall(function():void
				{
					// now would be a good time for a clean-up 
					System.pauseForGCIfCollectionImminent(0);
					System.gc();
				}, 0.15);
				BuildInitialScene();
				assetsLoaded = true;
			}
		}

		private function BuildInitialScene():void
		{
			oldBackground = new Image(assets.getTexture("Scene0"));
			sceneRoot.addChildAt(oldBackground, 0);
			background = new Image(assets.getTexture("Scene0"));
			sceneRoot.addChildAt(background, 1);

			dispatchEventWith(Root.SCENE_LOADED,true);

			AddReaderBox();

			PositionWords();
			FadeInOutText(true);

			dispatchEventWith(PageManager.LOAD_COMPLETE, true);
		}

		private function AddReaderBox():void
		{
			Reader_BOX = new Sprite();

			sceneRoot.addChildAt(Reader_BOX, 2);
			
			NarrationImage.x = -Costanza.STAGE_OFFSET;

			Reader_BOX.addChild(NarrationImage);
			Reader_BOX.x = Costanza.STAGE_OFFSET;
			Reader_BOX.y = Costanza.STAGE_HEIGHT - Reader_BOX.height;

			var cuesArray:Array = PageManager.NarrationXML.Page[currentPage].CuePoints.split(", ");
			CueVector = new <int>[];

			for (var j:int = 0; j < cuesArray.length; j++)
			{
				CueVector.push(int(cuesArray[j]));
			}

			for(var x:int = 0; x < words.length; x++)
			{
				Text = null;
				Text = new TextField(1,1,words[x], PageManager.NarrationXML.@FontClass, PageManager.NarrationXML.@FontSize);
				Text.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
				Text.addEventListener(TouchEvent.TOUCH, PressWord);
				WordBoxes.push(Text);
			}
		}

		private function EmptyReaderBox():void
		{
			Reader_BOX.removeChildren(0, Reader_BOX.numChildren - 1);
			WordBoxes.length = 0;
		}

		private function FocusLost(e:flash.events.Event):void
		{
			trace("BookPage.FocusLost()");
			if(assetsLoaded)
			{
				if(!NarrationDone && !paused && NarrationChannel != null && NarrationStarted)
				{	
					trace( "Pausing narration" );
					NarrationPausePoint = NarrationChannel.position;
					CuePointPausePoint = CueTimer;
					NarrationChannel.stop();
	
					paused = true;
				}
			}
			else
			{
				assets.purgeQueue();
			}
		}

		private function FocusGained(e:flash.events.Event):void
		{
			trace("BookPage.FocusGained()");
			
			if ( assetsLoaded )
			{
				if ( paused )
				{
					paused = false;

					trace( "Unpausing narration" );

					CurrentTime = flash.utils.getTimer();
					PreviousTime = flash.utils.getTimer();

					CueTimer = CuePointPausePoint;

					if ( NarrationChannel != null ) { NarrationChannel.stop(); }

					NarrationChannel = PageManager.NarrationSound.play(NarrationPausePoint);
				}
			}
			else if (PageManager.assetsLoaded && !assetsLoaded)
			{
				trace("Load assets");
				assets.enqueue(
					appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Pages/Page0")
				);
				LoadWords();
				assets.loadQueue( onProgress );
			}
		}

		private function PositionWords():void
		{
			//x:315 y:40 w:765 h:120
			LineOne = new Sprite();
			LineOne.alpha = 0;
			LineTwo = new Sprite();
			LineTwo.alpha = 0;
			LineThree = new Sprite();
			LineThree.alpha = 0;
			LineFour = new Sprite();
			LineFour.alpha = 0;

			Line1Done = false;
			Line2Done = false;
			Line3Done = false;

			for(var x:int = 0; x < WordBoxes.length; x++)
			{
				if(((LineOne.width + WordBoxes[x].width) < 765) && !Line1Done)
				{
					WordBoxes[x].x = int(LineOne.width);
					LineOne.addChild(WordBoxes[x]);
				}
				else if(((LineTwo.width + WordBoxes[x].width) < 765) && !Line2Done)
				{
					Line1Done = true;
					WordBoxes[x].x = int(LineTwo.width);
					LineTwo.addChild(WordBoxes[x]);
				}
				else if(((LineThree.width + WordBoxes[x].width) < 765) && !Line3Done)
				{
					Line2Done = true;
					WordBoxes[x].x = int(LineThree.width);
					LineThree.addChild(WordBoxes[x]);
				}
				else if((LineFour.width + WordBoxes[x].width) < 765)
				{
					Line3Done = true;
					WordBoxes[x].x = int(LineFour.width);
					LineFour.addChild(WordBoxes[x]);
				}
			}

			if(LineFour.width > 0)
			{
				LineOne.x = int((315 + (765 - LineOne.width)/2));
				LineOne.y = 40;
				LineTwo.x = int((315 + (765 - LineTwo.width)/2));
				LineTwo.y = 70;
				LineThree.x = int((315 + (765 - LineThree.width)/2));
				LineThree.y = 100;
				LineFour.x = int((315 + (765 - LineFour.width)/2));
				LineFour.y = 130;
				
				Reader_BOX.addChild(LineOne);
				Reader_BOX.addChild(LineTwo);
				Reader_BOX.addChild(LineThree);
				Reader_BOX.addChild(LineFour);
			}
			else if(LineThree.width > 0)
			{
				LineOne.x = int((315 + (765 - LineOne.width)/2));
				LineOne.y = 40;
				LineTwo.x = int((315 + (765 - LineTwo.width)/2));
				LineTwo.y = 70;
				LineThree.x = int((315 + (765 - LineThree.width)/2));
				LineThree.y = 100;
				
				Reader_BOX.addChild(LineOne);
				Reader_BOX.addChild(LineTwo);
				Reader_BOX.addChild(LineThree);
			}
			else if(LineTwo.width > 0)
			{
				LineOne.x = int((315 + (765 - LineOne.width)/2));
				LineOne.y = 50;
				LineTwo.x = int((315 + (765 - LineTwo.width)/2));
				LineTwo.y = 80;
				
				Reader_BOX.addChild(LineOne);
				Reader_BOX.addChild(LineTwo);
			}
			else
			{
				LineOne.x = int((315 + (765 - LineOne.width)/2));
				LineOne.y = 60;
				
				Reader_BOX.addChild(LineOne);
			}
			trace(LineOne.x + "  " + LineOne.y + "      " + LineTwo.x + "  " + LineTwo.y + "       " + LineThree.x + "  " + LineThree.y);
		}
		
		public function PressWord(e:TouchEvent):void
		{
			if(NarrationDone || PageManager.muted)
			{
				var touch:Touch = e.getTouch(stage);
				if(touch == null){return;}
				if(touch.phase == TouchPhase.BEGAN)
				{
				Text = e.currentTarget as TextField;
					for(var x:int = 0; x < words.length; x++)
					{
						if(Text.text == words[x])
						{
							var WordSound:Sound;
							//WordSound = assets.getSound("day");
							WordSound = assets.getSound(words[x].split(" ").join("").split(".").join("").split(",").join("").split("!").join("").split("\"").join("").split(":").join("").split("-").join("").split(";").join("").split("?").join("").toUpperCase());
							WordSound.play();
							x = words.length;
							Text.color = 0x0C20FF;
							TweenMax.delayedCall(0.5, FadeText, [Text]);
						}
					}
				};
			}
		}
		
		public function FadeText(text:TextField):void
		{
			text.color = 0x000000;
		}
		
		public function CuePointTimer(e:starling.events.Event):void
		{
			if(!PageManager.muted && !NarrationDone && !paused)
			{
				PreviousTime = CurrentTime;
				//var holdertime:int = CurrentTime;
				CurrentTime = flash.utils.getTimer();
				
				ElapsedTime = CurrentTime - PreviousTime;
				
				//PreviousTime = holdertime;
				
				CueTimer += ElapsedTime;
				
				for(var x:int = 0; x < CueVector.length; x++)
				{
					if(CueTimer > CueVector[x])
					{
						if(WordBoxes.length > x)
						{
							WordBoxes[x].color = 0x0C20FF;
						}
						if(x > 0)
						{
							WordBoxes[x-1].color = 0x000000;
						}
					}
				}
				if(CueTimer >= CueVector[CueVector.length-1])
				{
					trace("Stop Sound");
					NarrationChannel.stop();
					NarrationDone = true;
				}
			}
		}

		public function InitializeTimer():void
		{
			CueTimer += CueVector[0];
			CurrentTime = flash.utils.getTimer();
			PreviousTime = flash.utils.getTimer();
		}

		public function LoadWords():void
		{
			wordsArray = PageManager.NarrationXML.Page[currentPage].PageText.split(" ");
			words = new <String>[];

			for (var i:int = 0; i < wordsArray.length; i++)
			{
				words.push(wordsArray[i] + " ");
				assets.enqueue(
					appDir.resolvePath("audio/IndividualWords/" + words[i].split(" ").join("").split(".").join("").split(",").join("").split("!").join("").split("\"").join("").split(":").join("").split("-").join("").split(";").join("").split("?").join("").toUpperCase() + ".mp3")
				);
				trace("Word: " + wordsArray[i]);
				//NarrationSounds.push(assets.getSound(wordsArray[i]));
			}
		}

		public function PreviousPage():void
		{
			Pause();
			FadeInOutText();
			//advance our page
			NarrationDone = true;
			NarrationStarted = false;
			currentPage--;
			sceneRoot.removeChild(oldBackground);
			oldBackground.dispose();
			oldBackground = new Image(background.texture);
			sceneRoot.addChildAt(oldBackground,0);
			trace("currentPage " + currentPage);
			assets.enqueue(
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Pages/Page" + currentPage)
			);
			//load the individual words for the currently selected page (in this case we just started so 1)
			LoadWords();
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1)
				{
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
					TransitionBackward();
				}
			});
		}

		private function TransitionBackward():void
		{
			trace("transition Backward");
			sceneRoot.removeChild(background);
			background.dispose();
			background = new Image(assets.getTexture("Scene" + currentPage));
			sceneRoot.addChildAt(background,1);
			background.x = -background.width;
			TweenMax.to(background, 1.5, {x:0});
			TweenMax.to(Reader_BOX, 0.5, {alpha: 1});
			EmptyReaderBox();
			AddReaderBox();
			PositionWords();
			Resume();
			FadeInOutText(true);
			dispatchEventWith(PageManager.TRANSITION_FINISHED, true);
		}

		public function NextPage():void
		{
			Pause();
			FadeInOutText();
			NarrationDone = true;
			NarrationStarted = false;
			//advance our page
			currentPage++;
			sceneRoot.removeChild(oldBackground);
			oldBackground.dispose();
			oldBackground = new Image(background.texture);
			sceneRoot.addChildAt(oldBackground,0);
			trace("currentPage " + currentPage);
			assets.enqueue(
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Pages/Page" + currentPage)
			);
			//load the individual words for the currently selected page (in this case we just started so 1)
			LoadWords();
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1)
				{
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
					TransitionForward();
				}
			});
		}

		private function TransitionForward():void
		{
			trace("transition Forward");
			sceneRoot.removeChild(background);
			background.dispose();
			background = new Image(assets.getTexture("Scene" + currentPage));
			sceneRoot.addChildAt(background,1);
			background.x = 1500;
			TweenMax.to(background, 1.5, {x:0});
			EmptyReaderBox();
			AddReaderBox();
			PositionWords();
			Resume();
			FadeInOutText(true);
			dispatchEventWith(PageManager.TRANSITION_FINISHED, true);
		}

		private function FadeInOutText(fadeIn:Boolean = false):void
		{
			if(fadeIn)
			{
				TweenMax.delayedCall(1, TweenMax.to, [LineOne, 0.5, {alpha:1}]);
				TweenMax.delayedCall(1, TweenMax.to, [LineTwo, 0.5, {alpha:1}]);
				TweenMax.delayedCall(1, TweenMax.to, [LineThree, 0.5, {alpha:1}]);
				TweenMax.delayedCall(1, TweenMax.to, [LineFour, 0.5, {alpha:1}]);
			}
			else
			{
				TweenMax.to(LineOne, 0.5, {alpha:0});
				TweenMax.to(LineTwo, 0.5, {alpha:0});
				TweenMax.to(LineThree, 0.5, {alpha:0});
				TweenMax.to(LineFour, 0.5, {alpha:0});
			}
		}

		public function Pause():void
		{
			removeEventListener(starling.events.Event.ENTER_FRAME, CuePointTimer);
			NarrationChannel.stop();
			NarrationDone = true;

			trace("Pause");
			paused = true;
		}

		public function Resume():void
		{
			//MusicChannel = assets.playSound("Music2");
			CueTimer = 0;
			NarrationDone = false;
			paused = false;
			TweenMax.delayedCall(3, BeginNarration);
		}

		public function Stop():void
		{
			for(var x:int = 0; x < wordsArray.length; x++)
			{
				WordBoxes[x].color = 0x000000;
			}

			NarrationChannel.stop();
			removeEventListener(starling.events.Event.ENTER_FRAME, CuePointTimer);
		}

		public function Destroy():void
		{
			TweenMax.killAll();
			if(assets != null)
			{
				assets.dispose();
			}

			removeEventListener(starling.events.Event.ENTER_FRAME, CuePointTimer);
			Starling.juggler.purge();
			NarrationChannel.stop();
			for(var i:int = 0; i < NarrationSounds.length - 1; i++)
			{
				NarrationSounds[i].close();
				NarrationSounds[i] = null;
			}
			for(var j:int = 0; j < WordBoxes.length - 1; j++)
			{
				WordBoxes[j].dispose();
			}
			wordsArray.length = 0;
			Text.dispose();

			words.length = 0;

			Reader_BOX.dispose();

			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.ACTIVATE, FocusGained);
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.DEACTIVATE, FocusLost);

			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
		}

		public function BeginNarration():void
		{
			if(!PageManager.muted && assetsLoaded)
			{
				NarrationStarted = true;
				InitializeTimer();

				addEventListener(starling.events.Event.ENTER_FRAME, CuePointTimer);
				trace("Begin Narration");
				NarrationChannel = PageManager.NarrationSound.play(CueVector[0],0);
			}
		}

		public function MuteNarration(mute:Boolean):void
		{
			if(mute)
			{
				PageManager.muted = true;
				NarrationChannel.stop();
				NarrationDone = true;
				for(var x:int = 0; x < wordsArray.length; x++)
				{
					WordBoxes[x].color = 0x000000;
				}
			}
			else
			{
				NarrationDone = false;
				PageManager.muted = false;

				CueTimer = 0;
				BeginNarration();
			}
		}
	}
}
