package scenes.Pages
{
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.system.Capabilities;
	import flash.system.System;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;

	public class PageManager extends Sprite
	{
		public static const LOAD_LOBBY:String = "loadLobby";
		public static const TRANSITION_FINISHED:String = "transitionFinished";
		
		public static const MIN_PAGE:int = 0;
		public static const MAX_PAGE:int = 17;

		public static var muted:Boolean = false;

		private var sceneRoot:Sprite;
		private var bookPage:BookPage;

		//asset manager
		private var assets:AssetManager;
		private var appDir:File = File.applicationDirectory;

		//Buttons
		private var backBtn:Button;
		private var forwardBtn:Button;
		private var menuBtn:Button;
		private var muteBtn:Image;
		private var unmuteBtn:Image;

		//Music
		private var MusicChannel:SoundChannel;
		private var Music:Sound = new Sound();

		public static var assetsLoaded:Boolean = false;

		//sounds
		private var soundManager:SoundManager;

		//public var PagesVector:Vector.<BookPage> = new Vector.<BookPage>();
		public static var NarrationXML:XML;
		public static var NarrationSound:Sound;

		

		public function PageManager(num:int)
		{
			sceneRoot = new Sprite();
			addChild(sceneRoot);

			addEventListener(Root.DISPOSE, DisposeScene);
			addEventListener(TRANSITION_FINISHED, TransitionFinished);

			assets = new AssetManager(Costanza.SCALE_FACTOR,Costanza.mipmapsEnabled);
			assets.useMipMaps = Costanza.mipmapsEnabled;
			Root.currentAssetsProxy = assets;
			assets.verbose = Capabilities.isDebugger;
			//load up the navobjects and music we are going to be using
			assets.enqueue(
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Pages/NavObjects"),
				appDir.resolvePath("Narration"),
				appDir.resolvePath("audio/PageMusic")
			)

			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1)
				{
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
					}, 0.15);
					BuildScene();
					assetsLoaded = true;
				};
			});
		}

		private function BuildScene():void
		{
			soundManager = SoundManager.getInstance();

			NarrationSound = assets.getSound("story narration");
			NarrationXML = assets.getXml("page_strings");

			//add the book first, since we want the hud to sit on top of it
			bookPage = new BookPage(new Image(assets.getTexture("Textbox")));
			sceneRoot.addChild(bookPage);

			backBtn = new Button(assets.getTexture("ArrowIMG"));
			backBtn.alignPivot();
			backBtn.x = Costanza.SCREEN_START_X + Costanza.STAGE_OFFSET + backBtn.width/2;
			backBtn.y = backBtn.height/2;
			backBtn.rotation = -1;
			backBtn.visible = false;
			backBtn.alpha = 0;
			backBtn.addEventListener(starling.events.Event.TRIGGERED, GoToPreviousPage);
			sceneRoot.addChild(backBtn);

			forwardBtn = new Button(assets.getTexture("ArrowIMG"));
			forwardBtn.alignPivot();
			forwardBtn.x = Costanza.STAGE_WIDTH + Costanza.WIDESCREEN_WIDTH_OFFSET - forwardBtn.width/2;
			forwardBtn.y = forwardBtn.height/2;
			forwardBtn.alpha = 0;
			forwardBtn.touchable = false;
			forwardBtn.addEventListener(starling.events.Event.TRIGGERED, GoToNextPage);
			sceneRoot.addChild(forwardBtn);

			menuBtn = new Button(assets.getTexture("Home"));
			menuBtn.x = 200 + Costanza.STAGE_OFFSET;
			menuBtn.y = 640;
			menuBtn.addEventListener(starling.events.Event.TRIGGERED, ToMenu);
			sceneRoot.addChild(menuBtn);

			unmuteBtn = new Image(assets.getTexture("mute_off"));
			unmuteBtn.x = 1090 + Costanza.STAGE_OFFSET;
			unmuteBtn.y = 640;
			unmuteBtn.visible = muted;
			sceneRoot.addChild(unmuteBtn);

			muteBtn = new Image(assets.getTexture("mute"));
			muteBtn.x = 1090 + Costanza.STAGE_OFFSET;
			muteBtn.y = 640;
			muteBtn.visible = !muted;
			sceneRoot.addChild(muteBtn);
			
			TweenMax.delayedCall(3, TweenMax.to, [forwardBtn, 1, {alpha:1}]);
			TweenMax.delayedCall(3, TurnOnButton);
			trace( "Starting book" );
			//bookPage.Resume( true );
			PlayMusic();
			
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE, FocusGained);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE, FocusLost);
		}
		
		private function TurnOnButton():void
		{
			forwardBtn.touchable = true;
			unmuteBtn.addEventListener(TouchEvent.TOUCH , Mute);
			muteBtn.addEventListener(TouchEvent.TOUCH , Mute);
		}

		private function GoToNextPage(e:starling.events.Event):void
		{
			unmuteBtn.removeEventListener(TouchEvent.TOUCH , Mute);
			muteBtn.removeEventListener(TouchEvent.TOUCH , Mute);
			TweenMax.killDelayedCallsTo( bookPage.BeginNarration );
			TweenMax.to(forwardBtn, 0.5, {alpha:0});
			TweenMax.to(backBtn, 0.5, {alpha:0});
			forwardBtn.touchable = false;
			backBtn.touchable = false;
			if( MusicChannel != null )
			{
				MusicChannel.stop();
				MusicChannel = null;
			}
			bookPage.TurnPage();
			PlayMusic();

			forwardBtn.visible = true;
			backBtn.visible = true;

			if(bookPage.currentPage == MAX_PAGE) { forwardBtn.visible = false; }
		}

		private function GoToPreviousPage(e:starling.events.Event):void
		{
			unmuteBtn.removeEventListener(TouchEvent.TOUCH , Mute);
			muteBtn.removeEventListener(TouchEvent.TOUCH , Mute);
			TweenMax.killDelayedCallsTo( bookPage.BeginNarration );
			TweenMax.to(forwardBtn, 0.5, {alpha:0});
			TweenMax.to(backBtn, 0.5, {alpha:0});
			forwardBtn.touchable = false;
			backBtn.touchable = false;
			if( MusicChannel != null )
			{
				MusicChannel.stop();
				MusicChannel = null;
			}
			bookPage.TurnPage( false );
			PlayMusic();

			forwardBtn.visible = true;
			backBtn.visible = true;

			if(bookPage.currentPage == MIN_PAGE) { backBtn.visible = false; }
		}

		private function TransitionFinished(event:starling.events.Event):void
		{
			trace("We start fade in heres");
			TweenMax.delayedCall(3, TweenMax.to, [forwardBtn, 0.5, {alpha:1}]);
			TweenMax.delayedCall(3, TweenMax.to, [backBtn, 0.5, {alpha:1}]);
			TweenMax.delayedCall(3.5, ActivateButtons);
		}

		private function ActivateButtons():void
		{
			forwardBtn.touchable = true;
			backBtn.touchable = true;
			unmuteBtn.addEventListener(TouchEvent.TOUCH , Mute);
			muteBtn.addEventListener(TouchEvent.TOUCH , Mute);
		}

		private function Mute(e : TouchEvent):void
		{
			var touch:Touch = e.getTouch(stage);
			if(touch == null){return;}

			if(touch.phase == TouchPhase.BEGAN)
			{
				if(!muted)
				{
					muted = true;
					if(MusicChannel != null) { MusicChannel.stop(); MusicChannel = null; }
					bookPage.MuteNarration(true);
					muteBtn.visible = false;
					unmuteBtn.visible = true;
				}
				else
				{
					muted = false;
					PlayMusic();
					bookPage.MuteNarration(false);
					muteBtn.visible = true;
					unmuteBtn.visible = false;
				}
			}
		}

		private function PlayMusic():void
		{
			if(MusicChannel == null)
			{
				var soundTrans:SoundTransform = new SoundTransform(Costanza.musicVolume*.6);
				var currentPage:int = bookPage.currentPage;
				if(currentPage == 0 || currentPage == 8 || currentPage == 10 || currentPage == 17)
				{
					trace("Play Music 1,9,11,18");
					Music = assets.getSound("Music1");
					
					if(!muted)
						MusicChannel = Music.play(0,9,soundTrans);
				}
				else if(currentPage == 1 || currentPage == 5 || currentPage == 9)
				{
					trace("Play Music 2,6,10");
					Music = assets.getSound("Music2");
					
					if(!muted)
						MusicChannel = Music.play(0,9, soundTrans);
				}
				else if(currentPage == 2 || currentPage == 12 || currentPage == 14)
				{
					trace("Play Music 3,13,15");
					Music = assets.getSound("Music3");
					
					if(!muted)
						MusicChannel = Music.play(0,9, soundTrans);
				}
				else if(currentPage == 3 || currentPage == 6 || currentPage == 11 || currentPage == 16)
				{
					trace("Play Music 4,7,12,17");
					Music = assets.getSound("Music4");

					if(!muted)
						MusicChannel = Music.play(0,9, soundTrans);
				}
				else if(currentPage == 4 || currentPage == 13)
				{
					trace("Play Music 5,14");
					Music = assets.getSound("Music5");
					
					if(!muted)
						MusicChannel = Music.play(0,9, soundTrans);
				}
				else
				{
					trace("Play Music 8,16");
					Music = assets.getSound("Music6");
					
					if(!muted)
						MusicChannel = Music.play(0,9, soundTrans);
				}
			}
		}

		private function ToMenu(e:starling.events.Event):void
		{
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.ACTIVATE, FocusGained);
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.DEACTIVATE, FocusLost);
			bookPage.MuteNarration(true);
			if( MusicChannel != null ) { trace( "Stopping music" ); MusicChannel.stop(); }
			dispatchEventWith(Root.LOAD_LOBBY, true);
		}

		private function DisposeScene():void
		{
			trace("disposing of Story");
			bookPage.Destroy();
			if(MusicChannel != null) { MusicChannel.stop(); }
			Music = null;
			assets.dispose();

			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
			sceneRoot.dispose();
			System.pauseForGCIfCollectionImminent();
			System.gc();
		}
		
		private function FocusLost(e:flash.events.Event):void
			{ if( MusicChannel != null ) { MusicChannel.stop(); MusicChannel = null; } }
		
		private function FocusGained(e:flash.events.Event):void { PlayMusic(); }
	}
}