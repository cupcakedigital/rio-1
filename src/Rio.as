package
{
	import com.cupcake.DeviceInfo;
	import com.cupcake.Utils;
	import com.greensock.TweenMax;
	
	CONFIG::PUSH
	{
		import com.pushwoosh.nativeExtensions.PushNotification;
		import com.pushwoosh.nativeExtensions.PushNotificationEvent;
	}
	
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display3D.Context3DProfile;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	import flash.system.Capabilities;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	
	import utils.Captcha;

	[SWF(frameRate="60", backgroundColor="#000000")]
	// Light blue #76adc4
	public class Rio extends Sprite
	{
		// Startup image
		[Embed(source="/startup.png")]
		private static var Background:Class;
		
		[Embed(source="/startupHD.png")]
		private static var BackgroundHD:Class;

		private static var mStarling:Starling;
		private var mStarlingProfile:String = Context3DProfile.BASELINE;
		private var soundManager:SoundManager;
		
		[Embed(source = "../assets/fonts/Coop Flaired.otf",
			fontName = "Coop_Flaired",
			fontFamily="Coop_Flaired",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
		public static const Coop_Flaired:Class;
		
		[Embed(source = "../assets/fonts/VAGRounded-Bold.otf",
			fontName = "VAGRounded",
			fontFamily="VAGRounded",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
		public static const VagRounded:Class;
		
		[Embed(source = "../assets/fonts/DancingSuperserif.ttf",
			fontName = "DancingSuperserif",
			fontFamily="DancingSuperserif",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
		public static const Dancing_Superserif:Class;

		[Embed(source = "../assets/fonts/TikiIsland.ttf",
			fontName = "TikiIsland",
			fontFamily="TikiIsland",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
		public static const TikiIsland:Class;
		
		public static var savedVariablesObject:SharedObject;

		public function Rio()
		{
			//register for push notifications
			CONFIG::PUSH
			{
				var pushwoosh:PushNotification = PushNotification.getInstance();
				pushwoosh.addEventListener(PushNotificationEvent.PERMISSION_GIVEN_WITH_TOKEN_EVENT, onToken);
				pushwoosh.addEventListener(PushNotificationEvent.PERMISSION_REFUSED_EVENT, onError);
				pushwoosh.addEventListener(PushNotificationEvent.PUSH_NOTIFICATION_RECEIVED_EVENT, onPushReceived);
				pushwoosh.registerForPushNotification();
			}
		
			// set general properties
			var retinaTest:Boolean = false;
			
			var w:int		= Math.max( stage.fullScreenWidth, stage.fullScreenHeight );
			var h:int		= Math.min( stage.fullScreenWidth, stage.fullScreenHeight );
			var r:Number	= h/w; // calculates the aspect ratio
			var d:Number	= .56222 / r; // calculates the ratio difference between device and 16:9

			if( retinaTest || 
				( Capabilities.screenResolutionX == 1536 && Capabilities.screenResolutionY == 2048 && CONFIG::MARKET == "itunes" ) )
			{
				Costanza.IPAD_RETINA = true;
				Costanza.STAGE_HEIGHT = 768;
				Costanza.STAGE_WIDTH = 1024;
				Costanza.SCALE_FACTOR = 2;
				Costanza.STAGE_OFFSET = -171;
				Costanza.SCREEN_START_X = 171;
				Costanza.WIDESCREEN_WIDTH_OFFSET = 0;
				mStarlingProfile = Context3DProfile.BASELINE_EXTENDED;
			}
			else
			{
				Costanza.SCREEN_START_X = Math.abs( ( 1366 * d ) - 1366 ) / 2;
				Costanza.WIDESCREEN_WIDTH_OFFSET = -Costanza.SCREEN_START_X;
			}

			CONFIG::DEBUG { trace( "screenx = " + Capabilities.screenResolutionX  + " screeny = " + Capabilities.screenResolutionY); }

			//Setup stage and sizing params. also device detect for context.
			stage.align 				= StageAlign.TOP;
			var stageWidth:int   		= Costanza.STAGE_WIDTH;
			var stageHeight:int  		= Costanza.STAGE_HEIGHT;

			Costanza.VIEWPORT_WIDTH 	= stage.fullScreenWidth;
			Costanza.VIEWPORT_HEIGHT 	= stage.fullScreenHeight - ( CONFIG::MARKET == "nabi" ? Costanza.NABI_OFFSET : 0 );
			Costanza.VIEWPORT_OFFSET 	= (Costanza.STAGE_WIDTH - Costanza.VIEWPORT_WIDTH) / 2;

			// create a suitable viewport for the screen size
			// we develop the game in a *fixed* coordinate system of 1366x768; the game might 
			// then run on a device with a different resolution; for that case, we zoom the 
			// viewPort to the optimal size for any display and load the optimal textures.
			var viewPort:Rectangle = RectangleUtil.fit(new Rectangle(0, 0, stageWidth, stageHeight), 
				new Rectangle(0, 0, Costanza.VIEWPORT_WIDTH , Costanza.VIEWPORT_HEIGHT), 
				ScaleMode.NO_BORDER);	
	
			if ( CONFIG::MARKET == "nabi" ) { viewPort.top = 0; }
			var iOS:Boolean = Capabilities.manufacturer.indexOf("iOS") != -1;

			Starling.multitouchEnabled = true;  // useful on mobile devices
			Starling.handleLostContext = !iOS;  // not necessary on iOS. Saves a lot of memory!


			var appDir:File = File.applicationDirectory;

			//check resolution to see if we need mipmapping enabled
			if(stage.fullScreenHeight < 384)//if below half our default size of 768
			{
				Costanza.mipmapsEnabled = true;
			}

			// create the AssetManager, which handles all required assets for this resolution
			var assets:AssetManager = new AssetManager(Costanza.SCALE_FACTOR,Costanza.mipmapsEnabled);

			trace("Scale factor is " + Costanza.SCALE_FACTOR);

			assets.verbose = Capabilities.isDebugger;
			assets.enqueue(
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Global"),
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/HighScore"),
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/promo"),
				appDir.resolvePath("audio/menu"),
				appDir.resolvePath("fonts")
			);

			//shared Object with stored variables
			savedVariablesObject = SharedObject.getLocal("RioSavedData");
			setSharedData();

			// While Stage3D is initializing, the screen will be blank. To avoid any flickering, 
			// we display a startup image now and remove it below, when Starling is ready to go.
			// This is especially useful on iOS, where "Default.png" (or a variant) is displayed
			// during Startup. You can create an absolute seamless startup that way.
			// 
			// These are the only embedded graphics in this app. We can't load them from disk,
			// because that can only be done asynchronously (resulting in a short flicker).
			// 
			// Note that we cannot embed "Default.png" (or its siblings), because any embedded
			// files will vanish from the application package, and those are picked up by the OS!
			
			var backgroundClass:Class = Costanza.SCALE_FACTOR == 1 ? Background : BackgroundHD;
			var background:Bitmap = new backgroundClass();
			BackgroundHD = null; // no longer needed!
			Background = null; // no longer needed!

			background.x = viewPort.x;
			background.y = viewPort.y;
			background.width  = viewPort.width;
			background.height = viewPort.height;
			background.smoothing = true;
			addChild(background);

			// launch Starling
			mStarling = new Starling(Root, stage, viewPort,null,Context3DRenderMode.AUTO, mStarlingProfile);
			mStarling.stage.stageWidth  = stageWidth;  // <- same size on all devices!
			mStarling.stage.stageHeight = stageHeight; // <- same size on all devices!
			mStarling.simulateMultitouch = false;
			//mStarling.showStatsAt(HAlign.CENTER, VAlign.BOTTOM); // <-- stats box
			//mStarling.enableErrorChecking = Capabilities.isDebugger;

			mStarling.addEventListener(starling.events.Event.ROOT_CREATED, 
				function onRootCreated(event:Object, app:Root):void
				{
					mStarling.removeEventListener(starling.events.Event.ROOT_CREATED, onRootCreated);
					removeChild(background);
					background = null;

					var bgTexture:Texture = Texture.fromEmbeddedAsset(backgroundClass, false, false, Costanza.SCALE_FACTOR);
					app.start(bgTexture, assets);
					mStarling.start();
				});

			// When the game becomes inactive, we pause Starling; otherwise, the enter frame event
			// would report a very long 'passedTime' when the app is reactivated. 

			soundManager = SoundManager.getInstance();
			
			//soundManager.addSound("Menu", Root.assets.getSound("Menu"));
			//soundManager.addSound("Back", assets.getSound("back button on games"));

			NativeApplication.nativeApplication.addEventListener( flash.events.Event.ACTIVATE, WindowActivated ); 
			NativeApplication.nativeApplication.addEventListener( flash.events.Event.DEACTIVATE, WindowDeactivated );
			
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID && CONFIG::MARKET != "nabi" ) 
				{ stage.addEventListener( flash.events.Event.RESIZE, StageResized ); }
			
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID || DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP ) 
				{ Utils.addHandler( NativeApplication.nativeApplication, KeyboardEvent.KEY_DOWN, HandleKeys ); }
        }
		
		private function WindowActivated( e:flash.events.Event ):void
		{ 
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID && !CONFIG::NOOK ) 
				{ NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE; }
			
			if ( !Captcha.visible ) { mStarling.start(); } 
			soundManager.muteAll(false); 
			TweenMax.resumeAll(); 
			stage.quality = stage.quality; 
			trace( "Window activated" );
		}
		
		private function WindowDeactivated( e:flash.events.Event ):void
		{ 
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID && !CONFIG::NOOK ) 
				{ NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL; }
			
			mStarling.stop(); 
			soundManager.muteAll(true); 
			TweenMax.pauseAll();
			trace( "Window deactivated" );
		}
		
		private final function HandleKeys(event:KeyboardEvent):void
		{
			if ( event.keyCode == Keyboard.BACK )
			{
				event.stopImmediatePropagation();
				event.preventDefault();
				
				if ( CONFIG::MARKET == "nook" )
				{
					// putting this here as I expect we're going to have to do something silly for nook
					ExitApp();
				}
				else
				{
					ExitApp();
				}
			}
		}
		
		private final function ExitApp():void
		{
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
			NativeApplication.nativeApplication.exit(); 
		}
		
		private function StageResized( e:flash.events.Event ):void
		{
			Costanza.VIEWPORT_WIDTH = stage.stageWidth;
			Costanza.VIEWPORT_HEIGHT = stage.stageHeight;
			Costanza.VIEWPORT_OFFSET = (Costanza.STAGE_WIDTH - Costanza.VIEWPORT_WIDTH)/2;
			
			var viewPort:Rectangle = RectangleUtil.fit(
				new Rectangle(0, 0, Costanza.STAGE_WIDTH, Costanza.STAGE_HEIGHT), 
				new Rectangle(0, 0, Costanza.VIEWPORT_WIDTH , Costanza.VIEWPORT_HEIGHT), 
				ScaleMode.NO_BORDER);
			
			Starling.current.viewPort = viewPort;
			
			// only do this once on nook
			if ( CONFIG::MARKET == "nook" ) { stage.removeEventListener( flash.events.Event.RESIZE, StageResized ); }
		}

		private function setSharedData():void
		{
			//set variables from saved data object
			//savedVariablesObject.clear();
			
			//if first time and saved data is undefined, set data, else set vars based on saved data
			
			//VOLUME
			(savedVariablesObject.data.musicVolume >= 0) ? Costanza.musicVolume = savedVariablesObject.data.musicVolume : savedVariablesObject.data.musicVolume = Costanza.musicVolume;
			(savedVariablesObject.data.voiceVolume >= 0) ? Costanza.voiceVolume = savedVariablesObject.data.voiceVolume : savedVariablesObject.data.voiceVolume = Costanza.voiceVolume;
			(savedVariablesObject.data.soundVolume >= 0) ? Costanza.soundVolume = savedVariablesObject.data.soundVolume : savedVariablesObject.data.soundVolume = Costanza.soundVolume;
			(savedVariablesObject.data.lastNameEntered) ? Costanza.lastNameEntered = savedVariablesObject.data.lastNameEntered : savedVariablesObject.data.lastNameEntered = Costanza.lastNameEntered;
			savedVariablesObject.flush();
			
		}
		
		CONFIG::PUSH
		{
			public static function onToken(e:PushNotificationEvent):void
			{ trace("\n TOKEN: " + e.token + " "); }
			
			public static function onError(e:PushNotificationEvent):void
			{ trace("\n TOKEN: " + e.errorMessage+ " "); }
			
			public static function onPushReceived(e:PushNotificationEvent):void
			{ trace("\n TOKEN: " + JSON.stringify(e.parameters) + " "); }
		}
		
		
		public static function Cycle():void
		{
			mStarling.stop();
			mStarling.start();
		}
    }
}