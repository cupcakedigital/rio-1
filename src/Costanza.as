package
{
	import feathers.text.BitmapFontTextFormat;
	
	import starling.errors.AbstractClassError;

	public class Costanza
	{
		public function Costanza() { throw new AbstractClassError(); }
		
		public static const NABI_OFFSET:int				= 30;

		public static var IPAD_RETINA:Boolean			= false;
		public static var STAGE_WIDTH:int				= 1366;
		public static var STAGE_HEIGHT:int				= 768;
		public static var STAGE_OFFSET:int				= 0;
		public static var SCALE_FACTOR:int				= 1;

		//Visible regions.
		public static var VIEWPORT_WIDTH:int  			= 1024;
		public static var VIEWPORT_HEIGHT:int 			= 768;
		public static var VIEWPORT_OFFSET:int 			= 171;
		
		public static var SCREEN_START_X:int			= 171;
		public static var WIDESCREEN_WIDTH_OFFSET:int	= -171;

		public static var mipmapsEnabled:Boolean		= false;

		public static var LABEL_TEXT_FORMAT:BitmapFontTextFormat;

		public static var lastPageRead:int				= 0;
		public static var lastNameEntered:String		= "";

		//sound stuff
		public static var musicVolume:Number			= .4;
		public static var soundVolume:Number			= 1.0;
		public static var voiceVolume:Number			= 1.0;
		
		//if you want the music volume's max to be less than the others, otherwise set to 1
		public static var musicVolumeMax:Number			= 0.4;
		
		public static var StageProportionScale:Number   = 1.0;
		
		public static const VIDEO_FILES:String			= "Cupcake_Splash";
		public static const VIDEO_WIDTHS:String			= "1366";
    }
}